# -*- coding: utf-8 -*-
"""
Created on Mon Aug 21 13:12:51 2017

@author: roesch
"""
from imp import reload 
import disp as d
reload (d)
import structlistconversion as sl
reload (sl)
import numpy as np
import shelve
import os 

def save_output(pg,te,l,s,filetype):
    t1=d.Disp1("Saving input file "+pg.filename+"."+filetype)
    p_list=sl.structlist(pg)
    te_list=sl.structlist(te)
    l_list=sl.structlist(l)
    s_list=sl.structlist(s)
    file = open("output_files/"+pg.filename+"/"+pg.filename+"."+filetype,"w") 
    file.write("p\t...\r\n")
    for i in p_list:
#        print(i[0])
#        print(i[1])
        file.write(i[0]+"\t"+i[1]+"\r\n")
    file.write("p\t..\r\n")
    file.write("te\t...\r\n")
    for i in te_list:
#        print(i)
        file.write(i[0]+"\t"+i[1]+"\r\n")
    file.write("te\t..\r\n")
    file.write("l\t...\r\n")
    for i in l_list:
        file.write(i[0]+"\t"+i[1]+"\r\n")
    file.write("l\t..\r\n")
    file.write("s\t...\r\n")
    for i in s_list:
        file.write(i[0]+"\t"+i[1]+"\r\n")
    file.write("s\t..\r\n")
    file.close()
    d.Disp2(t1)
    
def save_default(pg,te,l):
    t1=d.Disp1("Saving default file")
    pg_list=sl.structlist(pg)
    te_list=sl.structlist(te)
    l_list=sl.structlist(l)
    file = open("default.txt","w") 
    file.write("pg\t...\r\n")
    for i in pg_list:
#        print(i[0])
#        print(i[1])
        file.write(i[0]+"\t"+i[1]+"\r\n")
    file.write("pg\t..\r\n")
    file.write("te\t...\r\n")
    for i in te_list:
#        print(i)
        file.write(i[0]+"\t"+i[1]+"\r\n")
    file.write("te\t..\r\n")
    file.write("l\t...\r\n")
    for i in l_list:
        file.write(i[0]+"\t"+i[1]+"\r\n")
    file.write("l\t..\r\n")
    file.close()
    d.Disp2(t1)

def read_input(filename):
    t1=d.Disp1("Reading "+filename)
    file = open(filename)
    raw=np.loadtxt(file,delimiter="\t", dtype={'names':('col0','col1'),'formats':('S65536','S65536')})
    file.close()
    r0=raw['col0']
    r1=raw['col1']
    data0=list()
    data1=list()
    for i in range(len(r0)):
        temp0=str(r0[i])
        temp1=str(r1[i])
        data0.append(temp0[2:-1])
        data1.append(temp1[2:-1])
    level=0
    lev=list()
    for i in range(len(data0)-1):
        lev.append(level)
        if "..." in data1[i]:
            level=level+1
        elif (".." in data1[i+1]) & ("..." not in data1[i+1]):
            level=level-1     
    lev.append(0)
    levelzero=[i for i, e in enumerate(lev) if e==0]
    subdata=list()
    results=dict()
    for i in range(0,len(levelzero),2):
        for j in range(levelzero[i],levelzero[i+1]+1):
            subdata.append([data0[j],data1[j]])
        results[data0[levelzero[i]]]=sl.deliststruct(subdata)
        subdata.clear()
    d.Disp2(t1)
    return results
    
def shelve_layout(pg,te,l):
    t1=d.Disp1("Saving Layout in Shelf")
    if not(os.path.exists("output_files/"+pg.filename+"/layout")):
        os.mkdir("output_files/"+pg.filename+"/layout")
    myshelf=shelve.open("output_files/"+pg.filename+"/layout/"+pg.filename+"_layout",'n')
    myshelf["pg"]=pg
    myshelf["te"]=te
    myshelf["l"]=l
    myshelf.close()
    d.Disp2(t1)
###############################################################################
def get_layout(filename):
    t1=d.Disp1("Getting Layout from Shelf")
    myshelf = shelve.open("output_files/"+filename+"/layout/"+filename+"_layout")
    pg=myshelf["pg"]
    te=myshelf["te"]
    l=myshelf["l"]
    d.Disp2(t1)
    return pg,te,l
###############################################################################
def write_outputfile(output):
    outputfile=open("output_files/"+output.filename+"/"+output.filename+"_"+output.model+"_"+"_output.txt",'w', encoding='utf-8')
    for string in output.monitor:
        outputfile.write(string+"\n")
#    outputfile.write("#################OUTPUT##################\n")          
#    outputfile.write("Simulation model:\t\t"+output.model+"\n")
#    outputfile.write("\nMATERIALS:"+"\n")
#    outputfile.write("p-type:\t\t\t\t"+output.s.p_material+"\n")
#    outputfile.write("    S:\t\t\t\t"+str(output.s.p_seebeck*1000000)+" "+d.greek("MU",0)+"V/K"+"\n")
#    outputfile.write("    "+d.greek("SIGMA",0)+":\t\t\t\t"+str(output.s.p_el_conductivity/100)+" S/cm"+"\n")
#    outputfile.write("    "+d.greek("KAPPA",0)+":\t\t\t\t"+str(output.s.p_th_conductivity)+" W/mK"+"\n")
#    outputfile.write("    Layer thickness:\t\t"+str(round(output.s.p_thickness*1000000,3))+" "+d.greek("MU",0)+"m"+"\n")
#    outputfile.write("n-type:\t\t\t\t"+output.s.n_material+"\n")
#    outputfile.write("    S:\t\t\t\t"+str(output.s.n_seebeck*1000000)+" "+d.greek("MU",0)+"V/K"+"\n")
#    outputfile.write("    "+d.greek("SIGMA",0)+":\t\t\t\t"+str(output.s.n_el_conductivity/100)+" S/cm"+"\n")
#    outputfile.write("    "+d.greek("KAPPA",0)+":\t\t\t\t"+str(output.s.n_th_conductivity)+" W/mK"+"\n")
#    outputfile.write("    Layer thickness:\t\t"+str(round(output.s.n_thickness*1000000,3))+" "+d.greek("MU",0)+"m"+"\n")
#    write_type_of_connectormaterial(outputfile,output)
#    outputfile.write("Substrate:\t\t\t"+str(round(output.s.substrate_thickness*1000000,3))+" "+d.greek("MU",0)+"m "+output.s.substrate_material+"\n")
#    outputfile.write("\nLAYOUT:"+"\n")
#    outputfile.write("Filename:\t\t\t"+output.filename+"\n")
#    outputfile.write("Printer:\t\t\t"+output.pg.printer+"\n")
#    outputfile.write("Tilt angle:\t\t\t"+str(round(output.pg.angle,1))+"\n")
#    outputfile.write("Number of thermocouples:\t"+str(output.number_of_thermocouples)+"\n")
#    outputfile.write("Fold depth:\t\t\t"+str(round(output.te.cell_y*1000, 3))+" mm"+"\n")
#    outputfile.write("Width p-element:\t\t"+str(round(output.te.p.effective_x*1000, 3))+" mm"+"\n")
#    outputfile.write("Width n-element:\t\t"+str(round(output.te.n.effective_x*1000, 3))+" mm"+"\n")
#    outputfile.write("Width connectors:\t\t"+str(round(output.pg.connector_width*1000, 3))+" mm"+"\n")
#    outputfile.write("Spacing between columns:\t"+str(round(output.te.spacing_x*1000, 3))+" mm"+"\n")
#    outputfile.write("\nGENERATOR:"+"\n")
#    outputfile.write("Number of thermocouples:\t"+str(output.number_of_thermocouples)+"\n")
#    outputfile.write("Eff. "+d.greek("DELTA",1)+"T/K:\t\t\t"+str(round(output.delta_T_eff,3))+"\n")
#    outputfile.write("OC-voltage/K:\t\t\t"+str(round(output.voltage_per_kelvin*1000,3))+" mV/K"+"\n")
#    outputfile.write("El. resistance:\t\t\t"+str(round(output.electrical_resistence,3))+" "+d.greek("OMEGA",1)+"\n")
#    outputfile.write("Th. resistance:\t\t\t"+str(round(output.thermal_resistence,3))+" K/W"+"\n")
#    outputfile.write("Max. power/K²:\t\t\t"+str(round(output.power_per_kelvin_squared*1000000,3))+" "+d.greek("MU",0)+"W/K²"+"\n")
#    outputfile.write("Eff. Z-value:\t\t\t"+str(round(output.eff_Z,8))+" 1/K"+"\n")
#    outputfile.write("\nSCENARIO:"+"\n")
#    outputfile.write("Scenario name:\t\t\t"+output.s.scenario+"\n")
#    outputfile.write("T heat source:\t\t\t"+str(output.s.source.temperature)+" K ("+str(output.s.source.temperature-273.15)+" °C)"+"\n")
#    outputfile.write("T heat sink:\t\t\t"+str(output.s.sink.temperature)+" K ("+str(output.s.sink.temperature-273.15)+" °C)"+"\n")
#    outputfile.write(d.greek("DELTA",1)+"T:\t\t\t\t"+str(output.deltaT)+" K"+"\n")
#    outputfile.write("Tm:\t\t\t\t"+str(output.Tm)+" K ("+str(output.Tm-273.15)+" °C)"+"\n")
#    outputfile.write("Rth heat source:\t\t"+str(output.s.source.th_resistance)+" K/W"+"\n")
#    outputfile.write("Rth heat sink:\t\t\t"+str(output.s.sink.th_resistance)+" K/W"+"\n")
#    outputfile.write("Rel load:\t\t\t"+str(output.s.el_load)+" "+d.greek("OMEGA",1)+"\n")
#    outputfile.write("\nOTEG IN SCENARIO:"+"\n")
#    outputfile.write(d.greek("DELTA",1)+"T@TEG:\t\t\t\t"+str(output.deltaTatTEG)+" K"+"\n")
#    outputfile.write("OC-Voltage:\t\t\t"+str(round(output.openvoltage,3))+" V"+"\n")
#    outputfile.write("Voltage:\t\t\t"+str(round(output.voltage,3))+" V"+"\n")
#    outputfile.write("Max output power:\t\t"+str(round(output.max_power*1000000,3))+" "+d.greek("MU",0)+"W"+"\n")
#    outputfile.write("Power at load:\t\t\t"+str(round(output.poweratload*1000000,3))+" "+d.greek("MU",0)+"W"+"\n")
#    outputfile.write("Heat flow:\t\t\t"+str(round(output.heatflow,3))+" W"+"\n")
#    outputfile.write("PCE:\t\t\t\t"+str(round(output.eta,8))+"\n")
#    outputfile.write("Eff. ZT-value:\t\t\t"+str(round(output.ZT,8))+"\n")
#    outputfile.write("Efficiency:\t\t\t"+str(round(output.efficiency,8))+"\n")
    outputfile.close()
    return True
###############################################################################
def write_type_of_connectormaterial(outputfile,output):
    if output.pg.connectormaterial=="c":
        outputfile.writet("Connectors:\t\t\t"+str(round(output.s.c_thickness*1000000,3))+d.greek("MU",0)+"m "+output.s.c_material+"\n")
        outputfile.write("    S:\t\t"+str(output.s.c_seebeck*1000000)+" "+d.greek("MU",0)+"V/K"+"\n")
        outputfile.write("    "+d.greek("SIGMA",0)+":\t\t\t\t"+str(output.s.c_el_conductivity)+" S/cm"+"\n")
        outputfile.write("    "+d.greek("KAPPA",0)+":\t\t\t\t"+str(output.s.c_th_conductivity)+" W/mK"+"\n")
        outputfile.write("    Layer thickness:\t\t"+str(round(output.s.c_thickness*1000000,3))+" "+d.greek("MU",0)+"m"+"\n")
    else:
        outputfile.write("Connectors:\t\t\t"+output.pg.connectormaterial+"-type"+"\n")
        return True