# -*- coding: utf-8 -*-
"""
Created on Tue Aug 28 15:01:53 2018

@author: andre
"""

import numpy as np

A=np.array([[1,0, 3],[0,0,0],[5, 0, 7]])
C=A[np.any(A != 0, axis=1)]
B=A[np.any(A == 0, axis=1)]
