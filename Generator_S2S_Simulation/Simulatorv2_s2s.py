# -*- coding: utf-8 -*-
"""
Created on Thu Apr 12 11:10:44 2018

@author: andres
"""
from imp import reload 
import numpy as n
import os
import iodata as inout
import time
import mainfunctions_s2s as m
import globalclasses as gc
import pswriter_s2s as ps 
import simulation_s2s as sim
import disp as d
import subprocess
reload(d)
reload(sim)
reload(ps)
reload(gc)
reload(m)
reload(inout)

starttime = time.time()

s=gc.voidclass()
s.scenario="TEGmeasurementsetupLTI"
s.p_material="PEDOTNanowires_SL"
s.n_material="TiS2_SL"
s.c_material="PEDOTNanowires_SL"
s.substrate_material="PEN"
s.precision=n.array(200)
s.v_factor=n.array(1)
s.n_wetfilm_thickness=n.array(40e-6)
s.c_wetfilm_thickness=n.array(40e-6)
s.p_wetfilm_thickness=n.array(40e-6)
s.n_thickness=n.array(2.3e-6)
s.p_thickness=n.array(2.8e-6)
s.c_thickness=n.array(2.8e-6)
s.air_thickness=n.array(1e-6)
s.substrate_thickness=n.array(1.4e-6)
s.contact_R=n.array(1e-3)
s.contact_resistance_pn=n.array(0)
s.contact_resistance_cp=n.array(0)
s.contact_resistance_cn=n.array(0)
  
#layout="180829_ARO_SL4"
#layout="180829_ARO_SL3"
layout="Simulationsbauer"
#layout="180606_ARO_Safelayout"
#layout="180713_ARO_Safelayout2"
#layout="180827_ARO_HandlayoutMeander"

# try:
#     os.remove(os.getcwd()+"/output_files/"+layout+"/"+layout+"_uncutlayout.eps")
# except:
#     pass
pg,te,l,output=sim.simulate_generator(layout,s)
inout.write_outputfile(output)
d.display_output(output)
endtime = time.time()
os.chdir("output_files/"+pg.filename)
subprocess.Popen("epstopdf "+pg.filename+"_uncutlayout.eps"+" --outfile "+pg.filename+"_uncutlayout.pdf",shell=True)
os.chdir("..")
os.chdir("..")
os.startfile(os.getcwd()+"/output_files/"+pg.filename+"/"+pg.filename+"_uncutlayout.pdf")

subprocess.Popen("epstopdf "+os.getcwd()+"/output_files/"+pg.filename+"/"+pg.filename+"_uncutlayout.eps"+" --outfile "+os.getcwd()+"/output_files/"+pg.filename+"/"+pg.filename+"_uncutlayout.pdf",shell=True)
#os.startfile(os.getcwd()+"/output_files/"+pg.filename+"/"+pg.filename+"_uncutlayout.pdf")

#subprocess.Popen("epstopdf "+os.getcwd()+"/output_files/"+pg.filename+"/"+pg.filename+"_uncutlayout.eps"+" --outfile "+os.getcwd()+"/output_files/"+pg.filename+"/"+pg.filename+"_uncutlayout.pdf",shell=True)
print("Elapsed time is "+str(endtime - starttime)+" seconds or "+str((endtime - starttime)/3600)+" hours.") 
#print(str(output.p_area)+","+str(output.n_area)+","+str(output.generator_area))