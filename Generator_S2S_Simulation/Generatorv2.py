# -*- coding: utf-8 -*-
"""
Created on Tue Aug 15 16:54:15 2017

@author: roesch
"""
from imp import reload 
import numpy as n
import os
import iodata as inout
import time
import mainfunctions as m
import globalclasses as gc
import pswriter as ps 
import simulation as sim
import disp as d
from datetime import datetime 
reload(d)
reload(sim)
reload(ps)
reload(gc)
reload(m)
reload(inout)
starttime = time.time()
pg=gc.voidclass()
te=gc.voidclass()
te.p=gc.voidclass()
te.n=gc.voidclass()
l=gc.voidclass()
s=gc.voidclass()

simulate=1
## DEFINITION FILENAME
pg.filename=datetime.now().strftime("%y%m%d")+"_ARO_Mofasser"
pg.comment="Uebermass in PDF wegen Belichtung 0.15mm. Ueberlapp von p und n 0.3 mm aufgrund von 0.05 bis 0.1 mm Randeinfransung und 0.1mm Sicherheit. 0.85 Gapsize resuliter in 1mm Gap wegen Siebbelichtung."
pg.printer="Roku" # Gallus or Roku
pg.material_unit1="c"
pg.material_unit2="p"
pg.material_unit3="n"
pg.shrinkfactor_unit1=n.array(0.0)
pg.shrinkfactor_unit2=n.array(0.0)
pg.shrinkfactor_unit3=n.array(0.0)
pg.connectormaterial="p"
##DEFINITION PAGE
pg.sheet_full_y=n.array(170e-3)#18*25.4e
pg.sheet_full_x=n.array(160e-3)
pg.substrate_foil_full_x=n.array(164e-3)

##ANGLE
pg.angle_in_degree=n.array(0)

## DEFINITION PRINTING AREA
pg.sheet_offset_bottom=n.array(3e-3)
pg.sheet_offset_top=n.array(4e-3)        
pg.sheet_offset_left=n.array(1e-3)        
pg.sheet_offset_right=n.array(1e-3)#"auto"#n.array(0.036941801051529465)
pg.second_screen_joint=n.array(80e-3)
pg.second_screen_joint_width=n.array(6e-3)

## SPECIAL SETTINGS
pg.effective_y_optimization=n.array(0)
pg.element_of_lower_value="p"
pg.startingpoint=n.array([0,0])
pg.no_c_material_between_elements=n.array(0)
pg.first_field_type="p"
pg.write_indicies=n.array(0)
pg.print_numeration=n.array(0)
pg.effective_y_optimization_y_variation_from_foldline=n.array(0e-3)
#pg.shrinkfactor=n.array(0.02)
pg.wrap_rod_diameter=n.array(10e-3)
pg.flip_inputconnector=n.array(1)
pg.draw_window=n.array(0)
pg.setmarkers=n.array(0)
pg.write_infobox=n.array(0)
pg.extra_rows_of_cutline=n.array(6)
pg.include_contactfield=n.array(0)
pg.periodic_enlargement=n.array(0)
## Printing SETTINGS
#pg.shrinkfactor=n.array(0.02)
pg.printdirection=n.array(1)


##MARKER PROPERTIES
pg.marker_line_x=n.array(18.5e-3)
pg.marker_line_y=n.array([0,0.14,0.24])
pg.marker_width=n.array(4e-3)
pg.marker_space_outer=n.array(1e-3)
pg.marker_space_inner=n.array(1e-3)
pg.sheet_marker_offset_left=n.array(17.5e-3)
pg.sheet_marker_offsets=n.array(20e-3)
pg.sheet_marker_offset_right=n.array(13e-3)
pg.circle_marker_outerradius=n.array(2e-3)
pg.circle_marker_linewidth=n.array(0.2e-3)
pg.circle_marker_crosswidth=n.array(0.2e-3)
pg.abs_width=n.array(5e-3)
pg.controlbar_length=n.array(20e-3)
pg.controlbar_width=n.array(1e-3)
pg.controlbar_position=n.array([50e-3,100e-3])
##TEXTBOX
pg.info_box_x_u1=n.array(25.5e-3)
pg.info_box_y_u1=n.array(10e-3)
pg.info_box_x_u2=n.array(25.5e-3)
pg.info_box_y_u2=n.array(65e-3)
pg.info_box_x_u3=n.array(25.5e-3)
pg.info_box_y_u3=n.array(120e-3)
pg.info_box_fontsize=n.array(8)
pg.info_box_font='Times-Roman'

## TESTPADS
pg.padradius=n.array(2.5e-3)
pg.pad_length=n.array(50e-3)
pg.pad_aspect_ratio=n.array(100)
pg.pad_ypositions_offset=n.array(56e-3)

##FOLD MARKERS
pg.foldmarker_width=n.array(0.15e-3)
pg.foldmarker_length=n.array(pg.wrap_rod_diameter*n.pi+3e-3)#  #pg.wrap_rod_diameter*n.pi Length of the folding markers
pg.foldmarker_spacing_right=n.array(9.5e-3)  # Padding from the right sheet border
pg.foldmarker_spacing_left=n.array(0e-3)

## CONTACTFIELD
pg.contactfield_spacing_right=n.array(10e-3)
pg.contactfield_spacing_left=n.array(0e-3)
pg.reduce_contactfield=n.array(4)
pg.space_between_last_connector_and_last_row=n.array(0e-3)
pg.window_width=n.array(5e-3)
pg.window_length=n.array(pg.wrap_rod_diameter*n.pi)#20e-3 

## COLORS
pg.color_n=n.array([0.4 ,0.8, 0.4])
pg.color_c=n.array([246/255, 52/255, 63/255])
pg.color_p=n.array([0, 0, 0.2])
pg.color_drawingarea=n.array([0.60, 0.60, 0.60])
pg.color_elementarea=n.array([0, 1, 0])
pg.color_design=n.array([0, 0, 0])

## DIMENSIONS OF THERMOELEMENT - TARGETS

#-- GENERAL PROPERTIES
pg.delta_element_x=n.array(0e-3)
te.cell_y=n.array(3.974360e-03)
te.spacing_x=n.array(2e-3)
te.spacing_y=n.array(0)  # should not be zero if connector_type ~=1
te.n_cell_x_to_p_cell_x_ratio=n.array(1/5) #te.n_element_x_to_p_element_x_ratio=1/5
te.dimensionsgiven=n.array(1)   # 1 -- p.effective_x n.effective_x  2 -- p.effective_x  n_cell_x_to_p_cell_x_ratio 3 -- n.effective_x  n_cell_x_to_p_cell_x_ratio
te.overlap_no_c=n.array(0.0001e-3)
te.foldline_displacement=n.array(0e-3)
#-- P-Element

te.p.overlap=n.array(-0.15e-3)
te.p.effective_x=n.array(1.015380e-02)
te.p.num_of_stripes=n.array(1)
te.p.gapsize=n.array(0.85e-3)

#-- N-Element

te.n.overlap=n.array(0.0001e-3)
te.n.effective_x=n.array(1.015380e-02)
te.n.num_of_stripes=n.array(1)
te.n.gapsize=n.array(0.85e-3)
#-- L-Element
pg.connector_type=n.array(4)
l.linker_type=n.array(2)
l.linker_height=n.array(4e-3)
l.linker_length=n.array(0e-3)

pg.connector_width=n.array(5e-3)
pg.connectorspacing=n.array(0e-3)
pg.adapt_n_width_to_connector_width=n.array(0)
pg.jump_connectors_are_turn_connectors=n.array(1) # always 1

## SIMULATION PARAMETERS
s.model=n.array(1)      # 1-- Simple Model 3-- Good Model 
s.scenario="TEGmeasurementsetupLTI"
s.p_material="PEDOTNanowires_SL"
s.n_material="TiS2_SL"
s.c_material="PEDOTNanowires_SL"
s.substrate_material="PEN"
s.precision=n.array(1000)
s.v_factor=n.array(1)
s.n_wetfilm_thickness=n.array(40e-6)
s.c_wetfilm_thickness=n.array(40e-6)
s.p_wetfilm_thickness=n.array(40e-6)
s.n_thickness=n.array(2*3.6e-6)
s.p_thickness=n.array(2*2.1e-6)
s.c_thickness=n.array(2e-6)
s.air_thickness=n.array(1e-6)
s.substrate_thickness=n.array(4e-6)

#s.n.el_conductivity=n.array(2.4e4)
#s.p.el_conductivity=n.array(4.4e4)
#s.c.el_conductivity=n.array(4.4e4)
#s.air_el_conductivity=n.array(0)
#s.substrate.el_conductivity=n.array(0)
#s.n.th_conductivity=n.array(0.7)
#s.p.th_conductivity=n.array(0.34)
#s.c.th_conductivity=n.array(0.34)
#s.air_th_conductivity=n.array(0.026)
#s.substrate.th_conductivity=n.array(0.25)
#s.n.seebeck=n.array(-92e-6)
#s.p.seebeck=n.array(2e-5)
#s.c.seebeck=n.array(2e-5)
#s.air_seebeck=n.array(0)
#s.substrate.seebeck=n.array(0)
#s.n.cost=n.array(500)
#s.p.cost=n.array(500)
#s.c.cost=n.array(500)
#s.air.cost=n.array(0)
#s.substrate.cost=n.array(0)
#s.n.solid_in_ink_percentage=n.array(0.01)
#s.p.solid_in_ink_percentage=n.array(0.03)
#s.c.solid_in_ink_percentage=n.array(0.01)
#s.air.solid_in_ink_percentage=n.array(0)
#s.substrate.solid_in_ink_percentage=n.array(0)
s.contact_resistance_pn=n.array(0)
s.contact_resistance_cp=n.array(0)
s.contact_resistance_cn=n.array(0)
if not(os.path.exists("output_files")):
    os.mkdir("output_files")
if not(os.path.exists("output_files\\"+pg.filename)):
    os.mkdir("output_files\\"+pg.filename)

inout.save_output(pg,te,l,s,"txt")
pg,te,l=m.generate("output_files\\"+pg.filename+"\\"+pg.filename+".txt")
m.drawing_generator(pg.filename)
if simulate:
    pg,te,l,output=sim.simulate_generator(pg.filename,s)
endtime = time.time()
os.startfile(os.getcwd()+"/output_files/"+pg.filename+"/"+pg.filename+"_rgb.pdf")
print("Elapsed time is "+str(endtime - starttime)+" seconds.") 
if simulate:
    inout.write_outputfile(output)
    d.display_output(output)