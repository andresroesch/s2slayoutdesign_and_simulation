# -*- coding: utf-8 -*-
"""
Created on Sat Oct 10 23:01:44 2020

@author: andre
"""
k_TiS2=0.69*(10.62e-3*7e-6)/4.17e-3
k_PEDOT=0.286*(10.62e-3*7e-6)/4.17e-3
k_PEN=0.22*(10.62e-3*6e-6)/4.17e-3
k_air=0.0264*(10.62e-3*6e-6)/4.17e-3

k_TC_PEN=k_PEDOT+k_TiS2+2*k_PEN
k_TC_air=k_PEDOT+k_TiS2+2*k_air
r_TC_PEN=1/k_TC_PEN
r_TC_air=1/k_TC_air
perc=(k_TC_PEN-k_TC_air)/k_TC_air
perc_r=(r_TC_air-r_TC_PEN)/r_TC_air

