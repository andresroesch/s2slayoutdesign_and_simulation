# -*- coding: utf-8 -*-
"""
Created on Fri Aug 25 14:56:13 2017

@author: roesch
"""
import numpy as np
import shapely.geometry as sp
import shapely.ops as so
###############################################################################
def sind(angle):
    return np.sin(np.deg2rad(angle))
###############################################################################
def cosd(angle):
    return np.cos(np.deg2rad(angle))
###############################################################################
def tand(angle):
    return np.tan(np.deg2rad(angle))
###############################################################################
def atand(ratio):
    return np.rad2deg(np.arctan(ratio))
###############################################################################
def rotatepoint(point,ang,centerofrotation):
    angle=np.copy(ang*np.pi/180)
    norm_x=np.array([np.cos(angle),np.sin(angle)])
    norm_y=np.array([-np.sin(angle),np.cos(angle)])
    return np.array(centerofrotation)+np.dot((point-np.array(centerofrotation)),np.array([norm_x,norm_y]))
###############################################################################
def rotate(polygon,ang,centerofrotation):
    angle=np.copy(ang*np.pi/180)
    norm_x=np.array([np.cos(angle),np.sin(angle)])
    norm_y=np.array([-np.sin(angle),np.cos(angle)])
    rot=list()
    for i in range(len(polygon)):
        rot.append(np.array(centerofrotation)+np.dot((polygon[i]-np.array(centerofrotation)),np.array([norm_x,norm_y])))  
    return np.array(rot)
###############################################################################
def translate(polygon,vector):
    return np.copy(np.array(polygon)+np.array(vector))
###############################################################################
def get_midpoint(polygon):
    midpoint=polygon[0,:]+(polygon[2,:]-polygon[0,:])/2;
    return midpoint
###############################################################################
def shrink(polygon,sf,pg):
    newpoly=np.copy(polygon)
    for point in polygon:
        point[0]=(point[0]-pg.sheet_full_x/2)*(1+sf)+pg.sheet_full_x/2
    return newpoly
###############################################################################
def mirror(polygon,pg):
    newpoly=np.copy(polygon)
    for point in newpoly:
        point[0]=-(point[0]-pg.sheet_full_x/2)+pg.sheet_full_x/2;
    return newpoly
###############################################################################
def polyable(polygon):
    if isinstance(polygon,list):
        answer=list()
        for p in polygon:
            answer.append(sp.Polygon(p))
    else:
        answer=sp.Polygon(polygon)
    return answer
###############################################################################
def drawable(polygon):
    if polygon.geom_type=='Polygon':
        return np.array(polygon.exterior.coords.xy).transpose()
    elif polygon.geom_type=='MultiPolygon':
        ans=list()
        for part in polygon:
            ans.append(part.exterior.coords.xy)
        return np.array(ans).transpose()
    elif polygon.geom_type=='MultiPoint':
        ans=list()
        for point in polygon:
            ans.append(np.reshape(np.array(point.coords.xy),-1))
        return ans
    else:
        ans=list()
        for i in range(len(polygon.xy)):
            ans.append(polygon.xy[i])
        return np.array(ans).transpose()
###############################################################################
def connect_two_fields_in_a_line(field1,field2):
    multipolygon=so.cascaded_union([sp.Polygon(field1),sp.Polygon(field2)])
    connection=drawable(multipolygon.convex_hull)
    return connection
###############################################################################
def findlengthofconnector(pg,field1,field2):
    f1=get_midpoint(field1)
    f2=get_midpoint(field2)
    connectorlength=np.linalg.norm((f1-f2)*transpose([-sind(pg.angle), cosd(pg.angle)]))+np.linalg.norm((f1-f2)*transpose([cosd(pg.angle), sind(pg.angle)]))
    return connectorlength
###############################################################################
def transpose(array):
    return np.array(array).transpose()  
###############################################################################  
def getarea(polygon):
    p=sp.Polygon(polygon)
    return p.area
###############################################################################
def par(r1,r2):
    return (r1*r2)/(r1+r2)