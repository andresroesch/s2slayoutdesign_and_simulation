# -*- coding: utf-8 -*-
"""
Created on Mon Jun 11 12:08:56 2018

@author: andre
"""
import sys

import numpy.linalg as lg
from imp import reload 
import pswriter as ps 
import globalclasses as gc
import numpy as np
import numpy.matlib as npm
import custommathfunctions as c
import shapely.geometry as sp
import shapely.ops as so
import matplotlib
import disp as d
import shapefunctions as sf

reload(sf)
reload(d)
reload(gc)
reload(ps)
def simulate_generator(output):
    pg=output.pg
    te=output.te
    l=output.l
    s=output.s
    s.THatTEG=25+273.15
    s.TCatTEG=49+273.15
    im=create_image(pg)
    p_areas=list()
    n_areas=list()
    for element in im.elementlist:
        for strip in element.stripes:
            exec(element.type+"_areas.append(sp.Polygon(strip))")
        exec(element.exitconnector_mat+"_areas.append(sp.Polygon(element.exitconnector))")
#        exec(element.exitconnector_mat+"_areas.append(sp.Polygon(element.exit))")
#        exec(element.exitconnector_mat+"_areas.append(sp.Polygon(element.entrance))")
    exec(pg.connectormaterial+"_areas.append(sp.Polygon(im.input_connector))") 
    p_area=so.cascaded_union(p_areas)
    n_area=so.cascaded_union(n_areas)
    overlap=p_area.intersection(n_area)
    generator=so.cascaded_union([p_area,n_area])
    infield=sp.Polygon(im.infield)
    outfield=sp.Polygon(im.outfield)
#    ps.draw(im.sheetfile,p_area,pg.color_p)
#    ps.draw(im.sheetfile,n_area,pg.color_n)
#    ps.draw(im.sheetfile,overlap,[1,1,0])         
#    ps.draw(im.sheetfile,im.infield,[1,0,1])
#    ps.draw(im.sheetfile,im.outfield,[0,1,0])  
    y_number=s.precision
    y_coordinates=np.linspace(0,im.limits[1],y_number+1)
    sidelength=y_coordinates[1]
    x_coordinates=np.arange(0,im.limits[0]+sidelength/2,sidelength)
    x_coordinates=x_coordinates[:-1]+sidelength/2
    y_coordinates=y_coordinates[:-1]+sidelength/2
    
#    dim=[len(x_coordinates),len(y_coordinates)]
    t1=d.Disp1("Meshing")
    output.coords=[sp.Point([x,y]) for x in x_coordinates for y in y_coordinates]
    midpoints=sp.MultiPoint(output.coords)
###############################################################################
    output.valid_midpoints=generator.intersection(midpoints)
    output.knot_coords=c.drawable(output.valid_midpoints)
    indices=(output.knot_coords-sidelength/2)/im.limits[1]*y_number 
    output.knot_indices=list()
    for index in indices:
        output.knot_indices.append([int(round(index[0])),int(round(index[1]))])
###############################################################################
    output.entrance=list()
    input_knot_neighbors=c.drawable(output.valid_midpoints.intersection(infield))
    input_knot_neighbor_indices=(input_knot_neighbors-sidelength/2)/im.limits[1]*y_number
    for index in input_knot_neighbor_indices:
        output.entrance.append(output.knot_indices.index([int(round(index[0])),int(round(index[1]))]))
###############################################################################    
    output.exit=list()
    output_knot_neighbors=c.drawable(output.valid_midpoints.intersection(outfield))
    output_knot_neighbor_indices=(output_knot_neighbors-sidelength/2)/im.limits[1]*y_number
    for index in output_knot_neighbor_indices:
        output.exit.append(output.knot_indices.index([int(round(index[0])),int(round(index[1]))]))
###############################################################################
    output.knot_in_p=list()
    kip=c.drawable(output.valid_midpoints.intersection(p_area))
    kip=(kip-sidelength/2)/im.limits[1]*y_number
    for index in kip:
        output.knot_in_p.append(output.knot_indices.index([int(round(index[0])),int(round(index[1]))]))
###############################################################################
    output.knot_in_n=list()
    kin=c.drawable(output.valid_midpoints.intersection(n_area))
    kin=(kin-sidelength/2)/im.limits[1]*y_number
    for index in kin:
        output.knot_in_n.append(output.knot_indices.index([int(round(index[0])),int(round(index[1]))]))    
###############################################################################
    output.knot_in_o=list()
    kio=c.drawable(output.valid_midpoints.intersection(overlap))
    kio=(kio-sidelength/2)/im.limits[1]*y_number
    for index in kio:
        output.knot_in_o.append(output.knot_indices.index([int(round(index[0])),int(round(index[1]))]))    
    d.Disp2(t1)
    
    t1=d.Disp1("Connecting")
    counter=0
    knot_neighbors=list()
    knot_neighbor_direction=list()
    output.knot_material=list()
    for i,index in enumerate(output.knot_indices):
        sys.stdout.write('\r'+"{:.2f}".format(counter*100/(len(output.knot_indices)))+" %\t")
        counter=counter+1
        neighbors=[[int(index[0]-1),int(index[1])],[int(index[0]+1),int(index[1])],[int(index[0]),int(index[1]-1)],[int(index[0]),int(index[1]+1)]]
        nd=[-1,1,-1j*1,1j]
        knot_n=list()
        knot_nd=list()
        for j,point in enumerate(neighbors):
            if point in output.knot_indices:
                knot_n.append(output.knot_indices.index(point))
                knot_nd.append(nd[j])
        knot_neighbor_direction.append(knot_nd)
        knot_neighbors.append(knot_n)
        if i in output.knot_in_p and i in output.knot_in_n:
            output.knot_material.append(0)
        elif i in output.knot_in_p:
            output.knot_material.append(1)
        elif i in output.knot_in_n:
            output.knot_material.append(-1)
        else:
            print("ERROOOOORR")         
    output.knot_neighbors=knot_neighbors
    d.Disp2(t1)   
    output=set_temperatureprofile(output,im,te,s)     
    for i,y in enumerate(output.temperatures_y):
        line=sf.makerectangle(im.limits[0],0.1e-3,[im.limits[0]/2,y])
        if output.temperatures_T[i]==s.THatTEG:    
            ps.draw(im.sheetfile,line,[1,0,0])
        else:
            ps.draw(im.sheetfile,line,[0,1,1])
    t1=d.Disp1("Setting up KPPV")
    output.I=np.zeros(len(output.knot_indices)+1)
#    
#    output.I[0]=1
    output.G=np.zeros([len(output.knot_indices)+2,len(output.knot_indices)+2])
    output.G[0][0]=1/s.contact_R*len(output.entrance)
    for i in output.entrance:
        output.G[0][i+1]=-1/s.contact_R
        output.G[i+1][0]=-1/s.contact_R
    output.G[-1][-1]=1/s.contact_R*len(output.exit)
    for i in output.exit:
        output.G[-1][i+1]=-1/s.contact_R
        output.G[i+1][-1]=-1/s.contact_R
    counter=0
    Gp=s.p_thickness*s.p_el_conductivity
    Gn=s.n_thickness*s.n_el_conductivity
    for i in range(len(output.knot_indices)):
        sys.stdout.write('\r'+"{:.2f}".format(counter*100/(len(output.knot_indices)))+" %\t")
        counter=counter+1
        j=i+1
        for m,n in enumerate(output.knot_neighbors[i]):
            ni=n+1
            if output.knot_material[i]==1 and output.knot_material[n]==1:
                output.G[j][ni]=-Gp
                output.G[ni][j]=-Gp
                S=s.p_seebeck
            elif output.knot_material[i]==-1 and output.knot_material[n]==-1:
                output.G[j][ni]=-Gn
                output.G[ni][j]=-Gn
                S=s.n_seebeck
            elif output.knot_material[i]==0 and output.knot_material[n]==0:
                output.G[j][ni]=-Gp-Gn
                output.G[ni][j]=-Gp-Gn
                S=0
            elif (output.knot_material[i]==-1 and output.knot_material[n]==0) or (output.knot_material[i]==0 and output.knot_material[n]==-1):
                output.G[j][ni]=ser(-2*Gn-2*Gp,-2*Gn)
                output.G[ni][j]=ser(-2*Gn-2*Gp,-2*Gn)
                S=s.n_seebeck/2
            elif (output.knot_material[i]==1 and output.knot_material[n]==0) or  (output.knot_material[i]==0 and output.knot_material[n]==1):
                output.G[j][ni]=ser(-2*Gn-2*Gp,-2*Gp)
                output.G[ni][j]=ser(-2*Gn-2*Gp,-2*Gp)
                S=s.p_seebeck/2
            elif (output.knot_material[i]==-1 and output.knot_material[n]==1) or  (output.knot_material[i]==1 and output.knot_material[n]==-1):    
                output.G[j][ni]=ser(ser(-3*Gn-3*Gp,-3*Gn),-3*Gp)
                output.G[ni][j]=ser(ser(-3*Gn-3*Gp,-3*Gn),-3*Gp)
                S=0
            else:
                print("SOMETHING ELSE WENT WRONG") 
            output.I[j]=output.I[j]+(output.T[n]-output.T[i])*S*output.G[j][ni]  
            output.I[ni]=output.I[ni]+(output.T[n]-output.T[i])*-S*output.G[j][ni]
        output.G[j][j]=-sum(output.G[j][:])    
    output.G=np.delete(output.G,(-1),axis=0)
    output.G=np.delete(output.G,(-1),axis=1)
    d.Disp2(t1)

    t1=d.Disp1("Solving KPPV")
    output.U=lg.solve(output.G,output.I)
    d.Disp2(t1)
    print(output.U[0])
    counter=0
    output.Iy=np.zeros(len(output.knot_indices), dtype=complex)
    for i in range(len(output.knot_indices)):
        sys.stdout.write('\r'+"{:.2f}".format(counter*100/(len(output.knot_indices)))+" %\t")
        counter=counter+1
        j=i+1
        for m,n in enumerate(output.knot_neighbors[i]):
            ni=n+1
            output.Iy[i]=output.Iy[i]+knot_neighbor_direction[i][m]*(output.U[j]-output.U[ni])*-output.G[j][ni]   
    output.Id=np.angle(output.Iy,deg=True)    
        
        
    t1=d.Disp1("Painting")
    cmap = matplotlib.cm.get_cmap('Spectral')
    Umax=max(output.U)
    Tmax=max(output.T)
    norm=matplotlib.colors.Normalize(vmin=min(output.T),vmax=max(output.T))
    counter=0
    for i,index in enumerate(output.knot_indices):
        sys.stdout.write('\r'+"{:.2f}".format(counter*100/(len(output.knot_indices)))+" %\t")
        counter=counter+1
        arrow=makearrow(sidelength,output.knot_coords[i],output.Id[i])
        square=sf.makerectangle(sidelength,sidelength,output.knot_coords[i])
        if i in output.exit:
            color=[1,0,0]
        elif i in output.entrance:
            color=[1,0,1]
        elif i in output.knot_in_o:
            color=[1,1,0]
        elif i in output.knot_in_n:
            color=[0,1,0]
        elif i in output.knot_in_p:
            color=[0,0,0]    
        else:
            color=[1,0,0]
#        ps.draw(im.sheetfile,square,cmap(norm(output.T[i]))[:-1])    
        ps.draw(im.sheetfile,square,cmap(output.U[i+1]/Umax)[:-1])
        ps.draw(im.sheetfile,arrow,[1,1,1])
    d.Disp2(t1)    
#    print(output.U[0])
    print(output.U[0])
    output.im=im
    return output      
###############################################################################
def create_image(pg):
    t1=d.Disp1("Creating Image")
    im=gc.voidclass()       # creates an image of the generator
    if pg.angle==0:
        im=pg
        im.sheetfile="output_files/"+im.filename+"/"+im.filename+"_uncutlayout.eps"
        try:
            im.outfield=pg.contactfield.into
        except:
            im.outfield=pg.navigationlist[-1].exit
        try:
            im.infield=pg.contactfield.out
        except:
            im.infield=pg.navigationlist[0].entrance
        im.offset=np.array([0,0])
        im.limits=np.array([pg.sheet_full_x,pg.sheet_full_y])
        im=ps.make_simulationscreen(im)
    else:
        im.filename=pg.filename
        offset,limits=get_range(pg)
        im.sheet_full_x=limits[0]
        im.sheet_full_y=limits[1]
        im=ps.make_simulationscreen(im)
        im.elementlist=list()
        im.infield=c.translate(pg.contactfield.out,[0,pg.sheet_full_y])
        im.infield=c.rotate(im.infield,-pg.angle,[0,0])
        im.infield=c.translate(im.infield,offset)
        im.outfield=c.rotate(pg.contactfield.into,-pg.angle,[0,0])
        im.outfield=c.translate(im.outfield,offset)
        im.input_connector=c.translate(pg.input_connector,[0,pg.sheet_full_y])
        im.input_connector=c.rotate(im.input_connector,-pg.angle,[0,0])
        im.input_connector=c.translate(im.input_connector,offset)
        counter=1
        for element in pg.elementlist:
            sys.stdout.write('\r'+"{:.2f}".format(counter*100/(len(pg.elementlist)))+" %\t")
            counter=counter+1
            im_element=element
            if element.index[0]<pg.cutlinerow:
                shift=np.array([0,pg.sheet_full_y])
            else:
                shift=np.array([0,0])
            im_element.outline=image_polygon(element.outline,pg.angle,offset,shift)
            im_element.exitconnector=image_polygon(element.exitconnector,pg.angle,offset,shift)
            im_element.active_area=image_polygon(element.active_area,pg.angle,offset,shift)
            im_element.entrance=image_polygon(element.entrance,pg.angle,offset,shift)
            im_element.exit=image_polygon(element.exit,pg.angle,offset,shift)
            im_element.fieldcorners=image_polygon(element.fieldcorners,pg.angle,offset,shift)
            im_element.bottom=image_polygon(element.bottom,pg.angle,offset,shift)
            im_element.top=image_polygon(element.top,pg.angle,offset,shift)
            im_element.overlaparea_bottom=image_polygon(element.overlaparea_bottom,pg.angle,offset,shift)
            im_element.overlaparea_top=image_polygon(element.overlaparea_top,pg.angle,offset,shift)
            im_element.midpoint=image_point(element.midpoint,pg.angle,offset,shift)
            for i in range(len(im_element.stripes)):
                im_element.stripes[i]=image_polygon(im_element.stripes[i],pg.angle,offset,shift)
                im_element.contactfield_bottom[i]=image_polygon(im_element.contactfield_bottom[i],pg.angle,offset,shift)
                im_element.contactfield_top[i]=image_polygon(im_element.contactfield_top[i],pg.angle,offset,shift)
            im.elementlist.append(im_element)
#            ps.draw(im.sheetfile,im_element.outline,[0,0,0])
#            ps.draw(im.sheetfile,im_element.exitconnector,[1,0,0])    
#        inputconnector=c.translate(pg.input_connector,[0,pg.sheet_full_y])
#        inputconnector=c.rotate(inputconnector,-pg.angle,[0,0])
#        inputconnector=c.translate(inputconnector,offset)
#        ps.draw(im.sheetfile,im.input_connector,[1,0,0])
#        ps.draw(im.sheetfile,im.infield,[1,0,1])
#        ps.draw(im.sheetfile,im.outfield,[0,1,0])
#        for marker in pg.marker.firstpress.values():
#            m=c.rotate(marker,-pg.angle,[0,0])
#            ps.draw(im.sheetfile,m,[0,0,1])
        im.offset=offset
        im.limits=limits
    d.Disp2(t1)    
    return im
###############################################################################
def get_range(pg):
    min_x_value=10000000
    min_y_value=10000000
    max_x_value=0
    max_y_value=0
    inputconnector=c.translate(pg.input_connector,[0,pg.sheet_full_y])
    inputconnector=c.rotate(inputconnector,-pg.angle,[0,0])
    infield=c.translate(pg.contactfield.out,[0,pg.sheet_full_y])
    infield=c.rotate(infield,-pg.angle,[0,0])
#    outfield=c.translate(pg.contactfield.into,[0,pg.sheet_full_y])
    outfield=c.rotate(pg.contactfield.into,-pg.angle,[0,0])
    for element in pg.elementlist:
        if element.index[0]<pg.cutlinerow:
            frame=c.translate(element.outline,[0,pg.sheet_full_y])
            connector=c.translate(element.exitconnector,[0,pg.sheet_full_y])
        else:
            frame=element.outline
            connector=element.exitconnector
        frame=c.rotate(frame,-pg.angle,[0,0])
        connector=c.rotate(connector,-pg.angle,[0,0])
        min_x_value=min(min_x_value,min(frame[:,0]))
        min_y_value=min(min_y_value,min(frame[:,1]))
        min_x_value=min(min_x_value,min(connector[:,0]))
        min_y_value=min(min_y_value,min(connector[:,1]))
        max_x_value=max(max_x_value,max(frame[:,0]))
        max_y_value=max(max_y_value,max(frame[:,1]))
        max_x_value=max(max_x_value,max(connector[:,0]))
        max_y_value=max(max_y_value,max(connector[:,1]))
    min_x_value=min(min_x_value,min(inputconnector[:,0]))
    min_y_value=min(min_y_value,min(inputconnector[:,1]))
    max_x_value=max(max_x_value,max(inputconnector[:,0]))
    max_y_value=max(max_y_value,max(inputconnector[:,1]))
    min_x_value=min(min_x_value,min(infield[:,0]))
    min_y_value=min(min_y_value,min(infield[:,1]))
    max_x_value=max(max_x_value,max(infield[:,0]))
    max_y_value=max(max_y_value,max(infield[:,1]))
    min_x_value=min(min_x_value,min(outfield[:,0]))
    min_y_value=min(min_y_value,min(outfield[:,1]))
    max_x_value=max(max_x_value,max(outfield[:,0]))
    max_y_value=max(max_y_value,max(outfield[:,1]))
#    bullethole=np.array([(pg.printinglimits.minimum[0]+pg.printinglimits.maximum[0])/2,pg.sheet_full_y/2])
#    distance=[np.linalg.norm(bullethole-field.midpoint) for field in pg.fieldlist]
#    anker=pg.fieldlist[distance.index(min(distance))]
#    p1=anker.corners[0]
#    p2=anker.corners[1]
#    A = np.vstack([np.array([p1[0],p2[0]]), np.ones(len(np.array([p1[0],p2[0]])))]).T
#    m,c0=np.linalg.lstsq(A,np.array([p1[1],p2[1]]),rcond=None)[0]
    offset=-np.array([min_x_value,min_y_value])
    limits=np.array([max_x_value,max_y_value])+offset
    return offset,limits
###############################################################################
def image_polygon(polygon,angle,offset,shift):
    im_polygon=c.translate(polygon,shift)
    im_polygon=c.rotate(im_polygon,-angle,[0,0])
    im_polygon=c.translate(im_polygon,offset)
    return im_polygon
###############################################################################
def image_point(point,angle,offset,shift):
    im_point=c.translate(point,shift)
    im_point=c.rotatepoint(im_point,-angle,[0,0])
    im_point=c.translate(im_point,offset)
    return im_point
###############################################################################
def set_temperatureprofile(output,im,te,s):
    t1=d.Disp1("Setting temperatures")
    output.temperatures_y=list()
    output.temperatures_y.append(im.elementlist[0].fieldcorners[0][1])
    n=1
    while im.elementlist[0].fieldcorners[0][1]+(n-1)*te.cell_y<im.limits[1]:
        output.temperatures_y.append(im.elementlist[0].fieldcorners[0][1]+n*te.cell_y)
        n=n+1
    n=1
    while im.elementlist[0].fieldcorners[0][1]-(n-1)*te.cell_y>0:
        output.temperatures_y.append(im.elementlist[0].fieldcorners[0][1]-n*te.cell_y)
        n=n+1
    output.temperatures_y.sort()
    output.temperatures_T=np.reshape(npm.repmat([s.THatTEG,s.TCatTEG],int(len(output.temperatures_y)/2),1),-1)
    if len(output.temperatures_y)!=len(output.temperatures_T):
        output.temperatures_T=np.append(output.temperatures_T,s.THatTEG)
    output.T=list()
    for coords in output.knot_coords:
        output.T.append(np.interp(coords[1],output.temperatures_y,output.temperatures_T))    
    d.Disp2(t1)
    return output
###############################################################################
def ser(G1,G2):
    return G1*G2/(G1+G2)
###############################################################################
def makearrow(sidelength,position,angle):
    triangle=np.array([[0,-sidelength/3],[sidelength/3,0],[0,sidelength/3]])    
    return c.translate(c.rotate(triangle,angle,[0,0]),position)