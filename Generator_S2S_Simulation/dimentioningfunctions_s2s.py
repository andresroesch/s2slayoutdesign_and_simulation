# -*- coding: utf-8 -*-
"""
Created on Tue Aug 29 16:53:59 2017

@author: roesch
"""
from imp import reload 
import sys
import iodata as inout
import disp as d
import numpy as np
import globalclasses as gc
import custommathfunctions as c
import pswriter_s2s as ps 
import shapely.ops as so
import shapely.geometry as sp
import shapefunctions_s2s as sf
reload(sf)
reload(ps)
reload(c)
reload (d)
reload(gc)
reload(inout)
def adjust_angle(pg):
    t1=d.Disp1("Adjusting angle to valid input")
    if bool(pg.angle_in_degree>90) | bool(pg.angle_in_degree<-90):
        pg.angle_in_degree=np.mod(pg.angle_in_degree,180)
        if pg.angle_in_degree>90:
            pg.angle_in_degree=pg.angle_in_degree-180
    pg.angle=pg.angle_in_degree
    if pg.angle<0:
        sys.exit('NO NEGATIVE ANGLES ALLOWED')
    d.Disp2(t1)
    if pg.printer=="Roku":
        pg.angle=0
    return pg
###############################################################################
def calculate_dimensions_thermoelement_for_effective_y_optimization(pg,te,l):
    t1=d.Disp1('Setting target dimentions of thermoelements')

    te.p.stripes_x=te.p.effective_x/te.p.num_of_stripes
    te.n.stripes_x=te.n.effective_x/te.n.num_of_stripes
    te.p.x=te.p.effective_x+te.p.gapsize*(te.n.num_of_stripes-1)
    if not(te.n.num_of_stripes==1):
        if te.n.gapsize>(te.p.x-te.n.effective_x)/(te.n.num_of_stripes-1):
            te.n.gapsize=(te.p.x-te.n.effective_x)/(te.n.num_of_stripes-1)
    te.n.x=te.n.effective_x+te.n.gapsize*(te.n.num_of_stripes-1)
    te.p.cell_x=te.p.x+te.spacing_x
    te.n.cell_x=te.p.x+te.spacing_x
    d.Disp2(t1)
    return pg,te,l
######################################################################################################################
def calculate_dimensions_thermoelement(pg,te,l):
    t1=d.Disp1('Setting target dimentions of thermoelements')
    te.p.cell_x=te.p.x+te.spacing_x
    te.n.cell_x=te.n.x+te.spacing_x
    d.Disp2(t1)
    return pg,te,l
#######################################################################################################################
def calculation_of_drawing_area(pg,te):
    t1=d.Disp1('Calculating the drawing area')
    printinglimits=gc.voidclass()
    elementslimits=gc.voidclass()
    sheetlimits=gc.voidclass()
    substratelimits=gc.voidclass()
    printinglimits.minimum=np.array([pg.sheet_offset_left, pg.sheet_offset_bottom])
    printinglimits.maximum=np.array([pg.sheet_full_x-pg.sheet_offset_right, pg.sheet_full_y-pg.sheet_offset_top])
    elementslimits.minimum=printinglimits.minimum
    elementslimits.maximum=printinglimits.maximum
    sheetlimits.minimum=np.array([0, 0])
    sheetlimits.maximum=np.array([pg.sheet_full_x, pg.sheet_full_y])
    substratelimits.minimum=np.array([(pg.sheet_full_x-pg.substrate_foil_full_x)/2, 0])
    substratelimits.maximum=np.array([(pg.sheet_full_x+pg.substrate_foil_full_x)/2, pg.sheet_full_y])
    pg.addatr("sheet",np.array([[0, 0],[pg.sheet_full_x, 0],[pg.sheet_full_x,  pg.sheet_full_y],[0, pg.sheet_full_y]]))
    pg.addatr("substrate",np.array([[(pg.sheet_full_x-pg.substrate_foil_full_x)/2, 0],[(pg.sheet_full_x+pg.substrate_foil_full_x)/2, 0],[(pg.sheet_full_x+pg.substrate_foil_full_x)/2, pg.sheet_full_y],[(pg.sheet_full_x-pg.substrate_foil_full_x)/2, pg.sheet_full_y]]))
    pg.addatr("printingarea",np.array([[printinglimits.minimum[0], printinglimits.minimum[1]],[printinglimits.maximum[0], printinglimits.minimum[1]],[printinglimits.maximum[0], printinglimits.maximum[1]],[printinglimits.minimum[0], printinglimits.maximum[1]]]))
    pg.addatr("elementarea",np.array([[elementslimits.minimum[0], elementslimits.minimum[1]],[elementslimits.maximum[0], elementslimits.minimum[1]],[elementslimits.maximum[0], elementslimits.maximum[1] ],[elementslimits.minimum[0], elementslimits.maximum[1]]]))
    pg.printinglimits=printinglimits
    pg.elementslimits=elementslimits
    pg.sheetlimits=sheetlimits
    pg.substratelimits=substratelimits
    pg.earea=c.polyable(pg.printingarea)
    forbiddenstrip=c.polyable(sf.makerectangle(pg.sheet_full_x,pg.second_screen_joint_width,[pg.sheet_full_x/2,pg.second_screen_joint]))
    pg.forbiddenarea_unit3=pg.earea.intersection(forbiddenstrip)
    pg.forbiddenarea_unit2=so.cascaded_union([sp.Polygon(sf.makerectangle(pg.substrate_foil_full_x,pg.sheet_offset_bottom,[pg.sheet_full_x/2,pg.sheet_offset_bottom/2])),sp.Polygon(sf.makerectangle(pg.substrate_foil_full_x,pg.sheet_offset_top,[pg.sheet_full_x/2,pg.sheet_full_y-pg.sheet_offset_top/2]))])
#    ps.draw("output_files/"+pg.filename+"/"+pg.filename+"_rgb.eps",pg.earea_joint,[1,0,0])
    # if abs(pg.angle)>abs(c.atand(pg.sheet_full_y/(pg.printinglimits.minimum[0]-pg.printinglimits.maximum[0]))):
    #    sys.exit("ANGLE TOO BIG")
    d.Disp2(t1)  
    return pg
#######################################################################################################################################   
def correction_for_periodicity(pg,te):
    t1=d.Disp1('Adjusting cell dimentions for periodicity')
    pg.cell_target_x =(te.p.cell_x+te.n.cell_x)/2
    pg.cell_target_y =te.cell_y
    pg.cell_y =te.cell_y
    pg.cell_x =pg.cell_target_x
    # pg.number_rows="view pg.num_of_rows"
    # pg.number_columns="view pg.num_of_columns"
    # else:
    #     sys.exit("Printer not found")
    d.Disp2(t1)   
    return pg
######################################################################################################################################
def adaptation_of_cell_dimensions_for_effective_y_optimization( pg,te,l):
    t1=d.Disp1('Adjusting dimentions of thermoelements to periodic cell dimentions')
    if pg.element_of_lower_value=='n':
        pg.element_of_higher_value='p'
    elif pg.element_of_lower_value=='p':
        pg.element_of_higher_value='n'
    else:
        exit('Material either n or p')   
    te.cell_y=pg.cell_y
    te.g(pg.element_of_higher_value).effective_y=te.cell_y+pg.effective_y_optimization_y_variation_from_foldline
    if pg.connector_type==1:
        if l.linker_type==1:
            te.g(pg.element_of_lower_value).effective_y=te.cell_y-4*l.linker_height-te.spacing_y-2*te.p.overlap-2*te.n.overlap-pg.effective_y_optimization_y_variation_from_foldline    
            if te.g(pg.element_of_lower_value).effective_y<0:
                te.g(pg.element_of_lower_value).overlap=te.g(pg.element_of_lower_value).overlap+te.g(pg.element_of_lower_value).effective_y
                te.g(pg.element_of_lower_value).effective_y=0
                if te.g(pg.element_of_lower_value).overlap<0:
                    te.spacing_y=te.spacing_y+0.5*te.g(pg.element_of_lower_value).overlap
                    te.g(pg.element_of_lower_value).overlap=0
                    if te.spacing_y<0:
                         l.linker_height=l.linker_height+0.25*te.spacing_y
                         te.spacing_y=0
                         if l.linker_height<0:
                             exit("not enough space")
                         if 4*l.linker_height>te.cell_y:
                             l.linker_height=0.25*(te.cell_y-2*te.g(pg.element_of_higher_value).overlap-pg.effective_y_optimization_y_variation_from_foldline)
        if (l.linker_type==2)|(l.linker_type==3):
            overlaps=0
            if l.linker_type==3:
                overlaps=te.p.overlap+te.n.overlap
            te.g(pg.element_of_lower_value).effective_y=te.cell_y-4*pg.connector_width-te.spacing_y-overlaps
            if te.g(pg.element_of_lower_value).effective_y<0:
                te.g(pg.element_of_lower_value).overlap=te.g(pg.element_of_lower_value).overlap+te.g(pg.element_of_lower_value).effective_y
                te.g(pg.element_of_lower_value).effective_y=0
                if te.g(pg.element_of_lower_value).overlap<0:
                    te.spacing_y=te.spacing_y+0.5*te.g(pg.element_of_lower_value).overlap
                    te.g(pg.element_of_lower_value).overlap=0
                    if te.spacing_y<0:
                         exit('not enough space')
    else:
        te.g(pg.element_of_lower_value).effective_y=te.cell_y-te.spacing_y-2*te.p.overlap-2*te.n.overlap
    te.g(pg.element_of_higher_value).y=te.g(pg.element_of_higher_value).effective_y+2*te.g(pg.element_of_higher_value).overlap
    te.g(pg.element_of_lower_value).y=te.g(pg.element_of_lower_value).effective_y+2*te.g(pg.element_of_lower_value).overlap
    te.p.effective_x=pg.cell_x-te.spacing_x-te.p.gapsize*(te.p.num_of_stripes-1)
    te.n.effective_x=(2*pg.cell_x-(te.p.x-te.n.x)-2*te.spacing_x-te.p.gapsize*(te.p.num_of_stripes-1)-te.n.gapsize*(te.n.num_of_stripes-1))/(1+1/te.n_cell_x_to_p_cell_x_ratio)
    te.p.stripes_x=te.p.effective_x/te.p.num_of_stripes
    te.n.stripes_x=te.n.effective_x/te.n.num_of_stripes
    te.p.x=te.p.effective_x+te.p.gapsize*(te.p.num_of_stripes-1)
    te.n.x=te.n.effective_x+te.n.gapsize*(te.n.num_of_stripes-1)
    te.p.cell_x=pg.cell_x
    te.n.cell_x=pg.cell_x   
    d.Disp2(t1)
    return te,l
###################################################################################################################################
def adaptation_of_cell_dimensions(pg,te):
    t1=d.Disp1('Adjusting dimentions of thermoelements to periodic cell dimentions')
    te.cell_y=pg.cell_y
    te.p.y=te.cell_y-te.spacing_y
    te.n.y=te.cell_y-te.spacing_y
    te.p.effective_y=te.p.y-2*te.p.overlap
    te.n.effective_y=te.p.y-2*te.p.overlap
    te.p.effective_x=(2*pg.cell_x-2*te.spacing_x)
    te.n.effective_x=(2*pg.cell_x-2*te.spacing_x)
    te.p.stripes_x=te.p.effective_x
    te.n.stripes_x=te.n.effective_x
    te.p.x=te.p.effective_x
    te.n.x=te.n.effective_x
    te.p.cell_x=te.p.x+te.spacing_x
    te.n.cell_x=te.n.x+te.spacing_x
    d.Disp2(t1)
    return te
###############################################################################
def adaptation_of_cell_dimensions_for_no_c_material(pg,te,l):
    t1=d.Disp1('Adjusting dimentions of thermoelements to periodic cell dimentions')
    te.cell_y=pg.cell_y
    te.p.overlap=te.overlap_no_c
    te.n.overlap=te.overlap_no_c
    te.p.y=te.cell_y+te.overlap_no_c#+te.foldline_displacement
    te.n.y=te.cell_y+te.overlap_no_c#-te.foldline_displacement
    te.p.effective_y=te.p.y-2*te.overlap_no_c
    te.n.effective_y=te.n.y-2*te.overlap_no_c
    te.p.effective_x=2*pg.cell_x-2*te.spacing_x
    te.n.effective_x=2*pg.cell_x-2*te.spacing_x
    te.p.stripes_x=te.p.effective_x
    te.n.stripes_x=te.n.effective_x
    te.p.x=te.p.effective_x
    te.n.x=te.n.effective_x
    te.p.cell_x=te.p.x+te.spacing_x
    te.n.cell_x=te.n.x+te.spacing_x
    d.Disp2(t1)
    return pg,te,l