# -*- coding: utf-8 -*-
"""
Created on Tue Jul 10 15:10:48 2018

@author: andre
"""

from numba import njit, prange
import shapely.geometry as sp
import numpy as np
import disp as d
A=np.zeros([1000,2])
x=np.arange(0,20000,1)
y=np.arange(0,10000,1)
value=np.array([0,2342])
B=np.zeros([len(x)*len(y),2])
@njit(parallel=True)
def prange_test(B,x,y):
    for i in prange(len(x)):
        for j in prange(len(y)):
            B[i*len(y)+j,0]=x[i]
            B[i*len(y)+j,1]=y[j]
            
    return B
#@njit(parallel=True)
def find_index(B,value):
    res=B-value
    for i in range(len(res)):
        if not(np.sum(res[i])) and not(np.prod(res[i])):
            return i+1
    return -1
t1=d.Disp1("Parallel")
B=prange_test(B,x,y)
d.Disp2(t1)

t1=d.Disp1("Find index")
index=find_index(B,value)
d.Disp2(t1)
#def indices(B,value):
#    for i in range(B.shape[0]):
#        
#            
#            
#    return index

#t1=d.Disp1("Series")
#x=0
#for i,a in enumerate(B):
#    B[i]=a+1
#d.Disp2(t1)
#t1=d.Disp1("direct")
#C=C+1
#d.Disp2(t1)