# -*- coding: utf-8 -*-
"""
Created on Tue Mar  6 11:27:39 2018

@author: andres
"""
import imp
import globalclasses as gc
import custommathfunctions as c
import numpy as np
import shelve

T='Hiya'
val=[1,2,3]

filename='selftest'
my_shelf = shelve.open(filename,'n') # 'n' for new
my_shelf["T"]=T
my_shelf["val"]=val
my_shelf.close()
del T,val
my_shelf = shelve.open(filename)
for key in my_shelf:
    globals()[key]=my_shelf[key]
my_shelf.close()

print(T)

print(val)
