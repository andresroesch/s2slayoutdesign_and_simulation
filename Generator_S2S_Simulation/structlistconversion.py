# -*- coding: utf-8 -*-
"""
Created on Mon Aug 21 13:56:40 2017

@author: roesch
"""
from imp import reload 
import numpy as np
import globalclasses as gc
reload(gc);
def oneline(expr):
    if isinstance(expr,str):
        return expr
    elif isinstance(expr,np.ndarray):
#        print(expr.shape);
        dim=expr.shape;
        string="";
        if len(dim)==0:
            string=str(expr);
        elif len(dim)==1:
            for i in range(dim[0]):
                 string=string+str(expr[i])+",";
            string=string[:-1];
        else:
            for i in range(dim[0]):
                for j in range (dim[1]):
                    string=string+str(expr[i,j])+",";
                string=string[:-1]; 
                string=string+";";
            string=string[:-1];    
        string="["+string+"]";
        return string
    else:
        npexpr=np.array(expr)
        return oneline(npexpr)
def structlist(struct):
    listing=[];
    if isinstance(struct,list):
        print ("list")
    elif isinstance(struct.__dict__, dict):
        for key in struct.__dict__.keys():
            if isinstance(struct.__dict__[key],gc.voidclass):
                temp_list=structlist(struct.__dict__[key]);
                listing.append([key,"..."])
                for i in range(len(temp_list)):
                    listing.append(temp_list[i])
                listing.append([key,".."])
            else:
                listing.append([key,oneline(struct.__dict__[key])])
    return listing


def deliststruct(listing):
    level=0;
    lev=list();
    for i in range(len(listing)-1):
        lev.append(level);
        if "..." in listing[i][1]:
            level=level+1;
        elif (".." in listing[i+1][1]) & ("..." not in listing[i+1][1]):
            level=level-1;     
    lev.append(0);
    startindices=list();
    startlevel=list();
    endindices=list();
    for i in range(max(lev),0,-1):
        elementindices=[j for j, e in enumerate(lev) if e==i];
        for j in elementindices:
            if lev[j-1]==i-1:
                startindices.append(j);
                startlevel.append(i);
            if lev[j+1]==i-1:
                endindices.append(j)
    i=1       
    struct=gc.voidclass();     
    while i<=len(listing)-2:   
        if "[" in listing[i][1]:
             expression=listing[i][1];
             expression=expression[1:-1]
             if ";" in expression:
                 struct.addatr(listing[i][0],"matrix");
             elif "," in expression:
                 struct.addatr(listing[i][0],np.array([float(j) for j in expression.split(",")]));
             else:
                 struct.addatr(listing[i][0],float(expression)); 
        elif "..." in listing[i][1]:
            sublisting=listing[i:endindices[startindices.index(i+1)]+2];
            struct.addatr(listing[i][0],deliststruct(sublisting));
            i+=len(sublisting)-1;
        else:
            struct.addatr(listing[i][0],listing[i][1]); 
        i+=1;
    return struct


def m2pysyntax(i_string):
    o_string="go"
    return o_string