# -*- coding: utf-8 -*-
"""
Created on Sun Jul 22 13:02:38 2018

@author: andre
"""

import numpy as np 
from numba import njit, prange

#def in_g(x,values): 
 
#        y[i]=y0*y1
#    return y
x=np.array([[0,0],[0,1],[1,1],[1,0]])
neighbors=np.array([[[0,0],[-1,1],[1,1]],[[0,0],[-1,1],[1,1]]])

y=np.zeros([neighbors.shape[0],neighbors.shape[1]],dtype=bool)
@njit(parallel=True)
def in_g(x,y,values):
    for i in prange(values.shape[0]):
        for j in prange(values.shape[1]):    
            x0=x[:,0]
            x1=x[:,1]
            y0=x0==neighbors[i][j][0]
            y1=x1==neighbors[i][j][1]
            yi=y0*y1
            y[i][j]=yi.any()
    y=y[:,:-1]
    return y

y=in_g(x,y,neighbors)