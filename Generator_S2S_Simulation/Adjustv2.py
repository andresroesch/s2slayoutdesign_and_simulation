# -*- coding: utf-8 -*-
"""
Created on Thu May 24 11:57:22 2018

@author: andre
"""

from imp import reload 
import numpy as n
import os
import iodata as inout
import time
import mainfunctions as m
#import globalclasses as gc
#import pswriter as ps 
#import simulation as sim
#import disp as d
from datetime import datetime 
starttime = time.time()

WIDTHs=n.linspace(0.002,0.01,20)

#for width in WIDTHs:
newfilename=datetime.now().strftime("%y%m%d")+"_ARO_Mofasser"

oldfilename="PEDOTTiS2211217"


if not(os.path.exists("output_files")):
    os.mkdir("output_files")
if not(os.path.exists("output_files\\"+newfilename)):
    os.mkdir("output_files\\"+newfilename)
inputdata=inout.read_input("output_files\\"+oldfilename+"\\"+oldfilename+".txt")
pg=inputdata["p"]
te=inputdata["te"]
l=inputdata["l"]
s=inputdata["s"]
del inputdata

pg.filename=newfilename
#pg.periodic_enlargement=n.array(0)
#pg.comment="Uebermass in PDF wegen Belichtung 0.15mm. Ueberlapp von p und n 0.3 mm aufgrund von 0.05 bis 0.1 mm Randeinfransung und 0.1mm Sicherheit. 0.85 Gapsize resuliter in 1mm Gap wegen Siebbelichtung."

#pg.printer="Gallus"
#pg.material_unit1="c"
#pg.material_unit2="p"
#pg.material_unit3="n"
#pg.shrinkfactor_unit1=n.array(0.0)
#pg.shrinkfactor_unit2=n.array(0.0)
#pg.shrinkfactor_unit3=n.array(0.0)
#pg.connectormaterial="p"
###DEFINITION PAGE
#pg.sheet_full_y=n.array(400e-3)
#pg.sheet_full_x=n.array(307e-3)
#pg.substrate_foil_full_x=n.array(307e-3)
#
###ANGLE
#pg.angle_in_degree=n.array(45)
#
### DEFINITION PRINTING AREA
#pg.sheet_offset_bottom=n.array(10e-3)
#pg.sheet_offset_top=n.array(10e-3)        
#pg.sheet_offset_left=n.array(20e-3)        
#pg.sheet_offset_right="auto"#n.array(30e-3)
#pg.second_screen_joint=n.array(80e-3)
#pg.second_screen_joint_width=n.array(5e-3)
### SPECIAL SETTINGS
#pg.effective_y_optimization=n.array(0)
#pg.element_of_lower_value="p"
#pg.startingpoint=n.array([0,0])
#pg.no_c_material_between_elements=n.array(1)
#pg.first_field_type="n"
#pg.write_indicies=n.array(0)
#pg.effective_y_optimization_y_variation_from_foldline=n.array(0e-3)
##pg.shrinkfactor=n.array(0.02)
#pg.wrap_rod_diameter=n.array(10e-3)
#pg.flip_inputconnector=n.array(1)
#pg.draw_window=n.array(0)
#pg.setmarkers=n.array(1)
#pg.write_infobox=n.array(0)
#pg.extra_rows_of_cutline=n.array(3)
#pg.include_contactfield=n.array(1)
#pg.print_numeration=n.array(0)
#pg.marker_line_y=n.array([0.01,0.15,0.25])
#pg.abs_width=n.array(3e-3)
#pg.abs_offset=n.array(2e-3)
#
### Printing SETTINGS
##pg.shrinkfactor=n.array(0.02)
#pg.printdirection=n.array(1)
#
#
###MARKER PROPERTIES
#pg.marker_line_x=n.array(14e-3)
#pg.marker_line_y=n.array([0.02,0.12,0.22])
#pg.marker_width=n.array(4e-3)
#pg.marker_space_outer=n.array(1e-3)
#pg.marker_space_inner=n.array(1e-3)
#pg.sheet_marker_offset_left=n.array(13e-3)
#pg.sheet_marker_offsets=n.array(20e-3)
#pg.sheet_marker_offset_right=n.array(13e-3)
#pg.circle_marker_outerradius=n.array(2e-3)
#pg.circle_marker_linewidth=n.array(0.2e-3)
#pg.circle_marker_crosswidth=n.array(0.2e-3)
#pg.abs_width=n.array(3e-3)
###TEXTBOX
#pg.info_box_x_u1=n.array(pg.marker_line_x)
#pg.info_box_y_u1=n.array(53e-3)
#pg.info_box_x_u2=n.array(pg.marker_line_x)
#pg.info_box_y_u2=n.array(153e-3)
#pg.info_box_x_u3=n.array(pg.marker_line_x)
#pg.info_box_y_u3=n.array(253e-3)
#pg.info_box_x=n.array(pg.marker_line_x+1e-3)
#pg.info_box_y=n.array(318e-3)
#pg.info_box_fontsize=n.array(10)
#pg.info_box_font='Arial'
#
### TESTPADS
#pg.padradius=n.array(2.5e-3)
#pg.pad_length=n.array(50e-3)
#pg.pad_aspect_ratio=n.array(100)
#pg.pad_ypositions_offset=n.array(56e-3)
#
#pg.controlbar_length=n.array(0e-3)
#pg.controlbar_width=n.array(0e-3)
#pg.controlbar_position=n.array([37e-3,92e-3])
###FOLD MARKERS 
#pg.foldmarker_width=n.array(0.15e-3)
#pg.foldmarker_length=n.array(pg.wrap_rod_diameter*n.pi+1e-3)#  #pg.wrap_rod_diameter*n.pi Length of the folding markers
#pg.foldmarker_spacing_right=n.array(9.5e-3)  # Padding from the right sheet border
#pg.foldmarker_spacing_left=n.array(0e-3)

#pg.set_intermediate_foldmarkers=n.array(0)
#pg.set_intermediate_foldmarkers=n.array(1)
#pg.intermediate_foldmarker_length=n.array(0.9e-3) # if foldmarker legth is not defined or 0 it will fall back to the old foldmarker length definition

#
### CONTACTFIELD
#pg.contactfield_spacing_right=n.array(7e-3)
#pg.contactfield_spacing_left=n.array(2e-3)
#pg.space_between_last_connector_and_last_row=n.array(2e-3)
#pg.reduce_contactfield=n.array(2.5)
#pg.window_width=n.array(5e-3)
#pg.window_length=n.array(pg.wrap_rod_diameter*n.pi)#20e-3 
#
### COLORS
#pg.color_n=n.array([0.4 ,0.8, 0.4])
#pg.color_c=n.array([0.1, 0.1, 1])
#pg.color_p=n.array([0, 0, 0.2])
#pg.color_drawingarea=n.array([0.60, 0.60, 0.60])
#pg.color_elementarea=n.array([0, 1, 0])
#pg.color_design=n.array([0, 0, 0])
#
### DIMENSIONS OF THERMOELEMENT - TARGETS
#
##-- GENERAL PROPERTIES
#te.cell_y=n.array(4.5e-3)
#te.spacing_x=n.array(7e-3)
#te.spacing_y=n.array(0)  # should not be zero if connector_type ~=1
#te.n_cell_x_to_p_cell_x_ratio=n.array(1/5) #te.n_element_x_to_p_element_x_ratio=1/5
#te.dimensionsgiven=n.array(1)   # 1 -- p.effective_x n.effective_x  2 -- p.effective_x  n_cell_x_to_p_cell_x_ratio 3 -- n.effective_x  n_cell_x_to_p_cell_x_ratio
#te.overlap_no_c=n.array(0.45e-3)

#te.cell_y=n.array(5e-3)
#te.spacing_x=n.array(4e-3)
#te.spacing_y=n.array(0)  # should not be zero if connector_type ~=1
#te.n_cell_x_to_p_cell_x_ratio=n.array(1/5) #te.n_element_x_to_p_element_x_ratio=1/5
#te.dimensionsgiven=n.array(1)   # 1 -- p.effective_x n.effective_x  2 -- p.effective_x  n_cell_x_to_p_cell_x_ratio 3 -- n.effective_x  n_cell_x_to_p_cell_x_ratio
#te.overlap_no_c=n.array(0.30001e-3)
#te.foldline_displacement=n.array(0e-3)
##-- P-Element
#
#te.p.overlap=n.array(0.4e-3)
#te.p.effective_x=n.array(width)
#te.p.num_of_stripes=n.array(1)
#te.p.gapsize=n.array(0.1e-3)
#
##-- N-Element
#
#te.n.overlap=n.array(0.4e-3)
#te.n.effective_x=n.array(width)
#te.n.num_of_stripes=n.array(1)
#te.n.gapsize=n.array(0.1e-3)
##-- L-Element
#pg.connector_type=n.array(2)
#l.linker_type=n.array(2)
#l.linker_height=n.array(4e-3)
#l.linker_length=n.array(4e-3)
#
#pg.connector_width=n.array(4e-3)
#pg.connectorspacing=n.array(0e-3)
#pg.adapt_n_width_to_connector_width=n.array(1)
#pg.jump_connectors_are_turn_connectors=n.array(1) # always 1
#
### SIMULATION PARAMETERS
#s.model=n.array(3)      # 1-- Simple Model 2-- Good Model
#s.scenario="TEGmeasurementsetupLTI"
#s.p_material="PEDOTNanowires"
#s.n_material="TiS2"
#s.c_material="PEDOTNanowires"
#s.substrate_material="PEN"
#s.precision=n.array(1000)
#s.v_factor=n.array(1)
#
#s.n_thickness=n.array(8e-6)
#s.p_thickness=n.array(2.5e-6)
#s.c_thickness=n.array(2.5e-6)
#s.air_thickness=n.array(1e-6)
#s.substrate_thickness=n.array(6e-6)
inout.save_output(pg,te,l,s,"txt")
pg,te,l=m.generate("output_files\\"+pg.filename+"\\"+pg.filename+".txt")
m.drawing_generator(pg.filename)
os.startfile("C:\Python_wdir\Generator_PY\output_files/"+pg.filename+"/"+pg.filename+"_rgb.eps")
endtime = time.time()
print("Elapsed time is "+str(endtime - starttime)+" seconds.") 