# -*- coding: utf-8 -*-
"""
Created on Fri Aug 25 15:34:13 2017

@author: roesch
"""
from imp import reload
import numpy as np
import custommathfunctions as c
reload(c)
def up(angle):
#    print(np.array([-c.sind(angle), c.cosd(angle)]))
    return np.array([-c.sind(angle), c.cosd(angle)])
def down(angle):
#    print(np.array([c.sind(angle), -c.cosd(angle)]))
    return np.array([c.sind(angle), -c.cosd(angle)])
def left(angle):
#    print(np.array([-c.cosd(angle), -c.sind(angle)]))
    return np.array([-c.cosd(angle), -c.sind(angle)])
def right(angle):
#    print(np.array([c.cosd(angle), c.sind(angle)]))
    return np.array([c.cosd(angle), c.sind(angle)])