# -*- coding: utf-8 -*-
"""
Created on Thu Jul  5 21:23:21 2018

@author: Andres
"""

from imp import reload 
from numba import njit, prange
import numpy as n
import os
import sys
import iodata as inout
import time
import mainfunctions as m
import globalclasses as gc
import pswriter as ps 
import simulation as sim
import disp as d
reload(d)
reload(sim)
reload(ps)
reload(gc)
reload(m)
reload(inout)
starttime = time.time()

folddepths=n.linspace(0.0005,0.01,20)
elementwidths=n.linspace(0.0005,0.01,20)
overlaps=n.linspace(0.0001,0.001,20)
spaces=n.linspace(0.0005,0.02,20)
#counter=0
@njit(parallel=True)
def create_layouts(folddepths,elementwidths,overlaps,spaces):    
    for i in prange(folddepths.shape[0]):
        for j in prange(elementwidths.shape[0]):
            for k in prange(overlaps.shape[0]):
                for l in prange(spaces.shape[0]):
                    folddepth=folddepths[i]
                    elementwidth=elementwidths[j]
                    overlap=overlaps[k]
                    space=spaces[l]
                    pg=gc.voidclass()
                    te=gc.voidclass()
                    te.p=gc.voidclass()
                    te.n=gc.voidclass()
                    l=gc.voidclass()
                    s=gc.voidclass()
    #                counter+=1
    #                sys.stdout.write('\r'+"{:.2f}".format(counter*100/(20**4))+" %\t")
                    ## DEFINITION FILENAME
                    pg.filename="Gallus"+"fd"+str(round(folddepth*1000,3)).replace(".","_")+"-ew"+str(round(elementwidth*1000,3)).replace(".","_")+"-ov"+str(round(overlap*1000,3)).replace(".","_")+"-sx"+str(round(space*1000,3)).replace(".","_")
                    pg.printer="Gallus" # Gallus or Roku
                    pg.material_unit1="c"
                    pg.material_unit2="p"
                    pg.material_unit3="n"
                    pg.shrinkfactor_unit1=n.array(0.0)
                    pg.shrinkfactor_unit2=n.array(0.0)
                    pg.shrinkfactor_unit3=n.array(0.0)
                    pg.connectormaterial="p"
                    ##DEFINITION PAGE
                    pg.sheet_full_y=n.array(18*25.4e-3)#18*25.4e
                    pg.sheet_full_x=n.array(330e-3)
                    pg.substrate_foil_full_x=n.array(315e-3)
                    
                    ##ANGLE
                    pg.angle_in_degree=n.array(45)
                    
                    ## DEFINITION PRINTING AREA
                    pg.sheet_offset_bottom=n.array(3e-3)
                    pg.sheet_offset_top=n.array(3e-3)        
                    pg.sheet_offset_left=n.array(32.5e-3)        
                    pg.sheet_offset_right="auto"#"auto"#n.array(30e-3)
                    pg.second_screen_joint=n.array(80e-3)
                    pg.second_screen_joint_width=n.array(6e-3)
                    
                    ## SPECIAL SETTINGS
                    pg.effective_y_optimization=n.array(0)
                    pg.element_of_lower_value="p"
                    pg.startingpoint=n.array([0,0])
                    pg.no_c_material_between_elements=n.array(1)
                    pg.first_field_type="p"
                    pg.write_indicies=n.array(0)
                    pg.print_numeration=n.array(0)
                    pg.effective_y_optimization_y_variation_from_foldline=n.array(0e-3)
                    #pg.shrinkfactor=n.array(0.02)
                    pg.wrap_rod_diameter=n.array(10e-3)
                    pg.flip_inputconnector=n.array(1)
                    pg.draw_window=n.array(0)
                    pg.setmarkers=n.array(0)
                    pg.write_infobox=n.array(0)
                    pg.extra_rows_of_cutline=n.array(3)
                    pg.include_contactfield=n.array(1)
                    
                    ## Printing SETTINGS
                    #pg.shrinkfactor=n.array(0.02)
                    pg.printdirection=n.array(1)
                    
                    
                    ##MARKER PROPERTIES
                    pg.marker_line_x=n.array(18.5e-3)
                    pg.marker_line_y=n.array([0,0.14,0.24])
                    pg.marker_width=n.array(4e-3)
                    pg.marker_space_outer=n.array(1e-3)
                    pg.marker_space_inner=n.array(1e-3)
                    pg.sheet_marker_offset_left=n.array(17.5e-3)
                    pg.sheet_marker_offsets=n.array(20e-3)
                    pg.sheet_marker_offset_right=n.array(13e-3)
                    pg.circle_marker_outerradius=n.array(2e-3)
                    pg.circle_marker_linewidth=n.array(0.2e-3)
                    pg.circle_marker_crosswidth=n.array(0.2e-3)
                    pg.abs_width=n.array(5e-3)
                    ##TEXTBOX
                    pg.info_box_x=n.array(1e-2)
                    pg.info_box_y=n.array(1e-2)
                    pg.info_box_fontsize=n.array(12)
                    pg.info_box_font='Times-Roman'
                    
                    ## TESTPADS
                    pg.padradius=n.array(2.5e-3)
                    pg.pad_length=n.array(50e-3)
                    pg.pad_aspect_ratio=n.array(100)
                    pg.pad_ypositions_offset=n.array(56e-3)
                    
                    ##FOLD MARKERS
                    pg.foldmarker_width=n.array(0.15e-3)
                    pg.foldmarker_length=n.array(pg.wrap_rod_diameter*n.pi+1e-3)#  #pg.wrap_rod_diameter*n.pi Length of the folding markers
                    pg.foldmarker_spacing_right=n.array(9.5e-3)  # Padding from the right sheet border
                    pg.foldmarker_spacing_left=n.array(0e-3)
                    
                    ## CONTACTFIELD
                    pg.contactfield_spacing_right=n.array(10e-3)
                    pg.contactfield_spacing_left=n.array(0e-3)
                    pg.reduce_contactfield=n.array(4)
                    pg.space_between_last_connector_and_last_row=n.array(0e-3)
                    pg.window_width=n.array(5e-3)
                    pg.window_length=n.array(pg.wrap_rod_diameter*n.pi)#20e-3 
                    
                    ## COLORS
                    pg.color_n=n.array([0.4 ,0.8, 0.4])
                    pg.color_c=n.array([246/255, 52/255, 63/255])
                    pg.color_p=n.array([0, 0, 0.2])
                    pg.color_drawingarea=n.array([0.60, 0.60, 0.60])
                    pg.color_elementarea=n.array([0, 1, 0])
                    pg.color_design=n.array([0, 0, 0])
                    
                    ## DIMENSIONS OF THERMOELEMENT - TARGETS
                    
                    #-- GENERAL PROPERTIES
                    te.cell_y=n.array(5e-3)
                    te.spacing_x=n.array(22e-3)
                    te.spacing_y=n.array(0)  # should not be zero if connector_type ~=1
                    te.n_cell_x_to_p_cell_x_ratio=n.array(1/5) #te.n_element_x_to_p_element_x_ratio=1/5
                    te.dimensionsgiven=n.array(1)   # 1 -- p.effective_x n.effective_x  2 -- p.effective_x  n_cell_x_to_p_cell_x_ratio 3 -- n.effective_x  n_cell_x_to_p_cell_x_ratio
                    te.overlap_no_c=overlap#n.array(1e-3)
                    te.foldline_displacement=n.array(0e-3)
                    #-- P-Element
                    
                    te.p.overlap=n.array(0.4e-3)
                    te.p.effective_x=n.array(5e-3)
                    te.p.num_of_stripes=n.array(1)
                    te.p.gapsize=n.array(0.1e-3)
                    
                    #-- N-Element
                    
                    te.n.overlap=n.array(0.4e-3)
                    te.n.effective_x=n.array(5e-3)
                    te.n.num_of_stripes=n.array(1)
                    te.n.gapsize=n.array(0.1e-3)
                    #-- L-Element
                    pg.connector_type=n.array(4)
                    l.linker_type=n.array(2)
                    l.linker_height=n.array(4e-3)
                    l.linker_length=n.array(0e-3)
                    
                    pg.connector_width=n.array(7e-3)
                    pg.connectorspacing=n.array(0e-3)
                    pg.adapt_n_width_to_connector_width=n.array(1)
                    pg.jump_connectors_are_turn_connectors=n.array(1) # always 1
                    
                    ## SIMULATION PARAMETERS
                    s.model=n.array(3)      # 1-- Simple Model 2-- Good Model
                    s.scenario="TEGmeasurementsetupLTI"
                    s.p_material="PEDOTNanowires_ref"
                    s.n_material="TiS2_ref"
                    s.c_material="PEDOTNanowires_ref"
                    s.substrate_material="PEN"
                    s.precision=n.array(1000)
                    s.v_factor=n.array(1)
                    
                    s.n_thickness=n.array(1.5e-6)
                    s.p_thickness=n.array(2e-6)
                    s.c_thickness=n.array(2e-6)
                    s.air_thickness=n.array(1e-6)
                    s.substrate_thickness=n.array(1.4e-6)
                    
                    #s.n.el_conductivity=n.array(2.4e4)
                    #s.p.el_conductivity=n.array(4.4e4)
                    #s.c.el_conductivity=n.array(4.4e4)
                    #s.air_el_conductivity=n.array(0)
                    #s.substrate.el_conductivity=n.array(0)
                    #s.n.th_conductivity=n.array(0.7)
                    #s.p.th_conductivity=n.array(0.34)
                    #s.c.th_conductivity=n.array(0.34)
                    #s.air_th_conductivity=n.array(0.026)
                    #s.substrate.th_conductivity=n.array(0.25)
                    #s.n.seebeck=n.array(-92e-6)
                    #s.p.seebeck=n.array(2e-5)
                    #s.c.seebeck=n.array(2e-5)
                    #s.air_seebeck=n.array(0)
                    #s.substrate.seebeck=n.array(0)
                    #s.n.cost=n.array(500)
                    #s.p.cost=n.array(500)
                    #s.c.cost=n.array(500)
                    #s.air.cost=n.array(0)
                    #s.substrate.cost=n.array(0)
                    #s.n.solid_in_ink_percentage=n.array(0.01)
                    #s.p.solid_in_ink_percentage=n.array(0.03)
                    #s.c.solid_in_ink_percentage=n.array(0.01)
                    #s.air.solid_in_ink_percentage=n.array(0)
                    #s.substrate.solid_in_ink_percentage=n.array(0)
                    s.contact_resistance_pn=n.array(0)
                    s.contact_resistance_cp=n.array(0)
                    s.contact_resistance_cn=n.array(0)
#                    if not(os.path.exists("output_files")):
#                        os.mkdir("output_files")
#                    if not(os.path.exists("output_files\\"+pg.filename)):
#                        try:
#                            os.mkdir("output_files\\"+pg.filename)
#                            inout.save_output(pg,te,l,s,"txt")
#                            m.generate("output_files\\"+pg.filename+"\\"+pg.filename+".txt")
#                            files=os.listdir("output_files/"+pg.filename)
#                            for file in files:     
#                                if ".eps" in file:
#                                    try:
#                                        os.remove("output_files/"+pg.filename+"/"+file)
#                                        print(file+" deleted")
#                                    except:
#                                        print(file+" not deleted")
#                        except:
#                            pass
    return True

create_layouts(folddepths,elementwidths,overlaps,spaces)    
#                m.drawing_generator(pg.filename)
#pg,te,l=m.generate("output_files\\"+pg.filename+"\\"+pg.filename+".txt")
#m.drawing_generator(pg.filename)
#if simulate:
#                pg,te,l,output=sim.simulate_generator(pg.filename,s)
#endtime = time.time()
#os.startfile("C:\Python_wdir\Generator_PY\output_files/"+pg.filename+"/"+pg.filename+"_rgb.eps")
#print("Elapsed time is "+str(endtime - starttime)+" seconds.") 
#if simulate:
#                inout.write_outputfile(output)
#    d.display_output(output)