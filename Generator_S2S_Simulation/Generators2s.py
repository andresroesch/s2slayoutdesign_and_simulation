# -*- coding: utf-8 -*-
"""
Created on Tue Aug 15 16:54:15 2017

@author: roesch

oTEGs s2s nonperiodic

"""
from imp import reload 
import numpy as np
import os
import iodata as inout
import time
import mainfunctions_s2s as m
import globalclasses as gc
import pswriter_s2s as ps 
import simulation_s2s as sim
import disp as d
from datetime import datetime 
reload(d)
reload(sim)
reload(ps)
reload(gc)
reload(m)
reload(inout)
starttime = time.time()
pg=gc.voidclass()
te=gc.voidclass()
te.p=gc.voidclass()
te.n=gc.voidclass()
l=gc.voidclass()
s=gc.voidclass()

simulate=0
## DEFINITION FILENAME
pg.filename="Simulationsbauer"#datetime.now().strftime("%y%m%d")+"_ARO_Mofasser"
pg.material_unit1="c"
pg.material_unit2="p"
pg.material_unit3="n"
pg.connectormaterial="p"
##DEFINITION PAGE
pg.sheet_full_y=np.array(170e-3)#18*25.4e
pg.sheet_full_x=np.array(160e-3)
pg.substrate_foil_full_x=np.array(164e-3)

## DEFINITION PRINTING AREA
pg.sheet_offset_bottom=np.array(11e-3)
pg.sheet_offset_top=np.array(10e-3)        
pg.sheet_offset_left=np.array(1e-3)        
pg.sheet_offset_right=np.array(1e-3)#"auto"#n.array(0.036941801051529465)
pg.second_screen_joint=np.array(80e-3)
pg.second_screen_joint_width=np.array(6e-3)

## SPECIAL SETTINGS
pg.with_effective_y_optimization=np.array(0)
pg.element_of_lower_value="p"
pg.startingpoint=np.array([0,0])
pg.with_connector_material=np.array(1)
pg.first_field_type="p"
pg.write_indicies=np.array(0)
pg.print_numeration=np.array(0)
pg.periodic_enlargement=np.array(0)

## COLORS
pg.color_n=np.array([0.4 ,0.8, 0.4])
pg.color_c=np.array([246/255, 52/255, 63/255])
pg.color_p=np.array([0, 0, 0.2])
pg.color_drawingarea=np.array([0.60, 0.60, 0.60])
pg.color_elementarea=np.array([0, 1, 0])
pg.color_design=np.array([0, 0, 0])

## DIMENSIONS OF THERMOELEMENT - TARGETS

#-- GENERAL PROPERTIES
pg.delta_element_x=np.array(0e-3)
te.cell_y=np.array(5e-03)
te.spacing_x=np.array(2e-3)
te.spacing_y=np.array(2e-3)
te.overlap_no_c=np.array(1e-3)
#-- P-Element
te.p.overlap=np.array(0.5e-3)
te.p.x=np.array(1.015380e-02)

#-- N-Element
te.n.overlap=np.array(0.5e-3)
te.n.x=np.array(2e-03)
#-- L-Element
# pg.connector_type=n.array(4)
l.linker_type=np.array(2)
l.linker_height=np.array(10e-3)
# l.linker_length=n.array(0e-3)

pg.connector_width=np.array(10e-3)
pg.connectorspacing=np.array(0e-3)

## SIMULATION PARAMETERS
s.scenario="TEGmeasurementsetupLTI"
s.p_material="PEDOTNanowires_SL"
s.n_material="TiS2_SL"
s.c_material="PEDOTNanowires_SL"
s.substrate_material="PEN"
s.precision=np.array(1000)
s.v_factor=np.array(1)
s.n_wetfilm_thickness=np.array(40e-6)
s.c_wetfilm_thickness=np.array(40e-6)
s.p_wetfilm_thickness=np.array(40e-6)
s.n_thickness=np.array(2*3.6e-6)
s.p_thickness=np.array(2*2.1e-6)
s.c_thickness=np.array(2e-6)
s.air_thickness=np.array(1e-6)
s.substrate_thickness=np.array(4e-6)

s.contact_resistance_pn=np.array(0)
s.contact_resistance_cp=np.array(0)
s.contact_resistance_cn=np.array(0)
if not(os.path.exists("output_files")):
    os.mkdir("output_files")
if not(os.path.exists("output_files\\"+pg.filename)):
    os.mkdir("output_files\\"+pg.filename)

inout.save_output(pg,te,l,s,"txt")
pg,te,l=m.generate("output_files\\"+pg.filename+"\\"+pg.filename+".txt")
m.drawing_generator(pg.filename)
if simulate:
    pg,te,l,output=sim.simulate_generator(pg.filename,s)
endtime = time.time()
time.sleep(1)
os.startfile(os.getcwd()+"/output_files/"+pg.filename+"/"+pg.filename+"_rgb.pdf")
print("Elapsed time is "+str(endtime - starttime)+" seconds.") 
if simulate:
    inout.write_outputfile(output)
    d.display_output(output)