# -*- coding: utf-8 -*-
"""
Created on Mon Jun 25 19:38:58 2018

@author: andres
"""
#import os
#import subprocess
#
#subprocess.Popen("putty hs9303@uc1.scc.kit.edu",shell=True)
#subprocess.Popen("mkdir testssh",shell=True)
#subprocess.Popen("exit",shell=True)
#import paramiko

#!/usr/bin/python

# All SSH libraries for Python are junk (2011-10-13).
# Too low-level (libssh2), too buggy (paramiko), too complicated
# (both), too poor in features (no use of the agent, for instance)

# Here is the right solution today:
import paramiko
from imp import reload
import os
import disp as d
import time
reload(d)
from scp import SCPClient
starttime = time.time()
startjobfile="start_job_in_virtual_environment"
working_directory="TEG"
startjobcontent="""module load devel/python/3.5.2
cd """+working_directory+"""
python -m venv virtualenvironment
source virtualenvironment/bin/activate
pip install --upgrade pip
pip list 
pip install shapely
pip install numpy
pip install time
pip install PIL
pip install matplotlib
python testscript.py 
deactivate
rm -rf virtualenvironment
touch jobterminated"""

waitjobfile="waitjob"
waitjobcontent="""cd """+working_directory+"""
while [ ! -f jobterminated ]
do
sleep 1
done
cd
rm -r """+working_directory

sjfh=open(startjobfile+".txt","w")
sjfh.write(startjobcontent)
sjfh.close()
gjfh=open(waitjobfile+".txt","w")
gjfh.write(waitjobcontent)
gjfh.close()

t1=d.Disp1("Copying files to uc1")
nbytes = 4096
hostname = 'uc1.scc.kit.edu'
port = 22
username = 'hs9303'
memory="20000mb" 
walltime="1:00:00"
key=paramiko.RSAKey.from_private_key_file("C:/keys/uc1.pem")
ssh = paramiko.SSHClient()
ssh.load_system_host_keys()
ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
ssh.connect(hostname=hostname,username=username,pkey=key)
ssh.exec_command("rm "+working_directory+"/"+startjobfile)
ssh.exec_command("rm "+working_directory+"/"+waitjobfile)
ssh.exec_command("rm "+working_directory+"/"+"jobterminated")
ssh.exec_command("mkdir "+working_directory)
with SCPClient(ssh.get_transport()) as scp:
    scp.put(startjobfile+".txt", working_directory+"/"+startjobfile+".sh")
    scp.put(waitjobfile+".txt", working_directory+"/"+waitjobfile+".sh")
    scp.put("testscript.py", working_directory+"/"+"testscript.py")
ssh.exec_command("dos2unix "+working_directory+"/"+startjobfile+".sh")
ssh.exec_command("dos2unix "+working_directory+"/"+waitjobfile+".sh")
ssh.close()
d.Disp2(t1)

client = paramiko.Transport((hostname, port))
client.connect(username=username,pkey=key)
t1=d.Disp1("Submitting job to uc1")
stdout_data = []
stderr_data = []
session = client.open_channel(kind='session')
session.exec_command("msub -q singlenode -N test -l nodes=1:ppn=1,walltime="+walltime+",pmem="+memory+" "+working_directory+"/"+startjobfile+".sh")



while True:
    if session.recv_ready():
        stdout_data.append(session.recv(nbytes))
    if session.recv_stderr_ready():
        stderr_data.append(session.recv_stderr(nbytes))
    if session.exit_status_ready():
        break
if not stdout_data:
    job_number="-1"
    print("Job not submitted")
    print(stderr_data)
    raise SystemError
else:
    job_number=str(int(stdout_data[0]))
    jobfilename="job_uc1_"+job_number+".out"
session.close()
d.Disp2(t1)

t1=d.Disp1("Waiting for job "+job_number)
waitout_data = []
waiterr_data = []
session = client.open_channel(kind='session')
session.exec_command("bash "+working_directory+"/"+waitjobfile+".sh")
while True:
    if session.recv_ready():
        waitout_data.append(session.recv(nbytes))
    if session.recv_stderr_ready():
        waiterr_data.append(session.recv_stderr(nbytes))
    if session.exit_status_ready():
        break

#print ('exit status: ', session.recv_exit_status())
#print (waitout_data)
#print (waiterr_data)
session.close()
client.close()
d.Disp2(t1)
t1=d.Disp1("Copying job "+job_number+" to local")
ssh = paramiko.SSHClient()
ssh.load_system_host_keys()
ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
ssh.connect(hostname=hostname,username=username,pkey=key)
with SCPClient(ssh.get_transport()) as scp:
    scp.get(jobfilename)
ssh.exec_command("rm "+jobfilename)
ssh.close()
os.rename(jobfilename,"uc1_output/"+jobfilename)
d.Disp2(t1)
endtime = time.time()
print("Elapsed time is "+str(endtime - starttime)+" seconds or "+str((endtime - starttime)/3600)+" hours.") 