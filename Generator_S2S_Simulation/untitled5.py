# -*- coding: utf-8 -*-
"""
Created on Tue Sep 11 17:12:17 2018

@author: andre
"""
import numpy as np
from scipy import sparse
from scipy.sparse.linalg import spsolve
from numpy.linalg import solve, norm
from numpy.random import rand
import disp as d 
import sys
mtx = sparse.lil_matrix((5,5))
emtx=sparse.lil_matrix((30,40))

i=[0,0,1,1,2,2]
j=[1,2,0,2,0,3]
v=[1,2,3,1,1,1]
c= [(0,1),(0,2),(1,0),(1,2),(2,0),(2,3)]
#mtx.rows[0]=np.array([0,2])
mtx.setdiag([1,3],-0)
print(mtx) 
print("or")
print(emtx)
print(sys.getsizeof(emtx))
print(mtx.todense())



mtx[:2, :]  


mtx[:2, :].todense()    


mtx[1:2, [0,2]].todense()    

mtx.todense()    

row = np.array([0, 0, 1, 2, 2, 2])
col = np.array([2, 0, 2, 0, 1, 2])
data = np.array([2, 1, 3, 4, 5, 6])
csr=sparse.csr_matrix((data, (row, col)), shape=(3, 3)).toarray()
print(csr)