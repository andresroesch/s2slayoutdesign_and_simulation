# -*- coding: utf-8 -*-
"""
Created on Fri Jun 22 17:12:47 2018

@author: andre
"""

import numpy as np

def col_vector_norms(a,order=None):
    """
    Return an array representing the norms of a set of column vectors.

    Arguments:
      a      - An (n x m) numpy matrix, representing m column vectors of length
               n.
      order  - The order of the norm.  See numpy.linalg.norm for more
               information.
    """
    (nrow,ncol) = a.shape
    norms = np.zeros((ncol,),a.dtype)
    for j in range(ncol):
        col = a[:,j].A
        col.shape = (nrow,)
        norms[j] = np.linalg.norm(col,order)
    return norms

def cg(A, b, x=None, tol=1.0e-8, itmax=None, info=False):
    """
    Use the conjugate gradient method to return x such that A * x = b.

    Arguments:
      A      - A square numpy matrix representing the linear operator.  If A is
               symmetric, then this algorithm is guaranteed to converge.
      b      - An (n x m) numpy matrix representing the right hand side, where m
               is the number of column vectors and n is the size of A.
      x      - A numpy matrix, same shape as b, that serves as the initial guess
               for the algorithm.  Defaults to the zero matrix.  If given, the
               return matrix is this matrix.
      tol    - Accuracy tolerance for the algorithm stopping criterion.  Default
               1.0e-8.
      itmax  - Maximun allowed iterations.  Default is n, the size of A.
      info   - If info is True, return a tuple (x, k, norms) where x is the
               solution, k is the required iterations and norms is an array of
               the final residual norms.  Default False.
    """

    # Sanity checks.  I check the types of A, b and x to ensure that I will get
    # matrix algebra.
    if not isinstance(A,np.matrix):
        raise (TypeError, "A must be a matrix")
    (nrow,ncol) = A.shape
    if nrow != ncol:
        raise (ValueError, "A must be a square matrix")
    if not isinstance(b,np.matrix):
        raise (TypeError, "b must be a matrix")
    if x is None:
        x = np.matrix(np.zeros(b.shape, b.dtype))
    if not isinstance(x,np.matrix):
        raise (TypeError, "x must be a matrix")
    if itmax is None:
        itmax = nrow

    # Initialization
    nrhs  = b.shape[1]
    r2    = np.zeros((nrhs,), b.dtype)
    alpha = np.zeros((nrhs,), b.dtype)
    beta  = np.zeros((nrhs,), b.dtype)
    r = b - A * x
    p = r.copy()
    k = 0

    # Main conjugate gradient loop
    while k < itmax:
        for j in range(nrhs):
            r2[j] = r[:,j].T * r[:,j]
        for j in range(nrhs):
            alpha[j] = r2[j] / (p[:,j].T * A * p[:,j])
        x[:] += p.A * alpha
        r[:] -= (A * p).A * alpha
        norms = col_vector_norms(r,2)
        if (norms < tol).all():
            break
        for j in range(nrhs):
            beta[j] = (r[:,j].T * r[:,j]) / r2[j]
        p[:] = r + p.A * beta
        k += 1

    # Return the requested information
    if info:
        return (x, k, norms)
    else:
        return x
    
def col_vector_norms2(a,order=None):
    """
    See doc for col_vector_norms
    """
    if isinstance(a,np.matrix):
        a = a.A
    norms = np.fromiter((np.linalg.norm(col,order) for col in a.T),a.dtype)
    return norms

def cg2(A, b, x=None, tol=1.0e-8, itmax=None, info=False):
    """
    See doc string for cg.
    """
    matrixout = True if isinstance(A,np.matrix) else False
    A = np.asarray(A)
    b = np.asarray(b)
    nrow, ncol = A.shape
    if nrow != ncol:
        raise (ValueError, "A must be square")
    if x is None:
        x = np.zeros(b.shape, b.dtype)
    if itmax is None:
        itmax = nrow

    # Initialization
    nrhs  = b.shape[1]
    alpha = np.zeros((nrhs,), b.dtype)
    beta  = np.zeros((nrhs,), b.dtype)
    r2old = np.zeros((nrhs,), b.dtype)
    r = b - np.dot(A,x)
    r2 = (r * r).sum(axis=0)
    p = r.copy()
    k = 0

    # Main conjugate gradient loop
    while k < itmax:
        for j in range(nrhs):
            alpha[j] = r2[j] / (np.outer(p[:,j],p[:,j]) * A).sum()
        # or replace the loop by:
        # alpha = r2 / (p*np.dot(A,p)).sum(axis=0)
        x[:] += p * alpha
        r[:] -= np.dot(A , p) * alpha
        norms = col_vector_norms2(r,2)
        if (norms < tol).all():
            break
        r2old[:] = r2
        r2[:] = (r * r).sum(axis=0)
        beta = r2 / r2old
        p[:] = r + (p * beta)
        k += 1

    if matrixout:
        x = np.asmatrix(x)
    # Return the requested information
    if info:
        return (x, k, norms)
    else:
        return x

    