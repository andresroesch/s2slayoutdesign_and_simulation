# -*- coding: utf-8 -*-
"""
Created on Mon Aug 21 13:43:07 2017

@author: roesch
"""
import time 
import unicodedata as ucd
def Disp1(s):
    print(s)
    return time.time()
###############################################################################
def Disp2(t):
    print("Done..    "+str(time.time() - t)+"s")
    return time.time()
###############################################################################
def display_output(output):
    for string in output.monitor:
        print(string)
#    print("\n")
#    print("#################OUTPUT##################\n")          
#    print("Simulation model:\t\t"+output.model)
#    print("\nMATERIALS:")
#    print("p-type:\t\t\t\t"+output.s.p_material)
#    print("    S:\t\t\t\t"+str(output.s.p_seebeck*1000000)+" "+greek("MU",0)+"V/K")
#    print("    "+greek("SIGMA",0)+":\t\t\t\t"+str(output.s.p_el_conductivity/100)+" S/cm")
#    print("    "+greek("KAPPA",0)+":\t\t\t\t"+str(output.s.p_th_conductivity)+" W/mK")
#    print("    Layer thickness:\t\t"+str(round(output.s.p_thickness*1000000,3))+" "+greek("MU",0)+"m")
#    print("n-type:\t\t\t\t"+output.s.n_material)
#    print("    S:\t\t\t\t"+str(output.s.n_seebeck*1000000)+" "+greek("MU",0)+"V/K")
#    print("    "+greek("SIGMA",0)+":\t\t\t\t"+str(output.s.n_el_conductivity/100)+" S/cm")
#    print("    "+greek("KAPPA",0)+":\t\t\t\t"+str(output.s.n_th_conductivity)+" W/mK")
#    print("    Layer thickness:\t\t"+str(round(output.s.n_thickness*1000000,3))+" "+greek("MU",0)+"m")
#    display_type_of_connectormaterial(output)
#    print("Substrate:\t\t\t"+str(round(output.s.substrate_thickness*1000000,3))+" "+greek("MU",0)+"m "+output.s.substrate_material)
#    print("\nLAYOUT:")
#    print("Filename:\t\t\t"+output.filename)
#    print("Printer:\t\t\t"+output.pg.printer)
#    print("Tilt angle:\t\t\t"+str(round(output.pg.angle,1)))
#    print("Number of thermocouples:\t"+str(output.number_of_thermocouples))
#    print("Fold depth:\t\t\t"+str(round(output.te.cell_y*1000, 3))+" mm")
#    print("Width p-element:\t\t"+str(round(output.te.p.effective_x*1000, 3))+" mm")
#    print("Width n-element:\t\t"+str(round(output.te.n.effective_x*1000, 3))+" mm")
#    print("Width connectors:\t\t"+str(round(output.pg.connector_width*1000, 3))+" mm")
#    print("Spacing between columns:\t"+str(round(output.te.spacing_x*1000, 3))+" mm")
#    print("\nGENERATOR:")
#    print("Number of thermocouples:\t"+str(output.number_of_thermocouples))
#    print("Eff. "+greek("DELTA",1)+"T/K:\t\t\t"+str(round(output.delta_T_eff,3)))
#    print("OC-voltage/K:\t\t\t"+str(round(output.voltage_per_kelvin*1000,3))+" mV/K")
#    print("El. resistance:\t\t\t"+str(round(output.electrical_resistence,3))+" "+greek("OMEGA",1))
#    print("Th. resistance:\t\t\t"+str(round(output.thermal_resistence,3))+" K/W")
#    print("Max. power/K²:\t\t\t"+str(round(output.power_per_kelvin_squared*1000000,3))+" "+greek("MU",0)+"W/K²")
#    print("Eff. Z-value:\t\t\t"+str(round(output.eff_Z,8))+" 1/K")
#    print("\nSCENARIO:")
#    print("Scenario name:\t\t\t"+output.s.scenario)
#    print("T heat source:\t\t\t"+str(output.s.source.temperature)+" K ("+str(output.s.source.temperature-273.15)+" °C)")
#    print("T heat sink:\t\t\t"+str(output.s.sink.temperature)+" K ("+str(output.s.sink.temperature-273.15)+" °C)")
#    print(greek("DELTA",1)+"T:\t\t\t\t"+str(output.deltaT)+" K")
#    print("Tm:\t\t\t\t"+str(output.Tm)+" K ("+str(output.Tm-273.15)+" °C)")
#    print("Rth heat source:\t\t\t"+str(output.s.source.th_resistance)+" K/W")
#    print("Rth heat sink:\t\t"+str(output.s.sink.th_resistance)+" K/W")
#    print("Rel load:\t\t\t"+str(output.s.el_load)+" "+greek("OMEGA",1))
#    print("\nOTEG IN SCENARIO:")
#    print(greek("DELTA",1)+"T@TEG:\t\t\t\t"+str(output.deltaTatTEG)+" K")
#    print("OC Voltage:\t\t\t"+str(round(output.openvoltage,3))+" V")
#    print("Voltage:\t\t\t"+str(round(output.voltage,3))+" V")
#    print("Max output power:\t\t"+str(round(output.max_power*1000000,3))+" "+greek("MU",0)+"W")
#    print("Power at load:\t\t\t"+str(round(output.poweratload*1000000,3))+" "+greek("MU",0)+"W")
#    print("Heat flow:\t\t\t"+str(round(output.heatflow,3))+" W")
#    print("PCE:\t\t\t\t"+str(round(output.eta,8)))
#    print("Eff. ZT-value:\t\t\t"+str(round(output.ZT,8)))
#    print("Efficiency:\t\t\t"+str(round(output.efficiency,8)))
    return True
###############################################################################
def greek(expr,capital):
    if capital:
        cap="CAPITAL"
    else:
        cap="SMALL"
    return ucd.lookup("GREEK "+cap+" LETTER "+expr)
###############################################################################
def display_type_of_connectormaterial(output):
    if output.pg.connectormaterial=="c":
        output.monitor.append("Connectors:\t\t\t"+str(round(output.s.c_thickness*1000000,3))+greek("MU",0)+"m "+output.s.c_material)
        output.monitor.append("    S:\t\t"+str(output.s.c_seebeck*1000000)+" "+greek("MU",0)+"V/K")
        output.monitor.append("    "+greek("SIGMA",0)+":\t\t\t\t"+str(output.s.c_el_conductivity)+" S/cm")
        output.monitor.append("    "+greek("KAPPA",0)+":\t\t\t\t"+str(output.s.c_th_conductivity)+" W/mK")
        output.monitor.append("    Layer thickness:\t\t"+str(round(output.s.c_thickness*1000000,3))+" "+greek("MU",0)+"m")
        output.monitor.append("    Wetfilm thickness:\t\t"+str(round(output.s.c_wetfilm_thickness*1000000,3))+" "+greek("MU",0)+"m")
    else:
        output.monitor.append("Connectors:\t\t\t"+output.pg.connectormaterial+"-type")
    
    return output

def subscript(character,capital):
    if capital:
        cap="CAPITAL"
    else:
        cap="SMALL"
    return ucd.lookup("LATIN SUBSCRIPT "+cap+" LETTER "+character)