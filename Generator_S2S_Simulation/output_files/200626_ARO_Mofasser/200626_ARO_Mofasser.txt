p	...
filename	200626_ARO_Mofasser
comment	Uebermass in PDF wegen Belichtung 0.15mm. Ueberlapp von p und n 0.3 mm aufgrund von 0.05 bis 0.1 mm Randeinfransung und 0.1mm Sicherheit. 0.85 Gapsize resuliter in 1mm Gap wegen Siebbelichtung.
material_unit1	c
material_unit2	p
material_unit3	n
connectormaterial	p
sheet_full_y	[0.17]
sheet_full_x	[0.16]
substrate_foil_full_x	[0.164]
sheet_offset_bottom	[0.011]
sheet_offset_top	[0.01]
sheet_offset_left	[0.001]
sheet_offset_right	[0.001]
second_screen_joint	[0.08]
second_screen_joint_width	[0.006]
effective_y_optimization	[0]
element_of_lower_value	p
startingpoint	[0,0]
no_c_material_between_elements	[0]
first_field_type	p
write_indicies	[0]
print_numeration	[0]
periodic_enlargement	[0]
color_n	[0.4,0.8,0.4]
color_c	[0.9647058823529412,0.20392156862745098,0.24705882352941178]
color_p	[0.0,0.0,0.2]
color_drawingarea	[0.6,0.6,0.6]
color_elementarea	[0,1,0]
color_design	[0,0,0]
delta_element_x	[0.0]
connector_width	[0.01]
connectorspacing	[0.0]
p	..
te	...
p	...
overlap	[0.00015]
x	[0.0101538]
p	..
n	...
overlap	[1e-07]
x	[0.002]
n	..
cell_y	[0.00397436]
spacing_x	[0.002]
spacing_y	[0]
overlap_no_c	[1e-07]
te	..
l	...
linker_type	[2]
linker_height	[0.01]
l	..
s	...
model	[1]
scenario	TEGmeasurementsetupLTI
p_material	PEDOTNanowires_SL
n_material	TiS2_SL
c_material	PEDOTNanowires_SL
substrate_material	PEN
precision	[1000]
v_factor	[1]
n_wetfilm_thickness	[4e-05]
c_wetfilm_thickness	[4e-05]
p_wetfilm_thickness	[4e-05]
n_thickness	[7.2e-06]
p_thickness	[4.2e-06]
c_thickness	[2e-06]
air_thickness	[1e-06]
substrate_thickness	[4e-06]
contact_resistance_pn	[0]
contact_resistance_cp	[0]
contact_resistance_cn	[0]
s	..
