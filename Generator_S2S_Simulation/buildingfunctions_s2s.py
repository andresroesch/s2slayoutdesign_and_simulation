# -*- coding: utf-8 -*-
"""
Created on Tue Aug 29 16:26:33 2017

@author: roesch
"""
from imp import reload 
import shapely.geometry as sp
import shapely.ops as so
import numpy as np
import disp as d
import globalclasses as gc
import custommathfunctions as c
import shapefunctions_s2s as sf
import directions as dr
import customboolean as cb
import pswriter_s2s as ps

reload(ps)
reload(cb)
reload(dr)
reload(sf)
reload(c)
reload(gc)
reload (d)
###############################################################################
def findnumberofdeletedrows( pg,te):
    if  pg.effective_y_optimization:
        pg.number_of_deleted_rows=int(np.floor((pg.space_between_last_connector_and_last_row+2*pg.connector_width+2*te.p.overlap+2*te.n.overlap)/te.cell_y)+round(pg.extra_rows_of_cutline))
    else:
        pg.number_of_deleted_rows=int(np.floor((pg.space_between_last_connector_and_last_row+2*pg.connector_width)/te.cell_y)+round(pg.extra_rows_of_cutline))
    return pg
###############################################################################
def tiltedchecker(pg):
    t1=d.Disp1('Creating tilted checkerboard')
    midpointcorrection=np.array([0,0])
    if pg.angle==0:
        exec("midpointcorrection=np.array([pg.cell_"+pg.first_field_type+"_x,pg.cell_y])/2-np.array([pg.cell_x,pg.cell_y])/2")
    else:
        exec("midpointcorrection=np.dot(np.array([pg.cell_"+pg.first_field_type+"_x-pg.cell_x,0])/2,np.array([[c.cosd(pg.angle)],[c.sind(pg.angle)]]))")
    first_field=gc.voidclass()
    first_field.midpoint=np.array([pg.cell_x,pg.cell_y])/2
    first_field.color=int(pg.first_field_type=="n")
    first_field.index=[0,0]
    first_field.corners=sf.makerectangle(pg.cell_x,pg.cell_y,first_field.midpoint)
    first_field.corners=c.rotate(first_field.corners,pg.angle,first_field.midpoint)
    if pg.angle>0:
        first_field.midpoint=first_field.midpoint-np.array([first_field.corners[3,0], first_field.corners[0,1]])
        first_field.corners=first_field.corners-np.array([first_field.corners[3,0], first_field.corners[0,1]])
    else:
        first_field.midpoint=first_field.midpoint-np.array([first_field.corners[0,0], first_field.corners[1,1]])
        first_field.corners=first_field.corners-np.array([first_field.corners[0,0], first_field.corners[1,1]])
    first_field.corners=c.translate(first_field.corners,+pg.elementslimits.minimum+pg.startingpoint+midpointcorrection)
    first_field.midpoint=first_field.midpoint+pg.elementslimits.minimum+pg.startingpoint+midpointcorrection
#    ps.draw("output_files/"+pg.filename+"/"+pg.filename+"_rgb.eps",first_field.corners,[1,0,1])
    no_need_to_check=0
    fieldcolorlist=list()
    fieldcolorlist.append(first_field.color)
    midpointlist=list()
    midpointlist.append(first_field.midpoint)
    indexlist=list()
    indexlist.append(first_field.index)
    newfields,newmidpointlist,newindexlist=unfold([first_field],pg.angle,pg.cell_x,pg.cell_y,pg.elementarea,midpointlist,indexlist,no_need_to_check)
    fieldcolorlist=fieldcolorlist+[field.color for field in newfields]
    intermediate1_no_need_to_check=1
    while cb.istherespace(newfields,pg.angle,pg.cell_x,pg.cell_y,pg.elementarea,indexlist):
        intermediate2_no_need_to_check=len(newfields)
        newfields,midpointlist,indexlist=unfold(newfields,pg.angle,pg.cell_x,pg.cell_y,pg.elementarea,midpointlist,indexlist,no_need_to_check)
        fieldcolorlist=fieldcolorlist+[field.color for field in newfields]
        no_need_to_check=intermediate1_no_need_to_check
        intermediate1_no_need_to_check=intermediate1_no_need_to_check+intermediate2_no_need_to_check
    pg.fieldlist=list()
    for i,midpt in enumerate(midpointlist):
        field=gc.voidclass()
        field.midpoint=midpt
        field.color=fieldcolorlist[i]
        field.index=indexlist[i]
        if fieldcolorlist[i]:
            field.corners=(c.rotate(sf.makerectangle(pg.cell_n_x,pg.cell_y,midpt),pg.angle,midpt))
        else:
             field.corners=(c.rotate(sf.makerectangle(pg.cell_p_x,pg.cell_y,midpt),pg.angle,midpt))   
        
        pg.fieldlist.append(field)
#    for field in pg.fieldlist:
##        ps.write("output_files/"+pg.filename+"/"+pg.filename+"_rgb.eps",str(field.index),field.midpoint,[0,0,0])
#        ps.draw("output_files/"+pg.filename+"/"+pg.filename+"_rgb.eps",field.corners,[1,0,1])
    d.Disp2(t1)
    return pg    
###############################################################################  
def unfold(fields,angle,length_x,length_y,limits,midpointlist,indexlist,no_need_to_check):
    newfields=list()
    for field in fields:
        temp=go_up(field,angle,length_y)
        if cb.ismidpointin(temp.midpoint,limits) and not(temp.index in indexlist):#b.isitfree(temp, midpointlist,no_need_to_check):
            newfields.append(temp)
            midpointlist.append(temp.midpoint)
            indexlist.append(temp.index)
        temp=go_down(field,angle,length_y)
        if cb.ismidpointin(temp.midpoint,limits) and not(temp.index in indexlist):#cb.isitfree(temp, midpointlist,no_need_to_check):
            newfields.append(temp)
            midpointlist.append(temp.midpoint)
            indexlist.append(temp.index)
        temp=go_left(field,angle,length_x)
        if cb.ismidpointin(temp.midpoint,limits) and not(temp.index in indexlist):#cb.isitfree(temp, midpointlist,no_need_to_check):
            newfields.append(temp)
            midpointlist.append(temp.midpoint)
            indexlist.append(temp.index)
        temp=go_right(field,angle,length_x)
        if cb.ismidpointin(temp.midpoint,limits) and not(temp.index in indexlist):#cb.isitfree(temp, midpointlist,no_need_to_check):
            newfields.append(temp)
            midpointlist.append(temp.midpoint)
            indexlist.append(temp.index)    
    return newfields,midpointlist,indexlist
###############################################################################
def go_up(field,angle,length):
    newfield=gc.voidclass()
    newfield.midpoint=field.midpoint+length*dr.up(angle)
    newfield.color=int(not(field.color))
    newfield.index=[field.index[0]+1,field.index[1]]
    return newfield
###############################################################################
def go_down(field,angle,length):
    newfield=gc.voidclass()
    newfield.midpoint=field.midpoint+length*dr.down(angle)
    newfield.color=int(not(field.color))
    newfield.index=[field.index[0]-1,field.index[1]]
    return newfield
###############################################################################
    
def go_left(field,angle,length):
    newfield=gc.voidclass()
    newfield.midpoint=field.midpoint+length*dr.left(angle)
    newfield.color=int(not(field.color))
    newfield.index=[field.index[0],field.index[1]-1]
    return newfield
###############################################################################
def go_right(field,angle,length):
    newfield=gc.voidclass()
    newfield.midpoint=field.midpoint+length*dr.right(angle)
    newfield.color=int(not(field.color))
    newfield.index=[field.index[0],field.index[1]+1]
    return newfield
###############################################################################
def set_cutline(pg):
    t1=d.Disp1('Setting cutline')
    if pg.angle!=0:
        bullethole=np.array([(pg.printinglimits.minimum[0]+pg.printinglimits.maximum[0])/2,pg.sheet_full_y/2])
        distance=[np.linalg.norm(bullethole-field.midpoint) for field in pg.fieldlist]
        pg.cutlinerow=pg.fieldlist[distance.index(min(distance))].index[0]
#        ps.draw("output_files/deleteme/deleteme_c.eps",pg.fieldlist.corners[distance.index(min(distance))],[1,0,0])
    d.Disp2(t1)
    return pg
###############################################################################
def set_thermoelements(pg,te,l):
    t1=d.Disp1('Placing thermoelements')
    pg.num_of_p_elements=0
    pg.num_of_n_elements=0
    pg.elementlist=list()
    rowindex=list()
    columnindex=list()
    for field in pg.fieldlist:
        if (not(bool(field.color)) and cb.isitfullyin(c.translate(c.rotate(te.p.outline,pg.angle,[0,0]),field.midpoint),pg.earea) and cb.isfieldoutofcutline(pg,field)):
            pg.num_of_p_elements=pg.num_of_p_elements+1
            element=gc.voidclass()
            element.type='p'
            element.index=field.index
            rowindex.append(element.index[0])
            columnindex.append(element.index[1])
            element.midpoint=field.midpoint
            element.outline=c.translate(c.rotate(te.p.outline,pg.angle,[0,0]),element.midpoint)
            element.top=c.translate(c.rotate(l.p.top,pg.angle,[0,0]),element.midpoint)
            element.bottom=c.translate(c.rotate(l.p.bottom,pg.angle,[0,0]),element.midpoint)
#            element.overlaparea_top=c.translate(te.p.overlaparea_top,element.midpoint)
#            element.overlaparea_bottom=c.translate(te.p.overlaparea_bottom,element.midpoint)
            element.overlaparea_top=c.translate(c.rotate(te.p.overlaparea_top,pg.angle,[0,0]),element.midpoint)
            element.overlaparea_bottom=c.translate(c.rotate(te.p.overlaparea_bottom,pg.angle,[0,0]),element.midpoint)
            element.active_area=c.translate(c.rotate(te.p.active_area,pg.angle,[0,0]),element.midpoint)
            element.fieldcorners=field.corners
            element.exitconnector_mat=pg.connectormaterial
#            ps.draw("output_files/"+pg.filename+"/"+pg.filename+"_rgb.eps",field.corners,[not(field.color),field.color,0])
            if pg.connector_type!=1:
                element.stripes=c.translate(c.rotate(te.p.stripes,pg.angle,[0,0]),element.midpoint)
                element.contactfield_bottom=c.translate(c.rotate(te.p.contactfield_bottom,pg.angle,[0,0]),element.midpoint)
                element.contactfield_top=c.translate(c.rotate(te.p.contactfield_top,pg.angle,[0,0]),element.midpoint)
            pg.elementlist.append(element)
        elif (bool(field.color) and cb.isitfullyin(c.translate(c.rotate(te.n.outline,pg.angle,[0,0]),field.midpoint),pg.earea) and cb.isfieldoutofcutline(pg,field)):
            pg.num_of_n_elements=pg.num_of_n_elements+1
            element=gc.voidclass()
            element.type='n'
            element.index=field.index
            rowindex.append(element.index[0])
            columnindex.append(element.index[1])
            element.midpoint=field.midpoint
            element.outline=c.translate(c.rotate(te.n.outline,pg.angle,[0,0]),element.midpoint)
            element.top=c.translate(c.rotate(l.n.top,pg.angle,[0,0]),element.midpoint)
            element.bottom=c.translate(c.rotate(l.n.bottom,pg.angle,[0,0]),element.midpoint)
#            element.overlaparea_top=c.translate(te.n.overlaparea_top,element.midpoint)
#            element.overlaparea_bottom=c.translate(te.n.overlaparea_bottom,element.midpoint)
            element.overlaparea_top=c.translate(c.rotate(te.n.overlaparea_top,pg.angle,[0,0]),element.midpoint)
            element.overlaparea_bottom=c.translate(c.rotate(te.n.overlaparea_bottom,pg.angle,[0,0]),element.midpoint)
            element.active_area=c.translate(c.rotate(te.n.active_area,pg.angle,[0,0]),element.midpoint)
            element.fieldcorners=field.corners
            element.exitconnector_mat=pg.connectormaterial
#            ps.draw("output_files/"+pg.filename+"/"+pg.filename+"_rgb.eps",field.corners,[not(field.color),field.color,0])
            if pg.connector_type!=1:
                element.stripes=c.translate(c.rotate(te.n.stripes,pg.angle,[0,0]),element.midpoint)
                element.contactfield_bottom=c.translate(c.rotate(te.n.contactfield_bottom,pg.angle,[0,0]),element.midpoint)
                element.contactfield_top=c.translate(c.rotate(te.n.contactfield_top,pg.angle,[0,0]),element.midpoint)
            pg.elementlist.append(element)
#    for i,element in enumerate(pg.elementlist):
#        ps.write("output_files/"+pg.filename+"/"+pg.filename+"_rgb.eps",str(i),element.midpoint,[0,0,0]) 
    pg.num_of_rows=max(rowindex)-min(rowindex)+1
    pg.num_of_columns=max(columnindex)-min(columnindex)+1
    try:
        pg.cutlinerow=pg.cutlinerow-min(rowindex)
    except:
        pass
    pg.elementmatrix=np.zeros([pg.num_of_rows,pg.num_of_columns])
    for i,element in enumerate(pg.elementlist):
        pg.elementmatrix[element.index[0]-min(rowindex),element.index[1]-min(columnindex)]=i+1
        element.index=[element.index[0]-min(rowindex),element.index[1]-min(columnindex)]
#        ps.write("output_files/"+pg.filename+"/"+pg.filename+"_rgb.eps",str(element.index),element.midpoint+np.array([0,0.001]),[0,0,1]) 
    d.Disp2(t1)
    return pg
###############################################################################
def seriesing(pg):
    t1=d.Disp1('Creating series interconnection')
    pg  = seriesing_elements(pg)
    navigationlist=np.zeros(len(pg.elementlist)).astype(int)
    for i,element in enumerate(pg.elementlist):
        navigationlist[element.element_in_series-1]=i
        if element.exit_direction:
            element.entrance=element.bottom
            element.exit=element.top
        else:
            element.entrance=element.top
            element.exit=element.bottom
    pg.navigationlist=list()
    for index in navigationlist:
        pg.navigationlist.append(pg.elementlist[index])
    for i,element in enumerate(pg.navigationlist):
        if i+1!=len(pg.elementlist):
            element.next_element_index=navigationlist[i+1]
        else:
            element.next_element_index=len(pg.elementlist)                  
    if pg.angle!=0:
        for i,element in enumerate(pg.navigationlist):
            if i+1!=len(pg.elementlist):
                if element.index[0]<pg.cutlinerow-1 and pg.navigationlist[i+1].index[0]>pg.cutlinerow-1:
                    element.page_jump=-1
                    element.exitconnector_mat=pg.material_unit3
                elif element.index[0]>pg.cutlinerow-1 and pg.navigationlist[i+1].index[0]<pg.cutlinerow-1:
                    element.page_jump=1
                    element.exitconnector_mat=pg.material_unit3
                else:
                    element.page_jump=0
            else:
                element.page_jump=0
    else:
        for element in pg. navigationlist:
            element.page_jump=0
#    for x,index in enumerate(pg.navigation):
#        ps.write("output_files/"+pg.filename+"/"+pg.filename+"_rgb.eps",str(pg.elementlist[int(index)].page_jump)+"_"+str(pg.elementlist[int(index)].exitconnector_type),pg.elementlist[int(index)].midpoint+np.array([0,-0.001]),[1,1,0])
    d.Disp2(t1)
    return pg
###############################################################################
def seriesing_elements( pg ):
    if pg.angle>0:
        highest_column_under_cutline=np.max(np.nonzero(pg.elementmatrix[0:pg.cutlinerow,:])[1])
        lowest_column_over_cutline=np.min(np.nonzero(pg.elementmatrix[pg.cutlinerow+1:-1,:])[1])
        pg.number_of_columns_in_parallelogramm=np.int((highest_column_under_cutline-lowest_column_over_cutline)+pg.number_columns+1)
        pg.number_of_rows_in_parallelogramm=pg.number_rows-1
        par=np.zeros([pg.number_of_rows_in_parallelogramm,pg.number_of_columns_in_parallelogramm])
        for i,element in enumerate(pg.elementlist):
            if element.index[0]<pg.cutlinerow:
                index_offset=[pg.number_rows,pg.number_columns]
            else:
                index_offset=[0,0]
            try:
#                ps.write("output_files/"+pg.filename+"/"+pg.filename+"_rgb.eps",str([element.index[0]-1-pg.cutlinerow+index_offset[0],element.index[1]-lowest_column_over_cutline+index_offset[1]]),element.midpoint,[0.5,0,0])
                par[element.index[0]-1-pg.cutlinerow+index_offset[0],element.index[1]-lowest_column_over_cutline+index_offset[1]]=int(i+1)
            except:
#                ps.draw("output_files/"+pg.filename+"/"+pg.filename+"_rgb.eps",element.outline,[0,0,0])
#                ps.write("output_files/"+pg.filename+"/"+pg.filename+"_rgb.eps",str([element.index[0]-1-pg.cutlinerow+index_offset[0],element.index[1]-lowest_column_over_cutline+index_offset[1]]),element.midpoint,[0.5,0,0])
                print(i+1)
    else:
        pg.number_of_columns_in_parallelogramm=np.int(pg.num_of_columns)
        pg.number_of_rows_in_parallelogramm=np.int(pg.num_of_rows)
        par=pg.elementmatrix
    c_direction=not(np.int(np.mod(pg.number_of_columns_in_parallelogramm,2)))
    c=1
    pg.navigation=list()
    for i,column in enumerate(np.flipud(np.transpose(par))):
        if c_direction:
            elements=column[column!=0]
            pg.navigation=pg.navigation+list(elements)
            for j,index in enumerate(elements):
                pg.elementlist[int(index-1)].element_in_series=c
                pg.elementlist[int(index-1)].exit_direction=c_direction
                pg.elementlist[int(index-1)].index_in_par=np.array(np.where(par==index))
#                ps.draw("output_files/"+pg.filename+"/"+pg.filename+"_rgb.eps",pg.elementlist[int(index-1)].outline,[1,0,0])
                c=c+1
                if index==elements[-1]:
                    c_direction=not(c_direction)
                    pg.elementlist[int(index-1)].exitconnector_type=0
                    
                else:
                    pg.elementlist[int(index-1)].exitconnector_type=1
        else:
            elements=np.flipud(column[column!=0])
            pg.navigation=pg.navigation+list(elements)
            for j,index in enumerate(elements):
                pg.elementlist[int(index-1)].element_in_series=c
                pg.elementlist[int(index-1)].exit_direction=c_direction
                pg.elementlist[int(index-1)].index_in_par=np.array(np.where(par==index))
#                ps.draw("output_files/"+pg.filename+"/"+pg.filename+"_rgb.eps",pg.elementlist[int(index-1)].outline,[0,1,0])
                c=c+1
                if i==pg.number_of_columns_in_parallelogramm-1 and index==elements[-1]:
                    pg.elementlist[int(index-1)].exitconnector_type=0
                else:
                    if index==elements[-1]:
                        c_direction=not(c_direction)
                        pg.elementlist[int(index-1)].exitconnector_type=0
                    else:
                        pg.elementlist[int(index-1)].exitconnector_type=1
    pg.navigation= [int(x-1) for x in pg.navigation]
    pg.par=par
    return pg 
###############################################################################
def delete_last_row(pg):
    t1=d.Disp1('Deleting excess rows')
    deleted_indices=list()
    for i,element in enumerate(pg.elementlist):
        if element.index_in_par[0]>=pg.number_of_rows_in_parallelogramm-pg.number_of_deleted_rows:
            deleted_indices.append(i)
    for i in reversed(deleted_indices):
        del pg.elementlist[i]
    del pg.elementmatrix
    del  pg.navigationlist
    del  pg.par
    del pg.number_of_rows_in_parallelogramm   
    maximum_rowindex=pg.elementlist[0].index[0]
    minimum_rowindex=pg.elementlist[0].index[0]
    maximum_columnindex=pg.elementlist[0].index[1]
    minimum_columnindex=pg.elementlist[0].index[1]
    for element in pg.elementlist:
        maximum_rowindex=max(maximum_rowindex,element.index[0])
        minimum_rowindex=min(minimum_rowindex,element.index[0])
        maximum_columnindex=max(maximum_columnindex,element.index[1])
        minimum_columnindex=min(minimum_columnindex,element.index[1])
    pg.num_of_rows=maximum_rowindex-minimum_rowindex+1
    pg.num_of_columns=maximum_columnindex-minimum_columnindex+1
    pg.elementmatrix=np.zeros([maximum_rowindex-minimum_rowindex+1,maximum_columnindex-minimum_columnindex+1])
    for i,element in enumerate(pg.elementlist):
        pg.elementmatrix[element.index[0]-minimum_rowindex,element.index[1]-minimum_columnindex]=i+1
        pg.elementlist[i].index=[element.index[0]-minimum_rowindex,element.index[1]-minimum_columnindex]
    d.Disp2(t1)
    pg=seriesing(pg)
    return pg   
###############################################################################
def delete_elements_in_second_joint(pg):
    t1=d.Disp1('Deleting Elements for second joint')
    deleted_indices=list()
    for i,element in enumerate(pg.elementlist):
        if sp.Polygon(element.outline).intersects(pg.forbiddenarea_unit3):
            deleted_indices.append(i)
    for i in reversed(deleted_indices):
        del pg.elementlist[i]
    del pg.elementmatrix
    del  pg.navigationlist
    del  pg.par
    del pg.number_of_rows_in_parallelogramm   
    maximum_rowindex=pg.elementlist[0].index[0]
    minimum_rowindex=pg.elementlist[0].index[0]
    maximum_columnindex=pg.elementlist[0].index[1]
    minimum_columnindex=pg.elementlist[0].index[1]
    for element in pg.elementlist:
        maximum_rowindex=max(maximum_rowindex,element.index[0])
        minimum_rowindex=min(minimum_rowindex,element.index[0])
        maximum_columnindex=max(maximum_columnindex,element.index[1])
        minimum_columnindex=min(minimum_columnindex,element.index[1])
    pg.num_of_rows=maximum_rowindex-minimum_rowindex+1
    pg.num_of_columns=maximum_columnindex-minimum_columnindex+1
    pg.elementmatrix=np.zeros([maximum_rowindex-minimum_rowindex+1,maximum_columnindex-minimum_columnindex+1])
    for i,element in enumerate(pg.elementlist):
        pg.elementmatrix[element.index[0]-minimum_rowindex,element.index[1]-minimum_columnindex]=i+1
        pg.elementlist[i].index=[element.index[0]-minimum_rowindex,element.index[1]-minimum_columnindex]
    d.Disp2(t1)
    pg=seriesing(pg)
    return pg
###############################################################################
def bridge_second_joint(pg):
    t1=d.Disp1('Deleting Elements for second joint')
    for element in pg.elementlist:
        if sp.Polygon(element.exitconnector).intersects(pg.forbiddenarea_unit2):
            element.exitconnector_mat=pg.material_unit3
    d.Disp2(t1)
    return pg
###############################################################################
def set_connectors(pg,l):
    t1=d.Disp1('Setting linker and centerconnectors')
    if pg.connector_type==1:
        pg=set_linker(pg,l)
    else:
        pg=set_turnconnectors(pg,l)
        pg=set_centerconnectors(pg)
    d.Disp2(t1)
    return pg
###############################################################################
def set_linker(pg,l):
    for element in pg.elementlist:
        if element.exit_direction:
            element.into=c.translate(c.rotate(l.g(element.type).bottom_connect,pg.angle,[0, 0]), element.midpoint)
            element.out=c.translate(c.rotate(l.g(element.type).top_connect,pg.angle,[0, 0]), element.midpoint)
        else:
            element.out=c.translate(c.rotate(l.g(element.type).bottom_connect,pg.angle,[0, 0]), element.midpoint)
            element.into=c.translate(c.rotate(l.g(element.type).top_connect,pg.angle,[0, 0]), element.midpoint)
        element.bottom=c.translate(c.rotate(l.g(element.type).bottom,pg.angle,[0, 0]), element.midpoint)
        element.top=c.translate(c.rotate(l.g(element.type).top,pg.angle,[0, 0]), element.midpoint)
    return pg
###############################################################################
def set_turnconnectors(pg,l):
    # if pg.jump_connectors_are_turn_connectors:
    #     if pg.angle!=0:
    #         for element in pg.elementlist:
    #             if element.page_jump:
    #                 element.exitconnector_type=0
                    
    for element in pg.elementlist:
        if element.element_in_series==1:
            if element.exit_direction:
                element.bottom=c.translate(c.rotate(l.g(element.type).bottom,pg.angle,[0, 0]), element.midpoint)
                element.into=c.translate(c.rotate(l.g(element.type).bottom_connect,pg.angle,[0, 0]), element.midpoint)
            else:
                element.top=c.translate(c.rotate(l.g(element.type).top,pg.angle,[0, 0]), element.midpoint)
                element.into=c.translate(c.rotate(l.g(element.type).top_connect,pg.angle,[0, 0]), element.midpoint)
        if not(element.exitconnector_type):
            if not(element.exitconnector_type):
                if element.exit_direction:
                    element.top=c.translate(c.rotate(l.g(element.type).top,pg.angle,[0, 0]), element.midpoint)
                    element.out=c.translate(c.rotate(l.g(element.type).top_connect,pg.angle,[0, 0]), element.midpoint)
                else:
                    element.out=c.translate(c.rotate(l.g(element.type).bottom_connect,pg.angle,[0, 0]), element.midpoint)
                    element.bottom=c.translate(c.rotate(l.g(element.type).bottom,pg.angle,[0, 0]), element.midpoint)
            if element.next_element_index!=len(pg.elementlist):
                if pg.elementlist[element.next_element_index].exit_direction:
                   pg.elementlist[element.next_element_index].bottom=c.translate(c.rotate(l.g(element.type).bottom,pg.angle,[0, 0]), pg.elementlist[element.next_element_index].midpoint)
                   pg.elementlist[element.next_element_index].into=c.translate(c.rotate(l.g(element.type).bottom_connect,pg.angle,[0, 0]), pg.elementlist[element.next_element_index].midpoint)
                else:
                   pg.elementlist[element.next_element_index].top=c.translate(c.rotate(l.g(element.type).top,pg.angle,[0, 0]), pg.elementlist[element.next_element_index].midpoint)
                   pg.elementlist[element.next_element_index].into=c.translate(c.rotate(l.g(element.type).top_connect,pg.angle,[0, 0]), pg.elementlist[element.next_element_index].midpoint)
#                ps.draw("output_files/"+pg.filename+"/"+pg.filename+"_rgb.eps",element.outline,[0,0,0])
#                ps.draw("output_files/"+pg.filename+"/"+pg.filename+"_rgb.eps",pg.elementlist[element.next_element_index].outline,[0,0,1])
#                ps.draw("output_files/"+pg.filename+"/"+pg.filename+"_rgb.eps",element.out,[1,0,0])
#                ps.draw("output_files/"+pg.filename+"/"+pg.filename+"_rgb.eps",pg.elementlist[element.next_element_index].into,[1,0,1])
    return pg
###############################################################################
def set_centerconnectors(pg):
    for element in pg.elementlist:
        if element.exitconnector_type:
            if element.exit_direction:
                bottom_dummy=element.overlaparea_top
                top_dummy=c.translate(pg.elementlist[element.next_element_index].overlaparea_bottom,[0,element.page_jump*pg.sheet_full_y])
                
                    
#                top_dummy=list()
#                if pg.angle!=0:
#                    if element.index[0]<pg.cutlinerow and pg.elementlist[element.next_element_index].index[0]>pg.cutlinerow:
#                        for contactfield in pg.elementlist[element.next_element_index].contactfield_bottom:
#                            next_element_contactfield_bottom=c.translate(contactfield,[0, -pg.sheet_full_y])
#                            top_dummy.append(next_element_contactfield_bottom)
#                    elif element.index[0]>pg.cutlinerow and pg.elementlist[element.next_element_index].index[0]<pg.cutlinerow:
#                        for contactfield in pg.elementlist[element.next_element_index].contactfield_bottom:
#                            next_element_contactfield_bottom=c.translate(contactfield,[0, -pg.sheet_full_y])
#                            top_dummy.append(next_element_contactfield_bottom)
#                    else:
#                        top_dummy=pg.elementlist[element.next_element_index].contactfield_bottom
#                else:
#                    top_dummy=pg.elementlist[element.next_element_index].contactfield_bottom
            else:
                top_dummy=element.overlaparea_bottom
                bottom_dummy=c.translate(pg.elementlist[element.next_element_index].overlaparea_top,[0,element.page_jump*pg.sheet_full_y])
#                bottom_dummy=list()
#                if pg.angle!=0:
#                    if element.index[0]<pg.cutlinerow and pg.elementlist[element.next_element_index].index[0]>pg.cutlinerow:
#                        for contactfield in pg.elementlist[element.next_element_index].contactfield_top:
#                            next_element_contactfield_top=c.translate(contactfield,[0, -pg.sheet_full_y])
#                            bottom_dummy.append(next_element_contactfield_top)
#                    elif element.index[0]>pg.cutlinerow and pg.elementlist[element.next_element_index].index[0]<pg.cutlinerow:
#                        for contactfield in pg.elementlist[element.next_element_index].contactfield_top:
#                            next_element_contactfield_top=c.translate(contactfield,[0, -pg.sheet_full_y])
#                            bottom_dummy.append(next_element_contactfield_top)
#                    else:
#                        bottom_dummy=pg.elementlist[element.next_element_index].overlaparea_top
#                else:
#                    bottom_dummy=pg.elementlist[element.next_element_index].overlaparea_top              
            element.exitconnector=sf.make_connector(top_dummy,bottom_dummy,pg)
            element.exitconnector_length=0
    return pg           
###############################################################################
def connect_elements(pg):
    t1=d.Disp1('Connecting elements')
    needscorrecting=list()
    for i,element in enumerate(pg.navigationlist):
        if element.element_in_series==1 and pg.include_contactfield:
            if pg.flip_inputconnector:
                dirc=not(element.exit_direction)
            else:
                dirc=element.exit_direction
            if pg.angle>0:
                if element.index[1]>pg.cutlinerow:
                    translator=[0, np.sign(pg.angle)*pg.sheet_full_y]
                    pg.draw_inputconnectortwice=1
                else:
                    translator=[0,0]
                    pg.draw_inputconnectortwice=0
            elif pg.angle<0:
                if element.index[1]<pg.cutlinerow:
                    translator=[0 ,-np.sign(pg.angle)*pg.sheet_full_y]
                    pg.draw_inputconnectortwice=1
                else:
                    translator=[0,0]
                    pg.draw_inputconnectortwice=0
            else:
                translator=[0,0]
                pg.draw_inputconnectortwice=0
            pg.input_connector=sf.connect_it(pg,pg.contactfield.out,c.translate(element.into,translator),dirc)
            pg.input_connector_length=c.findlengthofconnector(pg,pg.contactfield.out,c.translate(element.into,translator))
        elif element.element_in_series==1:
            pg.input_connector=sf.connect_it(pg,element.into,element.into,0)
            pg.inputconnector_length=0
            pg.draw_inputconnectortwice=0
        if not(element.exitconnector_type) or pg.connector_type==1:    
            if i==len(pg.navigationlist)-1:
                if pg.include_contactfield:
                    element.exitconnector=sf.connect_it(pg,element.out,pg.contactfield.into,up_or_down_last_connector(element.out,pg.contactfield.into))
                    element.exitconnector_length=c.findlengthofconnector(pg,element.out,pg.contactfield.into)

                else:
                    element.exitconnector=sf.connect_it(pg,element.out,element.out,0)
                    element.exitconnector_length=0

            else:
                next_element_in=c.translate(pg.navigationlist[i+1].into,[0, element.page_jump*pg.sheet_full_y])
                if not(element.index_in_par[0]==pg.navigationlist[i+1].index_in_par[0]):
                    needscorrecting.append(i)
                element.exitconnector=sf.connect_it(pg,element.out,next_element_in,element.exit_direction)
                if sp.Polygon(element.exitconnector).intersection(sp.Polygon(element.active_area)).area>1e-9 or sp.Polygon(element.exitconnector).intersection(sp.Polygon(pg.navigationlist[i+1].active_area)).area>1e-9:
                    element.exitconnector=sf.connect_it(pg,element.out,next_element_in,not(element.exit_direction))
                element.exitconnector_length=c.findlengthofconnector(pg,element.out,next_element_in)
#        ps.draw("output_files/"+pg.filename+"/"+pg.filename+"_rgb.eps",element.exitconnector,np.random.rand(3))
    # pg=correcting_inputconnector( pg)
    # pg=correcting_exitconnectors( pg,needscorrecting)
    #clipping
#     
    if pg.printer=="Roku":
        for index in pg.navigation:
            element=pg.elementlist[index]
            if element.element_in_series==1:
                pg.input_connector=c.drawable(so.cascaded_union([c.polyable(pg.input_connector),c.polyable(pg.elementlist[index].entrance)]).convex_hull)
            if element.next_element_index==len(pg.elementlist):
                element.exitconnector=c.drawable(so.cascaded_union([c.polyable(element.exitconnector),c.polyable(element.exit)]).convex_hull)
            elif not(element.exitconnector_type):
                element.exitconnector=c.drawable(so.cascaded_union([c.polyable(pg.elementlist[element.next_element_index].entrance),c.polyable(element.exitconnector),c.polyable(element.exit)]).convex_hull)
    d.Disp2(t1)
    return pg
##############################################################################
def correcting_inputconnector(pg):
    printingarea=so.cascaded_union([sp.Polygon(pg.printingarea),sp.Polygon(c.translate(pg.printingarea,[0,-0.9*pg.sheet_full_y])),sp.Polygon(c.translate(pg.printingarea,[0,+0.9*pg.sheet_full_y]))])
    elementarea=so.cascaded_union([sp.Polygon(pg.elementarea),sp.Polygon(c.translate(pg.elementarea,[0,-0.9*pg.sheet_full_y])),sp.Polygon(c.translate(pg.elementarea,[0,+0.9*pg.sheet_full_y]))])
    borderspace=printingarea.difference(sp.Polygon(elementarea))
    inputconnector=sp.Polygon(pg.input_connector)
    pg.input_connector=c.drawable(so.cascaded_union([inputconnector.intersection(printingarea),inputconnector.intersection(borderspace).convex_hull])) 
    return pg
###############################################################################
def correcting_exitconnectors(pg,needscorrecting):
    printingarea=so.cascaded_union([sp.Polygon(pg.printingarea),sp.Polygon(c.translate(pg.printingarea,[0,-0.9*pg.sheet_full_y])),sp.Polygon(c.translate(pg.printingarea,[0,+0.9*pg.sheet_full_y]))])
    elementarea=so.cascaded_union([sp.Polygon(pg.elementarea),sp.Polygon(c.translate(pg.elementarea,[0,-0.9*pg.sheet_full_y])),sp.Polygon(c.translate(pg.elementarea,[0,+0.9*pg.sheet_full_y]))])
    borderspace=printingarea.difference(sp.Polygon(elementarea))
    for i in needscorrecting:
        connector=sp.Polygon(pg.navigationlist[i].exitconnector)
        pg.navigationlist[i].exitconnector=c.drawable(so.cascaded_union([connector.intersection(printingarea),connector.intersection(borderspace).convex_hull]))
    return pg
###############################################################################
def up_or_down_last_connector(out_last_element,in_contactfield):
    return c.get_midpoint(out_last_element)[1]>c.get_midpoint(in_contactfield)[1]    
###############################################################################
def set_markers(pg):
    t1=d.Disp1('Setting markers')
    if pg.setmarkers:
        pg.marker=gc.voidclass()
        pg.marker.firstpress=dict()
        pg.marker.secondpress=dict()
        pg.marker.thirdpress=dict()
        pg=sf.set_syncmarkers(pg)
        pg=sf.set_shrink_markers(pg)
        pg=sf.set_cross_markers(pg)
        pg=sf.set_circle_markers(pg)
        pg=sf.set_antiboxingstrucure(pg)
        pg=sf.set_controlbar(pg)
    d.Disp2(t1)
    return pg
###############################################################################
def periodic_enlargement(pg):
#    for point in pg.input_connector:
#                    if point[0]<np.average(pg.input_connector[:,0]):
#                        point[0]=point[0]+(pg.num_of_columns-3)*pg.num_of_columns*pg.delta_element_x
#                    else:
#                        point[0]=point[0]+(pg.num_of_columns-2)*pg.num_of_columns*pg.delta_element_x
    for element in pg.elementlist:
#        print(element.index[1])
#        if element.index[1]!=0:
#            element.overlaparea_top[0][0]=element.overlaparea_top[0][0]+(element.index[1]-1)*pg.delta_element_x
#            element.overlaparea_top[1][0]=element.overlaparea_top[1][0]+element.index[1]*pg.delta_element_x
#            element.overlaparea_top[2][0]=element.overlaparea_top[2][0]+element.index[1]*pg.delta_element_x
#            element.overlaparea_top[3][0]=element.overlaparea_top[3][0]+(element.index[1]-1)*pg.delta_element_x
#            element.overlaparea_bottom[0][0]=element.overlaparea_bottom[0][0]+(element.index[1]-1)*pg.delta_element_x
#            element.overlaparea_bottom[1][0]=element.overlaparea_bottom[1][0]+element.index[1]*pg.delta_element_x
#            element.overlaparea_bottom[2][0]=element.overlaparea_bottom[2][0]+element.index[1]*pg.delta_element_x
#            element.overlaparea_bottom[3][0]=element.overlaparea_bottom[3][0]+(element.index[1]-1)*pg.delta_element_x
#            if element.exitconnector_type:
#                element.exitconnector[0][0]=element.exitconnector[0][0]+element.index[1]*(element.index[1]-1)*pg.delta_element_x
#                element.exitconnector[1][0]=element.exitconnector[1][0]+element.index[1]*element.index[1]*pg.delta_element_x
#                element.exitconnector[2][0]=element.exitconnector[2][0]+element.index[1]*element.index[1]*pg.delta_element_x
#                element.exitconnector[3][0]=element.exitconnector[3][0]+element.index[1]*(element.index[1]-1)*pg.delta_element_x
#            else:
#                for point in element.exitconnector:
#                    if point[0]<np.average(element.exitconnector[:,0]):
#                        point[0]=point[0]+(element.index[1]-2)**2*pg.delta_element_x
#                    else:
#                        point[0]=point[0]+element.index[1]*(element.index[1])*pg.delta_element_x
#            element.bottom[0][0]=element.bottom[0][0]+(element.index[1]-1)*pg.delta_element_x
#            element.bottom[1][0]=element.bottom[1][0]+element.index[1]*pg.delta_element_x
#            element.bottom[2][0]=element.bottom[2][0]+element.index[1]*pg.delta_element_x
#            element.bottom[3][0]=element.bottom[3][0]+(element.index[1]-1)*pg.delta_element_x
            for i,strip in enumerate(element.stripes):
#                strip[0][0]=strip[0][0]+element.index[1]*(element.index[1]-1)*pg.delta_element_x
#                strip[1][0]=strip[1][0]+element.index[1]*element.index[1]*pg.delta_element_x
#                strip[2][0]=strip[2][0]+element.index[1]*element.index[1]*pg.delta_element_x
#                strip[3][0]=strip[3][0]+element.index[1]*(element.index[1]-1)*pg.delta_element_x
                strip[0][0]=strip[0][0]+i*(i-1)*pg.delta_element_x
                strip[1][0]=strip[1][0]+i*i*pg.delta_element_x
                strip[2][0]=strip[2][0]+i*i*pg.delta_element_x
                strip[3][0]=strip[3][0]+i*(i-1)*pg.delta_element_x
#                element.contactfield_top[i][0][0]=element.contactfield_top[i][0][0]+(element.index[1]-1)*pg.delta_element_x
#                element.contactfield_top[i][1][0]=element.contactfield_top[i][1][0]+element.index[1]*pg.delta_element_x
#                element.contactfield_top[i][2][0]=element.contactfield_top[i][2][0]+element.index[1]*pg.delta_element_x
#                element.contactfield_top[i][3][0]=element.contactfield_top[i][3][0]+(element.index[1]-1)*pg.delta_element_x
#                element.contactfield_bottom[i][0][0]=element.contactfield_bottom[i][0][0]+(element.index[1]-1)*pg.delta_element_x
#                element.contactfield_bottom[i][1][0]=element.contactfield_bottom[i][1][0]+element.index[1]*pg.delta_element_x
#                element.contactfield_bottom[i][2][0]=element.contactfield_bottom[i][2][0]+element.index[1]*pg.delta_element_x
#                element.contactfield_bottom[i][3][0]=element.contactfield_bottom[i][3][0]+(element.index[1]-1)*pg.delta_element_x
#            element.exitconnector[0][0]=element.exitconnector[0][0]+element.index[1]*(element.index[1]-1)*pg.delta_element_x
            element.exitconnector[0][0]=element.exitconnector[0][0]+(len(element.stripes)**2-12)*pg.delta_element_x
            element.exitconnector[3][0]=element.exitconnector[3][0]+(len(element.stripes)**2-12)*pg.delta_element_x
            element.exitconnector[4][0]=element.exitconnector[4][0]+(len(element.stripes)**2-12)*pg.delta_element_x
#            pg.input_connector[0][0]=pg.input_connector[0][0]+(len(element.stripes)**2-12)*pg.delta_element_x
#            pg.input_connector[3][0]=pg.input_connector[3][0]+(len(element.stripes)**2-12)*pg.delta_element_x
#            pg.input_connector[2][0]=pg.input_connector[2][0]+(i)*pg.delta_element_x
#            element.exitconnector[3][0]=element.exitconnector[3][0]+element.index[1]*(element.index[1]-1)*pg.delta_element_x
    return pg