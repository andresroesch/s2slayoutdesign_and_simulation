# -*- coding: utf-8 -*-
"""
Created on Thu Jan 11 18:34:12 2018

@author: andres
"""
from imp import reload 
import shapely.geometry as sp
import shapely.ops as so
import numpy as np
import disp as d
import globalclasses as gc
import custommathfunctions as c
import shapefunctions_s2s as sf
import directions as dr
import customboolean as cb
import pswriter_s2s as ps
import simulationareas as sa
import simulation_simple as ss
import simulation_kppv as sk
import iodata as inout
reload(sk)
reload(inout)
reload(sa)
reload(ps)
reload(cb)
reload(dr)
reload(sf)
reload(c)
reload(gc)
reload (d)
###############################################################################
def simulate_generator(filename,s):
    pg,te,l=inout.get_layout(filename)
    output=gc.voidclass()
    s=load_scenario(s)
    s=load_materials(s)
    pg,te,l,output=kppv_model(pg,te,l,s,output)
    output=make_monitor(output)
    return pg,te,l,output
###############################################################################
def load_scenario(s):
    s.source=gc.voidclass()
    s.sink=gc.voidclass()
    cdata=np.genfromtxt("Scenarios/"+s.scenario+".txt",dtype='float',skip_header=2,max_rows=1)
    s.source.temperature=cdata[0]
    s.source.th_resistance=cdata[1]
    s.sink.temperature=cdata[2]
    s.sink.th_resistance=cdata[3]
    s.el_load=cdata[4]
    return s
###############################################################################
def load_materials(s):
    materials=["p","n","c","substrate"]
    for material in materials:
        s.addatr(material,gc.voidclass())
        cdata=np.genfromtxt("Materials/"+s.g(material+"_material")+".txt",dtype='float',skip_header=2,max_rows=1)
        s.g(material).solidcontent=cdata[0]
        s.g(material).costs=cdata[1]
        raw=np.genfromtxt("Materials/"+s.g(material+"_material")+".txt",dtype='float',skip_header=4)
        s.g(material).temperature=raw[:,0]
        s.g(material).seebeck=raw[:,1]
        s.g(material).el_cond=raw[:,2]
        s.g(material).th_cond=raw[:,3]
        s.g(material).zt=raw[:,4]
    s.air=gc.voidclass()
    airdata=np.genfromtxt("Materials/Air.txt",dtype='float',skip_header=2,max_rows=1)
    s.g(material).solidcontent=airdata[0]
    s.g(material).costs=airdata[1]
    airraw=np.genfromtxt("Materials/Air.txt",dtype='float',skip_header=4)
    s.air.temperature=airraw[:,0]
    s.air.seebeck=airraw[:,1]
    s.air.el_cond=airraw[:,2]
    s.air.th_cond=airraw[:,3]
    s.air.zt=airraw[:,4]
    return s
###############################################################################
def simple_model(pg,te,l,s,output):
    t1=d.Disp1('Simulating')
    s=ss.simplify_material_parameters(s)
    output=sa.count_thermocouples(pg,output)
    output.filename=pg.filename
    output.pg=pg
    output.te=te
    output.l=l
    output.s=s
    output.model= "Simple"
    im=sk.create_image(pg)
    output=sa.calculating_areas(im,te,output)
    output=sa.calculating_degree_of_filling(pg,te,output)
    output=sa.calculating_material_usage(pg,te,s,output)
    output=ss.calculate_thermal_resistance_of_generator(pg,te,s,output)
    output=ss.calculate_electrical_resistance_of_generator(pg,te,l,s,output)
    output=ss.calculate_voltage_per_kelvin(pg,te,s,output)
    output.power_per_kelvin_squared=0.25*output.voltage_per_kelvin**2/output.electrical_resistance #power in mpp
    output.deltaT=s.source.temperature-s.sink.temperature
    output.deltaTatTEG=output.deltaT
    output.Tm=s.source.temperature/2+s.sink.temperature/2
    output.openvoltage=output.voltage_per_kelvin*(output.deltaTatTEG*output.ratio_for_eff_dT)
    output.voltage=output.openvoltage*s.el_load/(output.electrical_resistance+s.el_load)
    output.poweratload=output.voltage**2/s.el_load
    output.eff_Z=output.voltage_per_kelvin**2*output.thermal_resistance/(output.electrical_resistance)
    output.max_power=output.power_per_kelvin_squared*(output.deltaTatTEG*output.ratio_for_eff_dT)**2
    output.heatflow=output.deltaTatTEG/output.thermal_resistance
    output.ZT=output.voltage_per_kelvin**2*(output.Tm)*output.thermal_resistance/(output.electrical_resistance)
    output.efficiency=output.deltaT/s.source.temperature*(np.sqrt(1+output.ZT)-1)/(np.sqrt(1+output.ZT)+s.sink.temperature/s.source.temperature)
    output.eta=output.poweratload/output.heatflow
    output.s=s
    d.Disp2(t1) 
    return pg,te,l,output
###############################################################################
def kppv_model(pg,te,l,s,output):
    s=ss.simplify_material_parameters(s)
    output=sa.count_thermocouples(pg,output)
    output=sa.calculating_areas(pg,te,output)
    output=sa.calculating_degree_of_filling(pg,te,output)
    output=sa.calculating_material_usage(pg,te,s,output)
    # output=ss.calculate_thermal_resistance_of_generator(pg,te,s,output)
    output.filename=pg.filename
    output.deltaT=s.source.temperature-s.sink.temperature
    output.deltaTatTEG=output.deltaT
    output.deltaT_effective=output.deltaTatTEG#*output.ratio_for_eff_dT
    output.Tm=s.source.temperature/2+s.sink.temperature/2
    output.pg=pg
    output.te=te
    output.l=l
    output.s=s
    output.model= "KPPV"   
    output=sk.simulate_generator(output)
    output.voltage_per_kelvin=output.openvoltage/output.deltaTatTEG
    output.power_per_kelvin_squared=0.25*output.voltage_per_kelvin**2/output.electrical_resistance #power in mpp
    output.voltage=output.openvoltage*s.el_load/(output.electrical_resistance+s.el_load)
    output.poweratload=output.voltage**2/s.el_load
    output.eff_Z=output.voltage_per_kelvin**2*output.thermal_resistance/(output.electrical_resistance)
    output.max_power=output.power_per_kelvin_squared*(output.deltaTatTEG*output.ratio_for_eff_dT)**2
    output.heatflow=output.deltaTatTEG/output.thermal_resistance
    output.ZT=output.voltage_per_kelvin**2*(output.Tm)*output.thermal_resistance/(output.electrical_resistance)
    output.efficiency=output.deltaT/s.source.temperature*(np.sqrt(1+output.ZT)-1)/(np.sqrt(1+output.ZT)+s.sink.temperature/s.source.temperature)
    output.eta=output.poweratload/output.heatflow
    return pg,te,l,output
###############################################################################
def rough_model(pg,te,l,s,output):
    t1=d.Disp1('Simulating')
    s=ss.simplify_material_parameters(s)
    d.Disp2(t1) 
    return  pg,te,l,output

def make_monitor(output):
    output.monitor=list()
    output.monitor.append("\n")
    output.monitor.append("#################OUTPUT##################\n")          
    output.monitor.append("Simulation model:\t\t"+output.model)
    output.monitor.append("\nMATERIALS:")
    output.monitor.append("p-type:\t\t\t\t"+output.s.p_material)
    output.monitor.append("    S:\t\t\t\t"+str(output.s.p_seebeck*1000000)+" "+d.greek("MU",0)+"V/K")
    output.monitor.append("    "+d.greek("SIGMA",0)+":\t\t\t\t"+str(output.s.p_el_conductivity/100)+" S/cm")
    output.monitor.append("    "+d.greek("KAPPA",0)+":\t\t\t\t"+str(output.s.p_th_conductivity)+" W/mK")
    output.monitor.append("    Layer thickness:\t\t"+str(round(output.s.p_thickness*1000000,3))+" "+d.greek("MU",0)+"m")
    output.monitor.append("    Wetfilm thickness:\t\t"+str(round(output.s.p_wetfilm_thickness*1000000,3))+" "+d.greek("MU",0)+"m")
    output.monitor.append("n-type:\t\t\t\t"+output.s.n_material)
    output.monitor.append("    S:\t\t\t\t"+str(output.s.n_seebeck*1000000)+" "+d.greek("MU",0)+"V/K")
    output.monitor.append("    "+d.greek("SIGMA",0)+":\t\t\t\t"+str(output.s.n_el_conductivity/100)+" S/cm")
    output.monitor.append("    "+d.greek("KAPPA",0)+":\t\t\t\t"+str(output.s.n_th_conductivity)+" W/mK")
    output.monitor.append("    Layer thickness:\t\t"+str(round(output.s.n_thickness*1000000,3))+" "+d.greek("MU",0)+"m")
    output.monitor.append("    Wetfilm thickness:\t\t"+str(round(output.s.n_wetfilm_thickness*1000000,3))+" "+d.greek("MU",0)+"m")
    output=d.display_type_of_connectormaterial(output)
    output.monitor.append("Substrate:\t\t\t"+str(round(output.s.substrate_thickness*1000000,3))+" "+d.greek("MU",0)+"m "+output.s.substrate_material)
    output.monitor.append("\nLAYOUT:")
    output.monitor.append("Filename:\t\t\t"+output.filename)
    output.monitor.append("printer:\t\t\t"+output.pg.printer)
    output.monitor.append("Tilt angle:\t\t\t"+str(round(output.pg.angle,1)))
    output.monitor.append("Number of thermocouples:\t"+str(output.number_of_thermocouples))
    output.monitor.append("Fold depth:\t\t\t"+str(round(output.te.cell_y*1000, 3))+" mm")
    output.monitor.append("Width p-element:\t\t"+str(round(output.te.p.effective_x*1000, 3))+" mm")
    output.monitor.append("Width n-element:\t\t"+str(round(output.te.n.effective_x*1000, 3))+" mm")
    output.monitor.append("Width connectors:\t\t"+str(round(output.pg.connector_width*1000, 3))+" mm")
    output.monitor.append("Spacing between columns:\t"+str(round(output.te.spacing_x*1000, 3))+" mm")
    output.monitor.append("Areas:")
    output.monitor.append("        p:\t\t\t"+str(round(output.p_area, 5))+" m²")
    output.monitor.append("        n:\t\t\t"+str(round(output.n_area, 5))+" m²")
    output.monitor.append("        Generator:\t\t"+str(round(output.generator_area, 5))+" m²")
    output.monitor.append("Degree of filling:\t\tSubs.\tPrint area")
    output.monitor.append("        p:\t\t\t"+str(round(output.p_degree_of_filling_substrate*100, 3))+"%\t"+str(round(output.p_degree_of_filling_printingarea*100, 3))+"%")
    output.monitor.append("        n:\t\t\t"+str(round(output.n_degree_of_filling_substrate*100, 3))+"%\t"+str(round(output.n_degree_of_filling_printingarea*100, 3))+"%")
    output.monitor.append("        Generator:\t\t"+str(round(output.generator_degree_of_filling_substrate*100, 3))+"%\t"+str(round(output.generator_degree_of_filling_printingarea*100, 3))+"%")
#    output.monitor.append("        Generator:\t\t"+str(round(output.generator_area, 5)))
    output.monitor.append("\nGENERATOR:")
    output.monitor.append("Number of thermocouples:\t"+str(output.number_of_thermocouples))
    output.monitor.append("Eff. "+d.greek("DELTA",1)+"T/K:\t\t\t"+str(round(output.ratio_for_eff_dT,3)))
    output.monitor.append("OC-voltage/K:\t\t\t"+str(round(output.voltage_per_kelvin*1000,3))+" mV/K")
    output.monitor.append("El. resistance:\t\t\t"+str(round(output.electrical_resistance,3))+" "+d.greek("OMEGA",1))
    output.monitor.append("Th. resistance:\t\t\t"+str(round(output.thermal_resistance,3))+" K/W")
    output.monitor.append("Max. power/K²:\t\t\t"+str(round(output.power_per_kelvin_squared*1000000,3))+" "+d.greek("MU",0)+"W/K²")
    output.monitor.append("Eff. Z-value:\t\t\t"+str(round(output.eff_Z,8))+" 1/K")
    output.monitor.append("Material usage:")
    output.monitor.append("        p:\t\t\t"+str(round(output.p_material_usage*1000000, 3))+" ml")
    output.monitor.append("        n:\t\t\t"+str(round(output.n_material_usage*1000000, 3))+" ml")
    output.monitor.append("        Generator:\t\t"+str(round(output.generator_material_usage*1000000, 3))+" ml")
    output.monitor.append("\nSCENARIO:")
    output.monitor.append("Scenario name:\t\t\t"+output.s.scenario)
    output.monitor.append("T heat source:\t\t\t"+str(output.s.source.temperature)+" K ("+str(output.s.source.temperature-273.15)+" °C)")
    output.monitor.append("T heat sink:\t\t\t"+str(output.s.sink.temperature)+" K ("+str(output.s.sink.temperature-273.15)+" °C)")
    output.monitor.append(d.greek("DELTA",1)+"T:\t\t\t\t"+str(output.deltaT)+" K")
    output.monitor.append("Tm:\t\t\t\t"+str(output.Tm)+" K ("+str(output.Tm-273.15)+" °C)")
    output.monitor.append("Rth heat source:\t\t"+str(output.s.source.th_resistance)+" K/W")
    output.monitor.append("Rth heat sink:\t\t\t"+str(output.s.sink.th_resistance)+" K/W")
    output.monitor.append("Rel load:\t\t\t"+str(output.s.el_load)+" "+d.greek("OMEGA",1))
    output.monitor.append("\nOTEG IN SCENARIO:")
    output.monitor.append(d.greek("DELTA",1)+"T@TEG:\t\t\t\t"+str(output.deltaTatTEG)+" K")
    output.monitor.append("OC Voltage:\t\t\t"+str(round(output.openvoltage,3))+" V")
    output.monitor.append("Voltage:\t\t\t"+str(round(output.voltage,3))+" V")
    output.monitor.append("Max output power:\t\t"+str(round(output.max_power*1000000,3))+" "+d.greek("MU",0)+"W")
    output.monitor.append("Power at load:\t\t\t"+str(round(output.poweratload*1000000,3))+" "+d.greek("MU",0)+"W")
    output.monitor.append("Heat flow:\t\t\t"+str(round(output.heatflow,3))+" W")
    output.monitor.append("PCE:\t\t\t\t"+str(round(output.eta,8)))
    output.monitor.append("Eff. ZT-value:\t\t\t"+str(round(output.ZT,8)))
    output.monitor.append("Efficiency:\t\t\t"+str(round(output.efficiency,8)))
    return output