# -*- coding: utf-8 -*-
"""
Created on Mon Aug 21 18:16:27 2017

@author: roesch
"""

from imp import reload
import os
import subprocess
import numpy as np 
import iodata as inout
import disp as d
import globalclasses as gc
import custommathfunctions as c
import dimentioningfunctions_s2s as df
import buildingfunctions_s2s as bf
import shapefunctions_s2s as sf
import pswriter_s2s as ps
import directions as dr
import customboolean as cb

reload(dr)
reload(ps)
reload(bf)
reload(sf)
reload(c)
reload (d)
reload (df)
reload(gc)
reload (inout)

def generate(filename):
    inputdata=inout.read_input(filename)
    pg=inputdata["p"]
    te=inputdata["te"]
    l=inputdata["l"]
    del inputdata
    # pg=df.adjust_angle(pg)
    pg.angle=0
    pg.printer="Roku" # Gallus or Roku
    pg.connector_type=4
    pg.include_contactfield=0
    pg.setmarkers=0
    pg.shrinkfactor_unit1=np.array(0.0)
    pg.shrinkfactor_unit2=np.array(0.0)
    pg.shrinkfactor_unit3=np.array(0.0)
    pg=ps.newblanksheets(pg)
    pg,te,l=df.calculate_dimensions_thermoelement(pg,te,l)    
    # pg=bf.set_markers(pg)
    pg=df.calculation_of_drawing_area(pg,te)
    pg=df.correction_for_periodicity(pg,te)
    if pg.no_c_material_between_elements:
        pg,te,l=df.adaptation_of_cell_dimensions_for_no_c_material(pg,te,l)
    elif pg.effective_y_optimization:
        te,l= df.adaptation_of_cell_dimensions_for_effective_y_optimization( pg,te,l)
    else:
        te =  df.adaptation_of_cell_dimensions( pg,te)
    pg.cell_n_x=te.n.cell_x
    pg.cell_p_x=te.p.cell_x
    pg.cell_x=(pg.cell_n_x+pg.cell_p_x)/2
    # pg=bf.findnumberofdeletedrows( pg,te)
    te=sf.form_shapes_of_thermoelement(te)
    l=sf.make_linker(pg,te,l) 
    pg=bf.tiltedchecker(pg)
    pg=bf.set_cutline(pg)
    # if bool(pg.include_contactfield):
    #     pg=sf.make_contactfield(pg,te)
    pg=bf.set_thermoelements(pg,te,l)
    pg=bf.seriesing(pg)    
    if pg.printer=="Gallus":
        pg=bf.delete_last_row(pg)
        pg=bf.delete_elements_in_second_joint(pg)
    pg=bf.set_connectors( pg,l)
#    if pg.periodic_enlargement:
#        pg=bf.periodic_enlargement(pg)
    pg=bf.connect_elements(pg)
    if pg.periodic_enlargement:
        pg=bf.periodic_enlargement(pg)
    # if pg.printer=="Gallus":
    #      pg=bf.bridge_second_joint(pg)
    # if pg.setmarkers:
    #     pg=sf.set_fold_markers(pg)
    inout.shelve_layout(pg,te,l)
    return pg,te,l
###############################################################################
def drawing_generator(filename):
    pg,te,l=inout.get_layout(filename)
    t1=d.Disp1('Drawing Generator...')
    t9=d.Disp1('    Drawing markers')
    ps.draw_markers(pg)
    d.Disp2(t9)
    materials=[pg.material_unit1,pg.material_unit2,pg.material_unit3]
    shrinkfactors=[0,0,0]
    ci=materials.index('c')+1
    ni=materials.index('n')+1
    pi=materials.index('p')+1
    coni=materials.index(pg.connectormaterial)+1
    file=dict()
    # if pg.print_numeration:
    #     ps.draw_numbermarkers(pg)
    fhs=list()
    # if pg.write_infobox:
    #    t6=d.Disp1("    Writing textbox") 
    #    ps.write_infobox(pg)
    #    d.Disp2(t6)
    
    for sheet in pg.sheetnames:
            fhs.append(open(sheet,'a'))
    pg.left=dr.left(pg.angle)
    #Darwing Areas
    ps.f_ps_make_polygon_open( fhs[0], pg.substrate,0.2, [0, 0, 0] )
    ps.f_ps_make_polygon_open( fhs[0], pg.printingarea,0.3,pg.color_drawingarea )
    ps.f_ps_make_polygon_open( fhs[0], pg.elementarea,0.3,  pg.color_elementarea )
    color=dict()
    color['n']=pg.color_n
    color['p']=pg.color_p
    color['c']=pg.color_c
   
    if pg.write_indicies:
        t5=d.Disp1("    Writing indices")
        for i,element in enumerate(pg.elementlist):
#            ps.f_write_text("output_files/"+pg.filename+"/"+pg.filename+"_rgb.eps",str(element.index_in_par),5,element.midpoint,[0,0,0])
#            ps.f_write_text("output_files/"+pg.filename+"/"+pg.filename+"_rgb.eps",str(element.index),5,element.midpoint-0.001,[1,0,0])
#            ps.f_write_text("output_files/"+pg.filename+"/"+pg.filename+"_rgb.eps",str(element.next_element_index),5,element.midpoint+0.001,[0,1,1])
           ps.f_write_text("output_files/"+pg.filename+"/"+pg.filename+"_rgb.eps",str(element.element_in_series),9,element.midpoint,pg.color_c)
        d.Disp2(t5)
    if pg.include_contactfield:
        t2=d.Disp1('    Drawing contactfield')
        ps.f_ps_make_polygon_filled_shrinkfactor(fhs[0],pg.contactfield.main,color[pg.connectormaterial],pg.shrinkfactor_unit3,pg.sheet_full_x)
        ps.f_ps_make_polygon_filled_shrinkfactor(fhs[coni],pg.contactfield.main,pg.color_design,shrinkfactors[coni-1],pg.sheet_full_x)
        if pg.draw_window:
            ps.f_ps_make_polygon_filled_shrinkfactor(fhs[0],c.translate(pg.contactfield.window,-0.2*pg.left*pg.window_length),[1, 1, 1],pg.shrinkfactor_unit3,pg.sheet_full_x)
            ps.f_ps_make_polygon_filled_shrinkfactor(fhs[coni],c.translate(pg.contactfield.window,-0.2*pg.left*pg.window_length),[1, 1, 1],shrinkfactors[coni-1],pg.sheet_full_x)
            ps.f_ps_make_polygon_filled_shrinkfactor(fhs[0],c.translate(pg.contactfield.windowfield,9.8*pg.left*pg.window_length),color[pg.connectormaterial],pg.shrinkfactor_unit3,pg.sheet_full_x)
            ps.f_ps_make_polygon_filled_shrinkfactor(fhs[coni],c.translate(pg.contactfield.windowfield,9.8*pg.left*pg.window_length),pg.color_design,shrinkfactors[coni-1],pg.sheet_full_x)
        if cb.isitofftop(pg.contactfield.main,pg.sheetlimits):
            ps.f_ps_make_polygon_filled_shrinkfactor(fhs[0],c.translate(pg.contactfield.main,[0, -pg.sheet_full_y]),color[pg.connectormaterial],pg.shrinkfactor_unit3,pg.sheet_full_x)
            ps.f_ps_make_polygon_filled_shrinkfactor(fhs[coni],c.translate(pg.contactfield.main,[0, -pg.sheet_full_y]),pg.color_design,shrinkfactors[coni-1],pg.sheet_full_x)
        elif cb.isitoffbottom(pg.contactfield.main,pg.sheetlimits):
            ps.f_ps_make_polygon_filled_shrinkfactor(fhs[0],c.translate(pg.contactfield.main,[0, pg.sheet_full_y]),color[pg.connectormaterial],pg.shrinkfactor_unit3,pg.sheet_full_x)
            ps.f_ps_make_polygon_filled_shrinkfactor(fhs[coni],c.translate(pg.contactfield.main,[0, pg.sheet_full_y]),pg.color_design,shrinkfactors[coni-1],pg.sheet_full_x)
        d.Disp2(t2)
    file=dict()
    sf=dict()
    sf['n']=shrinkfactors[ni-1]
    sf['p']=shrinkfactors[pi-1]
    file['n']=fhs[ni]
    file['p']=fhs[pi]
    for i,material in enumerate(materials):
        t3=d.Disp1('    Drawing '+material+'-material in unit '+str(i+1))
        if i==2 and pg.printer=="Gallus":
            offset=-pg.second_screen_joint
        else:
            offset=0
            shift=0
        for element in pg.elementlist:
            if element.midpoint[1]<pg.second_screen_joint and i==2 and pg.printer=="Gallus":
                shift=pg.sheet_full_y
            else: 
                shift=0
            if element.type==material:
                for strip in element.stripes:
                    ps.f_ps_make_polygon_filled_shrinkfactor(fhs[0],strip,color[element.type],pg.shrinkfactor_unit3,pg.sheet_full_x)
                    ps.f_ps_make_polygon_filled_shrinkfactor(file[element.type],strip+np.array([0,offset+shift]),pg.color_design,sf[element.type],pg.sheet_full_x)
        if material==pg.connectormaterial:
            if pg.draw_inputconnectortwice:
                ps.f_ps_make_polygon_filled_shrinkfactor(fhs[0],c.translate(pg.input_connector,[0, -np.sign(pg.angle)*pg.sheet_full_y]),color[pg.connectormaterial],pg.shrinkfactor_unit3,pg.sheet_full_x)
                ps.f_ps_make_polygon_filled_shrinkfactor(fhs[coni],c.translate(pg.input_connector,[0, -np.sign(pg.angle)*pg.sheet_full_y]),pg.color_design,shrinkfactors[coni-1],pg.sheet_full_x)
            ps.f_ps_make_polygon_filled_shrinkfactor(fhs[0],pg.input_connector,color[pg.connectormaterial],pg.shrinkfactor_unit3,pg.sheet_full_x)
            ps.f_ps_make_polygon_filled_shrinkfactor(fhs[coni],pg.input_connector,pg.color_design,shrinkfactors[coni-1],pg.sheet_full_x)
        for element in pg.elementlist:
            if c.get_midpoint(element.exitconnector)[1]<pg.second_screen_joint and i==2 and pg.printer=="Gallus":
                shift=pg.sheet_full_y
            else: 
                shift=0
            if element.exitconnector_mat==material:
                if element.page_jump!=0:
                    ps.f_ps_make_polygon_filled_shrinkfactor(fhs[0],c.translate(element.exitconnector,[0, -element.page_jump*pg.sheet_full_y]),color[element.exitconnector_mat],pg.shrinkfactor_unit3,pg.sheet_full_x)
                if pg.no_c_material_between_elements:
                    ps.f_ps_make_polygon_filled_shrinkfactor(fhs[0],element.exitconnector,color[element.exitconnector_mat],pg.shrinkfactor_unit3,pg.sheet_full_x)
                    ps.f_ps_make_polygon_filled_shrinkfactor(file[element.exitconnector_mat],element.exitconnector+np.array([0,offset+shift]),pg.color_design,shrinkfactors[coni-1],pg.sheet_full_x)
                    
                else:
                    ps.f_ps_make_polygon_filled_shrinkfactor(fhs[0],element.exitconnector,color[element.exitconnector_mat],pg.shrinkfactor_unit3,pg.sheet_full_x)
                    ps.f_ps_make_polygon_filled_shrinkfactor(file[element.exitconnector_mat],element.exitconnector+np.array([0,offset+shift]),pg.color_design,shrinkfactors[coni-1],pg.sheet_full_x)
        d.Disp2(t3)
#    for element in pg.elementlist:
#        ps.f_ps_make_polygon_filled_shrinkfactor(fhs[0],element.overlaparea_top,[1, 0, 1],0,pg.sheet_full_x)
#        ps.f_ps_make_polygon_filled_shrinkfactor(fhs[0],element.overlaparea_bottom,[1, 0, 1],0,pg.sheet_full_x)
   
    
    for fh in fhs:
        fh.close()
    d.Disp2(t1)
    t8=d.Disp1('Creating pdf files...')
    convertepstopdf(pg)
    
    d.Disp2(t8)
#    im = Image.open( pg.sheetnames[0])
#    im.save("display.eps","eps")
###############################################################################
def convertepstopdf(pg):
    os.chdir("output_files/"+pg.filename)
    for sheet in pg.sheetnames:
        string=sheet
        string=string.replace("output_files/"+pg.filename+"/","")
        subprocess.Popen("epstopdf "+string+" --outfile "+string.replace("eps","pdf"),shell=True)
    os.chdir("..")
    os.chdir("..")

