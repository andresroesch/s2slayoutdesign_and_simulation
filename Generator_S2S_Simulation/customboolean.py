# -*- coding: utf-8 -*-
"""
Created on Sun Sep  3 14:26:07 2017

@author: Andres
"""
from imp import reload
import numpy as np
import buildingfunctions_s2s as bf
import shapely.geometry as sp
import shapely.ops as so
reload(bf)
###############################################################################
def ismidpointin(midpoint,limits):
    return sp.Polygon(limits).contains(sp.Point(midpoint))
###############################################################################   
def isitfree(polygon, midpointlist, no_need_to_check):
    for i in range(no_need_to_check, len(midpointlist)):
       if np.linalg.norm(polygon.midpoint-midpointlist[i])<1e-10:
           return 0
    return 1
###############################################################################
def istherespace( fields, angle, length_x, length_y, limits, indexlist):
    for field in fields:
        temp_up=bf.go_up(field,angle,length_y);
        temp_down=bf.go_down(field,angle,length_y);
        temp_left=bf.go_left(field,angle,length_x);
        temp_right=bf.go_right(field,angle,length_x)
        if ismidpointin(temp_up.midpoint,limits) and not(temp_up.index in indexlist):#
            return True 
        elif ismidpointin(temp_down.midpoint,limits) and not(temp_down.index in indexlist):#(not(any([np.all(row) for row in indexlist==temp_down.index]))):
            return True
        elif ismidpointin(temp_left.midpoint,limits) and not(temp_left.index in indexlist):#(not(any([np.all(row) for row in indexlist==temp_left.index]))):
            return True
        elif ismidpointin(temp_right.midpoint,limits) and not(temp_right.index in indexlist):#(not(any([np.all(row) for row in indexlist==temp_right.index]))):
            return True
        else:
            ans=False
    return False
###############################################################################        
def isitin(polygon,limits):
    return sp.Polygon(limits).intersects(sp.Polygon(polygon))
###############################################################################
def isitfullyin(polygon,limits):
    return limits.contains(sp.Polygon(polygon))
###############################################################################
def isfieldoutofcutline(pg,field):
    if pg.angle==0:
        return True
    else:
        return field.index[0]!=pg.cutlinerow 
###############################################################################
def isitofftop(polygon,limits):
    for point in polygon:
        if point[1]>limits.maximum[1]:
            answer=True
            break
        else:
            answer=False
    return answer 
###############################################################################
def isitoffbottom(polygon,limits):
    for point in polygon:
        if point[1]<limits.minimum[1]:
            answer=True
            break
        else:
            answer=False
    return answer 
###############################################################################
def interfereswithgenerator(shape,pg):
    generatorparts=list()
    generatorparts.append(sp.Polygon(pg.contactfield.main))
    generatorparts.append(sp.Polygon(pg.input_connector))
    for element in pg.elementlist:
        generatorparts.append(sp.Polygon(element.outline))
        generatorparts.append(sp.Polygon(element.exitconnector))
    return so.cascaded_union(generatorparts).intersects(sp.Polygon(shape))
        
    