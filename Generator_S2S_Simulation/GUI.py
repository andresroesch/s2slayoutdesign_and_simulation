# -*- coding: utf-8 -*-
"""
Created on Mon Jan 15 10:20:04 2018

@author: andres
"""
from PyQt5 import *
import sys 
import time
import os
import subprocess
from imp import reload 
import iodata as inout
import GUI_functions as gf
from PyQt5.QtWidgets import QVBoxLayout,QScrollArea,QWidget,QGridLayout,QLineEdit,QCheckBox,QPushButton,QApplication,QFileDialog,QLabel
from PyQt5.QtGui import QFont,QIcon,QFontDatabase,QImage,QImageReader,QPixmap,QWheelEvent
from PyQt5.QtCore import Qt,QUrl,QEvent,QMargins
from PyQt5.QtWebKitWidgets import QWebView
from PyQt5.QtWebKit import *

import mainfunctions as m
import simulation as sim
from PIL import Image,ImageQt
from QtImageViewer import QtImageViewer
reload(sim)
reload(m)
reload(gf)
reload(inout)
class window(QWidget):
    def __init__(self,preview):
        super().__init__()
        self.initMe(preview)   
    def initMe(self,preview):
        self.preview=preview
        inputdata=inout.read_input("default.txt")
        self.values=dict()
        blocks=gf.blocks()
        fieldnames=gf.fieldnames()
        pos=gf.pos()
        initialvalues=gf.load_values(inputdata)
        box=QVBoxLayout(self)
        self.setLayout(box)
        scroll=QScrollArea(self)
        scroll.setWidgetResizable(True)
        scroll.setFrameShadow(True)
        content=QWidget(scroll)
        grid=QGridLayout(content)
        content.setLayout(grid)
        scroll.setWidget(content)
        headline=QLineEdit("OTEGO Layout design")
        headline.setFont(QFont("EncodeSansNormal-Regular",30))
        headline.setStyleSheet("background: rgba(0, 0, 0,0);")
        headline.setFrame(False)
#        event=QWheelEvent(scroll)
#        event.isAccepted()
#        if event.isAccepted():
#            print('yo')
#        frame.setLayout(disp)
      

#        web.setMaximumWidth(600)
#        scene=QGraphicsScene()
#        view=QGraphicsView(scene)
#        pix = QPixmap.fromImage(qim)
#        viewer=QtImageViewer()
#        viewer.aspectRatioMode = Qt.KeepAspectRatioByExpanding
#        viewer.setHorizontalScrollBarPolicy(Qt.ScrollBarAsNeeded)
#        viewer.setVerticalScrollBarPolicy(Qt.ScrollBarAsNeeded)
#        viewer.canZoom = True
#        viewer.canPan = True
#        viewer.leftMouseButtonPressed.connect(self.handleLeftClick)
#        im=Image.open('display.eps').convert('RGBA')
#        qim = ImageQt.ImageQt(im)
#        viewer.setImage(web)
        box.addWidget(headline)
        box.addWidget(scroll)
        grid.setColumnStretch(5, 1)
        for i,block in enumerate(blocks):
            subgrid=QGridLayout()
            back=QWidget()
            if block=="TEG properties:":
                back.setStyleSheet("background: rgba(233, 76, 32,1);")
            blockline=QLineEdit(block)
            blockline.setFont(QFont("EncodeSansNormal-Regular",16))
            blockline.setStyleSheet("background: rgba(0, 0, 0,0);")
            blockline.setFrame(False)
            subgrid.addWidget(blockline,*(1,0))
            for j,name in enumerate(fieldnames[i]):
                if block=="Flags:":
                    self.values[name]=QCheckBox()
                    if bool(initialvalues[name]):
                        self.values[name].setCheckState(2)
                elif block=="TEG properties:":
                    self.values[name]=QLineEdit(str(initialvalues[name]))
                    self.values[name].setFrame(False)
                    self.values[name].setAlignment(Qt.AlignRight)
                    self.values[name].setMaxLength(100)
                    self.values[name].setFont(QFont("EncodeSansNormal-Regular",10))
                    self.values[name].setStyleSheet("background: rgba(0, 0, 0,0);")
                    self.values[name].setStyleSheet("color: rgb(255,255,255);")
                else:
                    self.values[name]=QLineEdit(str(initialvalues[name]))
                    self.values[name].setFrame(False)
                    self.values[name].setAlignment(Qt.AlignRight)
                    self.values[name].setMaxLength(100)
                    self.values[name].setFont(QFont("EncodeSansNormal-Regular",10))
                    
                line=QLineEdit(name)
                line.setStyleSheet("background: rgba(0,0,0,0);")
                if block=="TEG properties:":
                    blockline.setStyleSheet("color: rgb(255, 255, 255);")
                    line.setStyleSheet("color: rgb(255, 255, 255);")
                line.setFrame(False)
#                line.setSizePolicy(QSizePolicy.Fixed,QSizePolicy.Fixed)
                line.setMaxLength(100)
                line.setAlignment(Qt.AlignRight)
                line.setFont(QFont("EncodeSansNormal-Regular", 10))
                subgrid.addWidget(line,*(j+2,0))
#                subgrid.addWidget(filler,*(j+2,2))
                if name[-1]!=":":
                    subgrid.addWidget(self.values[name],*(j+2,1))
#            subgrid.setColumnStretch(1,0)
            back.setLayout(subgrid)        
            grid.addWidget(back,*pos[i])   
        controlpanel=QGridLayout()
        loadb=QPushButton('Load File')
        loadb.clicked.connect(self.loadbuttonpressed)
        saveb=QPushButton('Save File')
        saveb.clicked.connect(self.savebuttonpressed)
        defaultb=QPushButton("Set as default")
        defaultb.clicked.connect(self.defaultbuttonpressed)
        generateb=QPushButton("Generate")
        generateb.clicked.connect(self.generatebuttonpressed)
        simulateb=QPushButton("Simulate")
        simulateb.clicked.connect(self.simulatebuttonpressed)
        openfolderb=QPushButton("Open Output Folder")
        openfolderb.clicked.connect(self.openfolderbuttonpressed)
        controlpanel.addWidget(loadb,*(1,2))
        controlpanel.addWidget(saveb,*(2,2))
        controlpanel.addWidget(defaultb,*(3,2))
        controlpanel.addWidget(generateb,*(4,1))
        controlpanel.addWidget(simulateb,*(4,2))
        controlpanel.addWidget(openfolderb,*(5,1))
        grid.addLayout(controlpanel,*(2,4))
       
        self.setGeometry(50,50,1300,1000)
        self.setWindowTitle("Printed layout design")
        self.setWindowIcon(QIcon("otego_rgb.png"))
        self.show()
        
        
    def loadbuttonpressed(self):
        filename=QFileDialog.getOpenFileName(self,'Open File')
        if not(filename[0]==''):
            loadeddata=inout.read_input(filename[0])
            self.values=gf.display_values(self.values,gf.load_values(loadeddata))
    
    def savebuttonpressed(self):
        pg,te,l,s=gf.get_values(self.values)
        if not(os.path.exists("output_files")):
            os.mkdir("output_files")
        if not(os.path.exists("output_files\\"+pg.filename)):
            os.mkdir("output_files\\"+pg.filename)
        inout.save_output(pg,te,l,"txt")
    
    def defaultbuttonpressed(self):
        pg,te,l,s=gf.get_values(self.values)
        inout.save_default(pg,te,l)
        self.preview.load(QUrl("file:///"+os.path.abspath("leo_rgb.pdf")))
    def generatebuttonpressed(self):
        starttime = time.time()
        pg,te,l,s=gf.get_values(self.values)
        if not(os.path.exists("output_files")):
            os.mkdir("output_files")
        if not(os.path.exists("output_files\\"+pg.filename)):
            os.mkdir("output_files\\"+pg.filename)
        inout.save_output(pg,te,l,s,"txt")
        pg,te,l,s=m.generate("output_files/"+pg.filename+"/"+pg.filename+".txt")
        m.drawing_generator(pg,te)
        endtime = time.time()
        print("Elapsed time is "+str(endtime - starttime)+" seconds.") 
    
    def simulatebuttonpressed(self):
        starttime = time.time()
        pg,te,l=gf.get_values(self.values)
        pg,te,l,s=m.generate("output_files/"+pg.filename+"/"+pg.filename+".txt")
        
        output=sim.simulate_generator(pg,te,l,s)
        endtime = time.time()
        print("Elapsed time is "+str(endtime - starttime)+" seconds.")
    def openfolderbuttonpressed(self):
        pg,te,l,s=gf.get_values(self.values)
        print("start C:\Python_wdir\\Generator_PY\\output_files\\"+pg.filename)
        if not(os.path.exists("output_files\\"+pg.filename)):
            subprocess.call("explorer C:\Python_wdir\\Generator_PY\\output_files")
        else:    
            subprocess.call("explorer C:\Python_wdir\\Generator_PY\\output_files\\"+pg.filename)
app=QApplication(sys.argv)
web=QWebView()
web.settings().setAttribute(QWebSettings.PluginsEnabled,True)
web.load(QUrl("file:///"+os.path.abspath("display_default.pdf")))
web.show()
web.setWindowTitle("TEG Layout Preview")
web.setWindowIcon(QIcon("otego_rgb.png"))
fonts=QFontDatabase()
fonts.addApplicationFont("otegofont.ttf")
w=window(web)
w.setFont(QFont("EncodeSansNormal-Regular", 10))

sys.exit(app.exec_())

