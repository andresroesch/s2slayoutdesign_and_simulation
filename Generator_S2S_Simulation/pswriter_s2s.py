# -*- coding: utf-8 -*-
"""
Created on Thu Sep  7 16:24:12 2017

@author: roesch
"""
from imp import reload 
import os
import numpy as np
import disp as d
import directions as dr
import custommathfunctions as c
import customboolean as cb
import shapely.geometry as sp
import shapely.ops as so
import shapefunctions_s2s as sf
import matplotlib.pyplot as plt
reload(sf)
reload(cb)
reload(c)
reload(dr)
reload(d)

def newblanksheets(pg):
    sheetnames=list()
    files=["rgb",pg.printer+"_unit1_"+pg.material_unit1,pg.printer+"_unit2_"+pg.material_unit2,pg.printer+"_unit3_"+pg.material_unit3]
    for suffix in files:
        sheetnames.append("output_files/"+pg.filename+"/"+pg.filename+"_"+suffix+".eps")
        file=open(sheetnames[-1],"w")
        file.write('%%!PS-Adobe-3.0 EPSF-3.0\n%%%%BoundingBox: 0 0 %i %i \n%%%%LanguageLevel: 2\n%%%%Pages: 1\n%%Orientation: Portrait\n'%(m_to_point(pg.sheet_full_x),m_to_point(pg.sheet_full_y)))
#        fontfile=open(pg.info_box_font+".t42")
#        file.write(fontfile.read())
#        fontfile.close()
        file.close()
    pg.sheetnames=sheetnames
    return pg
###############################################################################
def f_ps_make_polygon_filled(file,polygon,rgb):
    file.write("newpath\n")
    file.write("%f %f moveto\n"%(m_to_point(polygon[0,0]),m_to_point(polygon[0,1])))
    for point in polygon[1:]:
        file.write("%f %f lineto\n"%(m_to_point(point[0]),m_to_point(point[1])))
    file.write("closepath\n")
    file.write("%f %f %f setrgbcolor\n"%(rgb[0],rgb[1],rgb[2]))
    file.write("fill\n")
################################################################################
def f_ps_make_polygon_filled_shrinkfactor(file,polygon,rgb,shrinkfactor,sheet_full_x):
    if shrinkfactor!=0:
        for point in polygon:
            point[0]=(point[0]-sheet_full_x/2)*(1+shrinkfactor)+sheet_full_x/2
    file.write("newpath\n")
    file.write("%f %f moveto\n"%(m_to_point(polygon[0,0]),m_to_point(polygon[0,1])))
    for point in polygon[1:]:
        file.write("%f %f lineto\n"%(m_to_point(point[0]),m_to_point(point[1])))
    file.write("closepath\n")
    file.write("%f %f %f setrgbcolor\n"%(rgb[0],rgb[1],rgb[2]))
    file.write("fill\n")
###############################################################################
def f_ps_make_polygon_open(file,polygon,linewidth,rgb):
    file.write("newpath\n")
    file.write("%f %f moveto\n"%(m_to_point(polygon[0,0]),m_to_point(polygon[0,1])))
    for point in polygon[1:]:
        file.write("%f %f lineto\n"%(m_to_point(point[0]),m_to_point(point[1])))
    file.write("closepath\n")
    file.write("%f %f %f setrgbcolor\n"%(rgb[0],rgb[1],rgb[2]))
    file.write("%f setlinewidth\n"%(linewidth) );
    file.write("stroke\n")
###############################################################################
def f_write_text(file,text,size,position,color):
    
    file.write("/Times-Roman findfont\n")
    file.write("%g scalefont\n"%(size))
    file.write("setfont\n")
    file.write("%f %f %f setrgbcolor\n"%(color[0],color[1],color[2]))
    file.write("%f %f translate\n"%(m_to_point(position[0]),m_to_point(position[1])))
    file.write("0 0 moveto\n")
    file.write("("+text+") show\n")
    file.write("%f %f translate\n"%(m_to_point(-position[0]),m_to_point(-position[1])))
    
###############################################################################
def f_write_text_rotated(file, text,size,position,color,angle,font):
    newposition=c.rotatepoint(position,-angle,[0,0])
    file.write("%f %f %f setrgbcolor\n"%(color[0],color[1],color[2]))
    file.write("/"+font+" "+str(size)+" selectfont\n")
    file.write("%g rotate\n"%(angle))
    file.write("newpath\n")
    file.write("%f %f moveto\n"%(m_to_point(newposition[0]),m_to_point(newposition[1])))
    file.write("("+text+")show\n")
    file.write("%g rotate\n"%(-angle))
###############################################################################    
def m_to_point(meter):
    return meter*72/25.4e-3
###############################################################################
def draw(filename,polygon,color):
    if hasattr(polygon, 'geom_type'):
        if polygon.geom_type=='MultiPolygon':
            for part in polygon:
                file=open(filename,"a")
                f_ps_make_polygon_filled(file,c.drawable(part) ,color)
                file.close()
        elif polygon.geom_type=='Polygon':
            file=open(filename,"a")
            f_ps_make_polygon_filled(file,c.drawable(polygon) ,color)
            file.close()
        elif polygon.geom_type=='LineString':
            file=open(filename,"a")
            f_ps_make_polygon_open(file,c.drawable(polygon),0.4 ,color)
            file.close()
        elif polygon.geom_type=='MultiPoint':
            file=open(filename,"a")
            for point in polygon:
                dot=sf.makerectangle(0.2e-3,0.2e-3,np.reshape(np.array(point.coords.xy),-1))
                f_ps_make_polygon_filled(file,dot ,color)
            file.close()
    else:
        if len(polygon)>2: 
            file=open(filename,"a")
            f_ps_make_polygon_filled(file,polygon,color)
            file.close()  
        else:
            file=open(filename,"a")
            f_ps_make_polygon_open(file,polygon,0.4 ,color)
            file.close()
###############################################################################
def write(filename,text,position,color):
    fh=open(filename,'a')
    f_write_text(fh,text,5,position,color)
    fh.close()
 ###############################################################################
def write_infobox(pg):
    color=dict()
    color['n']=pg.color_n
    color['p']=pg.color_p
    color['c']=pg.color_c
    fh1 = open( pg.sheetnames[0], 'a' )
    fh2 = open( pg.sheetnames[1], 'a' )
    fh3 = open( pg.sheetnames[2], 'a' )
    fh4 = open( pg.sheetnames[3], 'a' )
#    fh5 = open( pg.sheetnames['p'], 'a' )
#    fh6 = open( pg.sheetnames['pc'], 'a' )
    text_u1=pg.filename+" Cyl1"
    text_u2=pg.filename+" Cyl2"
    text_u3=pg.filename+" Cyl3"
    pos_u1=np.array([pg.info_box_x_u1,pg.info_box_y_u1])
    pos_u2=np.array([pg.info_box_x_u2,pg.info_box_y_u2])
    pos_u3=np.array([pg.info_box_x_u3,pg.info_box_y_u3])
    pos_u3_shifted=np.array([pg.info_box_x_u3,pg.info_box_y_u3-pg.second_screen_joint])
    f_write_text_rotated(fh1, text_u1,pg.info_box_fontsize,pos_u1,color[pg.material_unit1],90,pg.info_box_font)    
    f_write_text_rotated(fh1, text_u2,pg.info_box_fontsize,pos_u2,color[pg.material_unit2],90,pg.info_box_font)
    f_write_text_rotated(fh1, text_u3,pg.info_box_fontsize,pos_u3,color[pg.material_unit3],90,pg.info_box_font)
    f_write_text_rotated(fh2, text_u1,pg.info_box_fontsize,pos_u1,pg.color_design,90,pg.info_box_font)    
    f_write_text_rotated(fh3, text_u2,pg.info_box_fontsize,pos_u2,pg.color_design,90,pg.info_box_font)
    f_write_text_rotated(fh4, text_u3,pg.info_box_fontsize,pos_u3_shifted,pg.color_design,90,pg.info_box_font) 
    fh1.close()
    fh2.close()
    fh3.close()
    fh4.close()
#    fh5.close()
#    fh6.close()
###############################################################################
def draw_markers(pg):
    if pg.setmarkers:
#    materials=[pg.material_unit1,pg.material_unit2,pg.material_unit3]            
#    ci=materials.index('c')+1
#    ni=materials.index('n')+1
#    pi=materials.index('p')+1
#    coni=materials.index(pg.connectormaterial)+1
        color=dict()
        color['n']=pg.color_n
        color['p']=pg.color_p
        color['c']=pg.color_c
        fhs=list()
        for sheet in pg.sheetnames:
            fhs.append(open(sheet,'a'))
        for marker in pg.marker.firstpress.values():
            if not(sp.Polygon(marker).intersects(sp.Polygon(pg.contactfield.main))):
                f_ps_make_polygon_filled_shrinkfactor(fhs[0],np.copy(marker),color[pg.material_unit1],pg.shrinkfactor_unit3,pg.sheet_full_x)
                f_ps_make_polygon_filled_shrinkfactor(fhs[1],np.copy(marker),pg.color_design,pg.shrinkfactor_unit1,pg.sheet_full_x)
#            f_ps_make_polygon_filled_shrinkfactor(fh4,marker,pg.color_design,pg.shrinkfactor_press1,pg.sheet_full_x)
#            f_ps_make_polygon_filled_shrinkfactor(fh6,marker,pg.color_design,pg.shrinkfactor_press1,pg.sheet_full_x)
        for marker in pg.marker.secondpress.values():
            f_ps_make_polygon_filled_shrinkfactor(fhs[0],np.copy(marker),color[pg.material_unit2],pg.shrinkfactor_unit3,pg.sheet_full_x)
            f_ps_make_polygon_filled_shrinkfactor(fhs[2],np.copy(marker),pg.color_design,pg.shrinkfactor_unit2,pg.sheet_full_x)
        for marker in pg.marker.thirdpress.values():
            if c.get_midpoint(c.translate(np.copy(marker),[0,-pg.second_screen_joint]))[1]<0:
                shift=pg.sheet_full_y
            else:
                shift=0
            f_ps_make_polygon_filled_shrinkfactor(fhs[0],np.copy(marker),color[pg.material_unit3],pg.shrinkfactor_unit3,pg.sheet_full_x)
            f_ps_make_polygon_filled_shrinkfactor(fhs[3],c.translate(np.copy(marker),[0,-pg.second_screen_joint+shift]),pg.color_design,pg.shrinkfactor_unit3,pg.sheet_full_x)
        for fh in fhs:
            fh.close()
def draw_numbermarkers(pg):
    fh=list()
    font=pg.info_box_font
    for sheet in pg.sheetnames:
        fh.append(open(sheet,"a"))
    generatorparts=list()
    generatorparts.append(sp.Polygon(pg.contactfield.main))
    generatorparts.append(sp.Polygon(pg.input_connector))
    for element in pg.elementlist:
        generatorparts.append(sp.Polygon(element.outline))
        generatorparts.append(sp.Polygon(element.exitconnector))
    generator=so.cascaded_union(generatorparts)
    for i,element in enumerate(pg.navigationlist):
        hanger=(element.fieldcorners[0])+np.array([-0.001,0])
        freespace=sf.makerectangle(0.005,0.002,hanger+np.array([0.0025,0.001]))
#        draw(filename[0],freespace,[1,0,0])
        box=sp.Polygon(freespace)
#        bar=sf.makerectangle(pg.cell_x/10,pg.foldmarker_width,(element.fieldcorners[2]+element.fieldcorners[3])/2)
        if not(generator.intersects(sp.Polygon(box)) or not(sp.Polygon(pg.sheet).contains(sp.Polygon(box)))) :
#            filename[0].write("-12 1 scale\n")
#            f_write_text(filename[0],str(i+1),9,[-hanger[0],hanger[1]],pg.color_c)
#            f_write_text(filename[1],str(i+1),9,[-hanger[0],hanger[1]],pg.color_design)
#            filename[0].write("-1 1 scale\n")
#            filename[0].write("%f %f translate\n"%(m_to_point(pg.sheet_full_x),m_to_point(0)))
            
            f_write_text(fh[0],str(i+1),5,hanger,pg.color_c)
            f_write_text(fh[1],str(i+1),5,hanger,pg.color_design)
    for file in fh:
        file.close()
        #not(generator.intersects(sp.Polygon(bar))) or not(sp.Polygon(pg.sheet).intersects(sp.Polygon(bar))):
#            f0=open(filename[0],"a")
#            f1=open(filename[1],"a")
#            f_ps_make_polygon_filled_shrinkfactor(f0,bar,pg.color_c,pg.shrinkfactor_unit1,pg.sheet_full_x)
#            f_ps_make_polygon_filled_shrinkfactor(f1,bar,pg.color_design,pg.shrinkfactor_unit1,pg.sheet_full_x)
#            f0.close()
#            f1.close()
#    if __name__==__main__
#a=newblanksheets("test",1,1)
#polygon=np.array([[0,0],[1,0],[1,1]])
#rgb=np.array([0,1,1])
#file=open(a["c"],"a")
#f_ps_make_polygon_open(file,polygon,rgb)
#file.close()
def make_simulationscreen(im):
    im.sheetfile="output_files/"+im.filename+"/"+im.filename+"_uncutlayout.eps"
#    os.remove(im.sheetfile)
    file=open(im.sheetfile,"w")
    file.write('%%!PS-Adobe-3.0 EPSF-3.0\n%%%%BoundingBox: 0 0 %i %i \n%%%%LanguageLevel: 2\n%%%%Pages: 1\n%%Orientation: Portrait\n'%(m_to_point(im.sheet_full_x),m_to_point(im.sheet_full_y)))
    file.close()
    return im
    

def plot(polygon,color,al):
    if hasattr(polygon, 'geom_type'):
        if polygon.geom_type=='MultiPolygon':
            for part in polygon:
                x,y=part.exterior.xy
                plt.fill(x,y,c=color,alpha=al)
        elif polygon.geom_type=='Polygon':
            x,y=polygon.exterior.xy
            plt.fill(x,y,c=color,alpha=al)
        elif polygon.geom_type=='LineString':
            x,y=polygon.exterior.xy
            plt.fill(x,y,c=color,alpha=al)
        elif polygon.geom_type=='MultiPoint':
            for point in polygon:
                dot=sf.makerectangle(0.2e-3,0.2e-3,np.reshape(np.array(point.coords.xy),-1))
                plt.fill(dot[0],dot[1],c=color,alpha=al)
    else:
        plt.fill(polygon[0],polygon[1],c=color,alpha=al)  
     
    