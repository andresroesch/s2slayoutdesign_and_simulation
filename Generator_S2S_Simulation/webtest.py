# -*- coding: utf-8 -*-
"""
Created on Tue Mar 13 10:10:10 2018

@author: andres
"""
import sys
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtWebEngineWidgets import *
from PyQt5.QtWebEngineCore import *
from PyQt5.QtWebEngine import *
app =QCoreApplication(sys.argv)
app.setAttribute(Qt.AA_EnableHighDpiScaling)
app.setAttribute(Qt.AA_UseHighDpiPixmaps)
settings=QWebEngineSettings()
settings.defaultSettings().setAttribute(QWebEngineSettings.PluginsEnabled,True)
web=QWebEngineView()
url=QUrl("https://www.google.de")
web.load(url)
web.show()
app.exec()