# -*- coding: utf-8 -*-
"""
Created on Mon Mar 12 00:47:56 2018

@author: andre
"""

import sys

from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtWebKitWidgets import QWebView 
from PyQt5.QtWebKit import QWebSettings

app = QApplication(sys.argv)
web=QWebView()
web.settings().setAttribute(QWebSettings.PluginsEnabled,True)
web.load(QUrl("https://www.otego.de"))
web.show()
sys.exit(app.exec_())