# -*- coding: utf-8 -*-
"""
Created on Mon Jun 11 12:08:56 2018

@author: andre
"""
import sys

from numba import njit, prange
import matplotlib.pyplot as plt
import numpy.linalg as lg
import scipy.linalg as slg
from scipy.sparse import csr_matrix
from scipy.sparse.linalg import spsolve
from imp import reload 
import pswriter_s2s as ps 
import globalclasses as gc
import numpy as np
import numpy.matlib as npm
import simulationareas as sa
import custommathfunctions as c
import shapely.geometry as sp
import shapely.ops as so
import matplotlib
import disp as d
import shapefunctions_s2s as sf
import subprocess
reload(sa)
reload(sf)
reload(d)
reload(gc)
reload(ps)
def simulate_generator(output):
    pg=output.pg
    te=output.te
    
    s=output.s
    s.THatTEG=s.source.temperature-output.deltaTatTEG#*(1-output.ratio_for_eff_dT)
    s.TCatTEG=s.sink.temperature+output.deltaTatTEG#*(1-output.ratio_for_eff_dT)
    im=create_image(pg)
    im=ps.make_simulationscreen(im)
    t1=d.Disp1("Meshing")
    p_areas=list()
    n_areas=list()
    for element in im.elementlist:
        for strip in element.stripes:
            if sp.Polygon(strip).is_valid:
                exec(element.type+"_areas.append(sp.Polygon(strip))")
            else:
                exec(element.type+"_areas.append(sp.Polygon(strip).buffer(0))")
        if sp.Polygon(element.exitconnector).is_valid:
            exec(element.exitconnector_mat+"_areas.append(sp.Polygon(element.exitconnector))")
        else:
            exec(element.exitconnector_mat+"_areas.append(sp.Polygon(element.exitconnector).buffer(0))")
    exec(pg.connectormaterial+"_areas.append(sp.Polygon(im.input_connector).buffer(0))")
    if pg.include_contactfield:
        exec(pg.connectormaterial+"_areas.append(sp.Polygon(im.contactfield.main).buffer(0))") 
    p_area=so.unary_union(p_areas)
    n_area=so.unary_union(n_areas)
    overlap=p_area.intersection(n_area)
    pure_p=p_area.difference(overlap)
    pure_n=n_area.difference(overlap)
    generator=so.unary_union([p_area,n_area])
    pa=c.drawable(generator.convex_hull)
    pa=np.array([[min(pa[:,0]),min(pa[:,1])],[max(pa[:,0]),min(pa[:,1])],[max(pa[:,0]),max(pa[:,1])],[min(pa[:,0]),max(pa[:,1])]])
    printed_area=c.polyable(pa)
    plt.figure(dpi=600)
    # ps.plot(printed_area,'grey')
    ps.plot(p_area,'r',0.5)
    ps.plot(n_area,'g',0.5)
    # ps.plot(overlap,'y',0.5)
    # ps.plot(p_area[16].intersection(n_area[18]),'y',1)
    # ps.plot(p_area[16].difference(n_area[18]),'c',0.5)
    # plt.savefig('f.eps',format='eps', dpi=1200)
    plt.show()
    plt.figure(dpi=600)
    # ps.plot(printed_area,'grey')
    ps.plot(overlap,'y',1)
    ps.plot(pure_p,'r',1)
    ps.plot(pure_n,'g',1)
   
    # ps.plot(p_area[16].intersection(n_area[18]),'y',1)
    # ps.plot(p_area[16].difference(n_area[18]),'c',0.5)
    # plt.savefig('f.eps',format='eps', dpi=1200)
    plt.show()
   
   
    output.p_area=p_area.area
    output.n_area=n_area.area
    output.pritned_area=printed_area.area
    output.generator_area=generator.area
    output.printed_height=max(pa[:,1])-min(pa[:,1])
    output.printed_width=max(pa[:,0])-min(pa[:,0])
    sl=max(output.printed_height,output.printed_width)
    initialsquare=c.polyable(np.array([[0,0],[0,sl],[sl,sl],[0,sl]])+pa[0])
        # output=sa.calculating_degree_of_filling(pg,te,output)
    # output=sa.calculating_material_usage(pg,te,s,output)
#    infield=sp.Polygon(im.infield)
#    outfield=sp.Polygon(im.outfield)
    # ps.draw(im.sheetfile,p_area,pg.color_p)
    # ps.draw(im.sheetfile,n_area,pg.color_n)
#    ps.draw(im.sheetfile,overlap,[1,1,0])         
#    ps.draw(im.sheetfile,im.infield,[1,0,1])
#    ps.draw(im.sheetfile,im.outfield,[0,1,0])  
    y_number=s.precision
    y_coordinates=np.linspace(0,im.limits[1],y_number+1)
    sidelength=y_coordinates[1]
    x_coordinates=np.arange(0,im.limits[0]+sidelength/2,sidelength)
    x_coordinates=x_coordinates[:-1]+sidelength/2
    y_coordinates=y_coordinates[:-1]+sidelength/2
    coords=np.zeros([len(x_coordinates)*len(y_coordinates),2])
    coords=meshing(coords,x_coordinates,y_coordinates)
#    d.Disp2(t1)
##    ps.draw(im.sheetfile,sp.MultiPoint(coords),[1,0,0])
################################################################################
    knot_coords,knot_indices=find_knots_in_shape(coords,generator,im.limits[1],y_number,sidelength)
    knotmatrix=np.zeros([len(y_coordinates),len(x_coordinates)],dtype=int)
    knotmatrix=init_knotmatrix(knotmatrix,knot_indices)
    knotmatrix=np.flipud(knotmatrix)
    inp=np.zeros(knot_coords.shape[0], dtype=bool)
    inn=np.zeros(knot_coords.shape[0], dtype=bool)
    for element in im.elementlist:
        for strip in element.stripes:
#            exec(element.type+"_areaz.append(strip)")
            exec("in"+element.type+"+= matplotlib.path.Path(strip).contains_points(knot_coords)")
#        exec(element.exitconnector_mat+"_areaz.append(element.exitconnector)")
        exec("in"+element.exitconnector_mat+"+= matplotlib.path.Path(element.exitconnector).contains_points(knot_coords)")
#    exec(pg.connectormaterial+"_areaz.append(im.input_connector)")
    exec("in"+pg.connectormaterial+"+= matplotlib.path.Path(im.input_connector).contains_points(knot_coords)")
    entranceknot=matplotlib.path.Path(im.infield).contains_points(knot_coords)
    exitknot=matplotlib.path.Path(im.outfield).contains_points(knot_coords)    
    d.Disp2(t1)
#    t1=d.Disp1("Connecting")
#    directions=np.array([[0,1],[0,-1],[-1,0],[1,0]]) #directions encrypted in the position of the neighbor!
#    knot_neighbors=np.array([knot_indices[i]+directions for i in range(len(knot_indices))])
#    knot_neighbor_ingenerator=in_g(knot_indices,np.zeros([len(knot_indices),4]),knot_neighbors)
#    d.Disp2(t1)   
    output.knot_coords=knot_coords
    output=set_temperatureprofile(output,im,te,s)     
    t1=d.Disp1("Setting up KPPV")
    IforR=np.zeros(len(knot_indices)+1)    
    IforR[0]=1
    IforU=np.zeros(len(knot_indices)+1)    
#    G=np.zeros([len(knot_indices)+2,len(knot_indices)+2])
    
#    diaginal_G=np.zeros([len(knot_indices)+2])
    diag_G=np.zeros([len(knot_indices)+2])
    G_indices=np.zeros([len(knot_indices)+2,4],dtype=int)
    G_values=np.zeros([len(knot_indices)+2,4])
#    G[0][0]=1/s.contact_R*sum(entranceknot)
#    G[-1][-1]=1/s.contact_R*sum(exitknot)
    counter=0
    # Gp=s.p_thickness*s.p_el_conductivity
    # Gn=s.n_thickness*s.n_el_conductivity
    # Gc=1/s.contact_R
    # Sp=s.p_seebeck
    # Sn=s.n_seebeck
    # IforU,diag_G,G_indices,G_values=setting_up_kppv(IforU,output.T,diag_G,G_indices,G_values,Gn,Gp,Gc,Sp,Sn,inp,inn,exitknot,entranceknot,knotmatrix,np.zeros(2,dtype=int))
    G_row=list()
    G_column=list()
    G_value=list()
    for i in range(1,len(G_indices)):
        for x in range(2):
            if G_indices[i][x]:
                G_row.append(i)
                G_column.append(G_indices[i][x])
                G_value.append(G_values[i][x])
                G_row.append(G_indices[i][x])
                G_column.append(i)
                G_value.append(G_values[i][x])
        if G_values[i][2]:
            G_row.append(i)
            G_column.append(0)
            G_value.append(G_values[i][2])
            G_row.append(0)
            G_column.append(i)
            G_value.append(G_values[i][2])
        if G_values[i][3]:
            G_row.append(i)
            G_column.append(G_indices[i][3])
            G_value.append(G_values[i][3])
            G_row.append(G_indices[i][3])
            G_column.append(i)
            G_value.append(G_values[i][3])
#    x=[G[i,i] for i in range(G.shape[0])]
    sparse_G=csr_matrix((np.array(G_value),(np.array(G_row,dtype=int),np.array(G_column,dtype=int))))
    sparse_G=sparse_G.tolil()
    sparse_G.setdiag(diag_G)
    sparse_G=sparse_G[:-1,:-1]
    sparse_G=sparse_G.tocsr()
#    SG=sparse_G.todense()
    d.Disp2(t1)
    t1=d.Disp1("Solving KPPV for R")
#    UforR=lg.solve(G,IforR)
    UforR=spsolve(sparse_G,IforR)
    d.Disp2(t1)
    t1=d.Disp1("Solving KPPV for U")
    UforU=spsolve(sparse_G,IforU)
    d.Disp2(t1)
#    counter=0
#    output.Iy=np.zeros(len(output.knot_indices), dtype=complex)
#    for i in range(len(output.knot_indices)):
#        sys.stdout.write('\r'+"{:.2f}".format(counter*100/(len(output.knot_indices)))+" %\t")
#        counter=counter+1
#        j=i+1
#        for m,n in enumerate(output.knot_neighbors[i]):
#            ni=n+1
#            output.Iy[i]=output.Iy[i]+knot_neighbor_direction[i][m]*(output.U[j]-output.U[ni])*-output.G[j][ni]   
#    output.Id=np.angle(output.Iy,deg=True)    
#    output.voltage_per_kelvin=UforU[0]/output.deltaTatTEG
    t1=d.Disp1("Painting")
    cmap = matplotlib.cm.get_cmap('Spectral')
    Umax=max(UforR)
    Tmax=max(output.T)
    norm=matplotlib.colors.Normalize(vmin=min(output.T),vmax=max(output.T))    
    counter=0
    for i,index in enumerate(knot_indices):
        
        counter=counter+1
#        arrow=makearrow(sidelength,output.knot_coords[i],output.Id[i])
        square=sf.makerectangle(sidelength,sidelength,output.knot_coords[i])
        if exitknot[i]:
            color=[1,0,0]
            ps.draw(im.sheetfile,square,color)
        elif entranceknot[i]:
            color=[1,0,1]
            ps.draw(im.sheetfile,square,color)
        elif inn[i] and inp[i]:
            color=[1,1,0]
            ps.draw(im.sheetfile,square,color)
        elif inn[i]:
            color=[0,1,0]
            ps.draw(im.sheetfile,square,color)
        elif inp[i]:
            color=[0,0,0]
            ps.draw(im.sheetfile,square,color)
        else:
            color=[0.8,0.8,1]
#        ps.write(im.sheetfile,str(i),knot_coords[i],[0,0,0])
#        ps.draw(im.sheetfile,square,color)    
#        ps.draw(im.sheetfile,square,cmap(norm(output.T[i]))[:-1])    
        ps.draw(im.sheetfile,square,cmap(UforR[i+1]/Umax)[:-1])
#        ps.draw(im.sheetfile,arrow,[1,1,1])
#    for i,y in enumerate(output.temperatures_y):
#        line=sf.makerectangle(im.limits[0],0.1e-3,[im.limits[0]/2,y])
#        ps.draw(im.sheetfile,line,cmap(norm(output.temperatures_T[i]))[:-1])
    d.Disp2(t1)    
#    print(U[0])
#    print(output.U[0])
    output.electrical_resistance=UforR[0]/IforR[0]
    output.openvoltage=UforU[0]
    output.im=im
#    subprocess.Popen("epstopdf "+im.sheetfile+" --outfile "+im.sheetfile.replace("eps","pdf"),shell=True)
    return output      
###############################################################################
@njit(parallel=True)
def setting_up_kppv(I,T,diag_G,G_indices,G_values,Gn,Gp,Gc,Sp,Sn,inp,inn,exitknot,entranceknot,knotmatrix,neighbors):
    for i in prange(knotmatrix.shape[0]):
        for j in prange(knotmatrix.shape[1]):
            k=knotmatrix[i,j]
            if k:
                if entranceknot[k-1]:
                    G_indices[k,2]=1
                    G_values[k,2]=-Gc
#                    G[0,k]=-Gc
#                    G[k,0]=-Gc
                    diag_G[k]-=G_values[k,2]
                    diag_G[0]-=G_values[k,2]
                if exitknot[k-1]:
                    G_indices[k,3]=G_indices.shape[0]-1
                    G_values[k,3]=-Gc
#                    G[-1,k]=-Gc
#                    G[k,-1]=-Gc
                    diag_G[k]-=G_values[k,3]
                    diag_G[-1]-=G_values[k,3]
                if i<knotmatrix.shape[0]-1 and j<knotmatrix.shape[1]-1:
                    for m in prange(2):
                        n=knotmatrix[i+np.mod(m,2),j+np.mod(m+1,2)]
                        if n:
                            G_indices[k,m]=n
                            if inp[k-1] and inp[n-1] and inn[k-1] and inn[n-1]:
                                G_values[k,m]=-Gp-Gn
#                                G[k,n]=-Gp-Gn
                                diag_G[k]-=G_values[k,m]
                                diag_G[n]-=G_values[k,m]
                                S=0
                            elif (inn[k-1] and inn[n-1] and inp[n-1]) or (inn[k-1] and inp[k-1] and inn[n-1]):
                                G_values[k,m]=(-2*Gn-2*Gp)*(-2*Gn)/((-2*Gn-2*Gp)+(-2*Gn))
#                                G[k,n]=(-2*Gn-2*Gp)*(-2*Gn)/((-2*Gn-2*Gp)+(-2*Gn))
                                diag_G[k]-=G_values[k,m]
                                diag_G[n]-=G_values[k,m]
                                S=Sp/2
                            elif (inp[k-1] and inp[n-1] and inn[n-1]) or (inp[k-1] and inn[k-1] and inp[n-1]):
                                G_values[k,m]=(-2*Gn-2*Gp)*(-2*Gp)/((-2*Gn-2*Gp)+(-2*Gp))
#                                G[k,n]=(-2*Gn-2*Gp)*(-2*Gp)/((-2*Gn-2*Gp)+(-2*Gp))
                                diag_G[k]-=G_values[k,m]
                                diag_G[n]-=G_values[k,m]
                                S=Sp/2
                            elif (inn[k-1] and inp[n-1]) or (inp[k-1] and inn[n-1]):    
                                G_values[k,m]=(-3*Gn-3*Gp)*(-3*Gn)/((-3*Gn-3*Gp)+(-3*Gn))*-3*Gp/((-3*Gn-3*Gp)*(-3*Gn)/((-3*Gn-3*Gp)+(-3*Gn))+(-3*Gp))
#                                G[k,n]=(-3*Gn-3*Gp)*(-3*Gn)/((-3*Gn-3*Gp)+(-3*Gn))*-3*Gp/((-3*Gn-3*Gp)*(-3*Gn)/((-3*Gn-3*Gp)+(-3*Gn))+(-3*Gp))
                                diag_G[k]-=G_values[k,m]
                                diag_G[n]-=G_values[k,m]
                                S=0            
                            elif inn[k-1] and inn[n-1]:
                                G_values[k,m]=-Gn
#                                G[k,n]=-Gn
                                diag_G[k]-=G_values[k,m]
                                diag_G[n]-=G_values[k,m]
                                S=Sn
                            elif inp[k-1] and inp[n-1]:
                                G_values[k,m]=-Gp
#                                G[k,n]=-Gp
                                diag_G[k]-=G_values[k,m]
                                diag_G[n]-=G_values[k,m]
                                S=Sp
#                            G[n,k]=G[k,n]
                            I[k]=I[k]+(T[n-1]-T[k-1])*S*-G_values[k,m]
#    for k in prange(G.shape[0]):
#        for l in prange(G.shape[0]):
#            if l!=k:
#                G[k,k]+=-G[k,l]
#                diagonal_G[k]+=-G[k,l]                        
#    G=G[:-1,:-1]
    return I,diag_G,G_indices,G_values
###############################################################################
def create_image(pg):
    t1=d.Disp1("Creating Image")
    im=gc.voidclass()       # creates an image of the generator
    if pg.angle==0:
        im=pg
#        im.sheetfile="output_files/"+im.filename+"/"+im.filename+"_uncutlayout.eps"
        try:
            im.outfield=pg.contactfield.into
        except:
            im.outfield=pg.navigationlist[-1].exit
        try:
            im.infield=pg.contactfield.out
        except:
            im.infield=pg.navigationlist[0].entrance
        im.offset=np.array([0,0])
        im.limits=np.array([pg.sheet_full_x,pg.sheet_full_y])
    else:
        im.filename=pg.filename
        offset,limits=get_range(pg)
        im.sheet_full_x=limits[0]
        im.sheet_full_y=limits[1]
        im.elementlist=list()
        im.infield=c.translate(pg.contactfield.out,[0,pg.sheet_full_y])
        im.infield=c.rotate(im.infield,-pg.angle,[0,0])
        im.infield=c.translate(im.infield,offset)
        im.outfield=c.rotate(pg.contactfield.into,-pg.angle,[0,0])
        im.outfield=c.translate(im.outfield,offset)
        im.input_connector=c.translate(pg.input_connector,[0,pg.sheet_full_y])
        im.input_connector=c.rotate(im.input_connector,-pg.angle,[0,0])
        im.input_connector=c.translate(im.input_connector,offset)
        im.contactfield=gc.voidclass()
        im.contactfield.main=c.translate(pg.contactfield.main,[0,pg.sheet_full_y])
        im.contactfield.main=c.rotate(im.contactfield.main,-pg.angle,[0,0])
        im.contactfield.main=c.translate(im.contactfield.main,offset)
        counter=1
        for element in pg.elementlist:
            sys.stdout.write('\r'+"{:.2f}".format(counter*100/(len(pg.elementlist)))+" %\t")
            counter=counter+1
            im_element=element
            if element.index[0]<pg.cutlinerow:
                shift=np.array([0,pg.sheet_full_y])
            else:
                shift=np.array([0,0])
            im_element.outline=image_polygon(element.outline,pg.angle,offset,shift)
            im_element.exitconnector=image_polygon(element.exitconnector,pg.angle,offset,shift)
            im_element.active_area=image_polygon(element.active_area,pg.angle,offset,shift)
            im_element.entrance=image_polygon(element.entrance,pg.angle,offset,shift)
            im_element.exit=image_polygon(element.exit,pg.angle,offset,shift)
            im_element.fieldcorners=image_polygon(element.fieldcorners,pg.angle,offset,shift)
            im_element.bottom=image_polygon(element.bottom,pg.angle,offset,shift)
            im_element.top=image_polygon(element.top,pg.angle,offset,shift)
            im_element.overlaparea_bottom=image_polygon(element.overlaparea_bottom,pg.angle,offset,shift)
            im_element.overlaparea_top=image_polygon(element.overlaparea_top,pg.angle,offset,shift)
            im_element.midpoint=image_point(element.midpoint,pg.angle,offset,shift)
            for i in range(len(im_element.stripes)):
                im_element.stripes[i]=image_polygon(im_element.stripes[i],pg.angle,offset,shift)
                im_element.contactfield_bottom[i]=image_polygon(im_element.contactfield_bottom[i],pg.angle,offset,shift)
                im_element.contactfield_top[i]=image_polygon(im_element.contactfield_top[i],pg.angle,offset,shift)
            im.elementlist.append(im_element)
        im.connectormaterial=pg.connectormaterial
        im.printingarea=pg.printingarea
        im.substrate=pg.substrate
        im.include_contactfield=pg.include_contactfield
#            ps.draw(im.sheetfile,im_element.outline,[0,0,0])
#            ps.draw(im.sheetfile,im_element.exitconnector,[1,0,0])    
#        inputconnector=c.translate(pg.input_connector,[0,pg.sheet_full_y])
#        inputconnector=c.rotate(inputconnector,-pg.angle,[0,0])
#        inputconnector=c.translate(inputconnector,offset)
#        ps.draw(im.sheetfile,im.input_connector,[1,0,0])
#        ps.draw(im.sheetfile,im.infield,[1,0,1])
#        ps.draw(im.sheetfile,im.outfield,[0,1,0])
#        for marker in pg.marker.firstpress.values():
#            m=c.rotate(marker,-pg.angle,[0,0])
#            ps.draw(im.sheetfile,m,[0,0,1])
        im.offset=offset
        im.limits=limits
    d.Disp2(t1)    
    return im
###############################################################################
def get_range(pg):
    min_x_value=10000000
    min_y_value=10000000
    max_x_value=0
    max_y_value=0
    inputconnector=c.translate(pg.input_connector,[0,pg.sheet_full_y])
    inputconnector=c.rotate(inputconnector,-pg.angle,[0,0])
    infield=c.translate(pg.contactfield.out,[0,pg.sheet_full_y])
    infield=c.rotate(infield,-pg.angle,[0,0])
#    outfield=c.translate(pg.contactfield.into,[0,pg.sheet_full_y])
    outfield=c.rotate(pg.contactfield.into,-pg.angle,[0,0])
    for element in pg.elementlist:
        if element.index[0]<pg.cutlinerow:
            frame=c.translate(element.outline,[0,pg.sheet_full_y])
            connector=c.translate(element.exitconnector,[0,pg.sheet_full_y])
        else:
            frame=element.outline
            connector=element.exitconnector
        frame=c.rotate(frame,-pg.angle,[0,0])
        connector=c.rotate(connector,-pg.angle,[0,0])
        min_x_value=min(min_x_value,min(frame[:,0]))
        min_y_value=min(min_y_value,min(frame[:,1]))
        min_x_value=min(min_x_value,min(connector[:,0]))
        min_y_value=min(min_y_value,min(connector[:,1]))
        max_x_value=max(max_x_value,max(frame[:,0]))
        max_y_value=max(max_y_value,max(frame[:,1]))
        max_x_value=max(max_x_value,max(connector[:,0]))
        max_y_value=max(max_y_value,max(connector[:,1]))
    min_x_value=min(min_x_value,min(inputconnector[:,0]))
    min_y_value=min(min_y_value,min(inputconnector[:,1]))
    max_x_value=max(max_x_value,max(inputconnector[:,0]))
    max_y_value=max(max_y_value,max(inputconnector[:,1]))
    min_x_value=min(min_x_value,min(infield[:,0]))
    min_y_value=min(min_y_value,min(infield[:,1]))
    max_x_value=max(max_x_value,max(infield[:,0]))
    max_y_value=max(max_y_value,max(infield[:,1]))
    min_x_value=min(min_x_value,min(outfield[:,0]))
    min_y_value=min(min_y_value,min(outfield[:,1]))
    max_x_value=max(max_x_value,max(outfield[:,0]))
    max_y_value=max(max_y_value,max(outfield[:,1]))
#    bullethole=np.array([(pg.printinglimits.minimum[0]+pg.printinglimits.maximum[0])/2,pg.sheet_full_y/2])
#    distance=[np.linalg.norm(bullethole-field.midpoint) for field in pg.fieldlist]
#    anker=pg.fieldlist[distance.index(min(distance))]
#    p1=anker.corners[0]
#    p2=anker.corners[1]
#    A = np.vstack([np.array([p1[0],p2[0]]), np.ones(len(np.array([p1[0],p2[0]])))]).T
#    m,c0=np.linalg.lstsq(A,np.array([p1[1],p2[1]]),rcond=None)[0]
    offset=-np.array([min_x_value,min_y_value])
    limits=np.array([max_x_value,max_y_value])+offset
    return offset,limits
###############################################################################
def image_polygon(polygon,angle,offset,shift):
    im_polygon=c.translate(polygon,shift)
    im_polygon=c.rotate(im_polygon,-angle,[0,0])
    im_polygon=c.translate(im_polygon,offset)
    return im_polygon
###############################################################################
def image_point(point,angle,offset,shift):
    im_point=c.translate(point,shift)
    im_point=c.rotatepoint(im_point,-angle,[0,0])
    im_point=c.translate(im_point,offset)
    return im_point
###############################################################################
def set_temperatureprofile(output,im,te,s):
    t1=d.Disp1("Setting temperatures")
    output.temperatures_y=list()
    output.temperatures_y.append(im.elementlist[0].fieldcorners[0][1])
    n=1
    while im.elementlist[0].fieldcorners[0][1]+(n-1)*te.cell_y<im.limits[1]:
        output.temperatures_y.append(im.elementlist[0].fieldcorners[0][1]+n*te.cell_y)
        n=n+1
    n=1
    while im.elementlist[0].fieldcorners[0][1]-(n-1)*te.cell_y>0:
        output.temperatures_y.append(im.elementlist[0].fieldcorners[0][1]-n*te.cell_y)
        n=n+1
    output.temperatures_y.sort()
    output.temperatures_T=np.reshape(npm.repmat([s.THatTEG,s.TCatTEG],int(len(output.temperatures_y)/2),1),-1)
    if len(output.temperatures_y)!=len(output.temperatures_T):
        output.temperatures_T=np.append(output.temperatures_T,s.THatTEG)
    output.T=list()
    for coords in output.knot_coords:
        output.T.append(np.interp(coords[1],output.temperatures_y,output.temperatures_T))    
    d.Disp2(t1)
    return output
###############################################################################
def ser(G1,G2):
    return G1*G2/(G1+G2)
###############################################################################
def makearrow(sidelength,position,angle):
    triangle=np.array([[0,-sidelength/3],[sidelength/3,0],[0,sidelength/3]])    
    return c.translate(c.rotate(triangle,angle,[0,0]),position)
###############################################################################
@njit(parallel=True)
def meshing(coords,x_coordinates,y_coordinates): 
    for i in prange(len(x_coordinates)):
        for j in prange(len(y_coordinates)):
            coords[i*len(y_coordinates)+j,0]=x_coordinates[i]
            coords[i*len(y_coordinates)+j,1]=y_coordinates[j]
    return coords
###############################################################################
@njit(parallel=True)
def connecting(knot_indices,knot_neighbors,knot_neighbors_direction,directions,knot_material):
    for i in prange(knot_indices.shape[0]):
        for j in prange(len(knot_neighbors_direction[i])):
            indexinlist=0
            res=knot_indices-knot_neighbors_direction[i][j]
            for r in prange(len(res)):
                if not(np.sum(res[r])) and not(np.prod(res[r])):
                    indexinlist=i+1
            if bool(indexinlist):
                knot_neighbors[i,j]=indexinlist+1
                knot_neighbors_direction[i,j]=directions[j]
    return knot_neighbors,knot_neighbors_direction,knot_material
###############################################################################
def find_knots_in_shape(coords,shape,y_limit,y_number,sidelength):    
    kcoords=np.array(c.drawable(shape.intersection(sp.MultiPoint(coords))))
    kindices=np.int16(np.round((kcoords-sidelength/2)/y_limit*y_number))
    return kcoords,kindices
###############################################################################
#@njit(parallel=True)
#def in_g(indices,nig,neighbors):
#    for i in prange(neighbors.shape[0]):
#        for j in prange(neighbors.shape[1]):    
#            indices0=indices[:,0]
#            indices1=indices[:,1]
#            nig0=indices0==neighbors[i][j][0]
#            nig1=indices1==neighbors[i][j][1]
#            nigi=nig0*nig1
#            nig[i][j]=nigi.any()
#    return nig
###############################################################################
@njit(parallel=True)
def init_knotmatrix(knotmatrix,knot_indices):
    for i in prange(len(knot_indices)):
        knotmatrix[knot_indices[i][1],knot_indices[i][0]]=i+1
    return knotmatrix
    
#def find_index(B,value):
#    res=knot_indices-neighbors[j]
#    for i in range(len(res)):
#        if not(np.sum(res[i])) and not(np.prod(res[i])):
#            return i+1
#    return -1
#@njit(parallel=True)
#def multi_ray_tracing_method(inside,coords_x,coords_y,polygon):
#    for co in prange(len(coords_x)):
#        inside[co]=ray_tracing_method(coords_x[co],coords_y[co],polygon)
#    return inside
###############################################################################
#@njit(parallel=True)
#def ray_tracing_method(x,y,poly):
#    n = len(poly)
#    inside = False
#    p1x,p1y = poly[0]
#    for i in range(n+1):
#        p2x,p2y = poly[i % n]
#        if y > min(p1y,p2y):
#            if y <= max(p1y,p2y):
#                if x <= max(p1x,p2x):
#                    if p1y != p2y:
#                        xints = (y-p1y)*(p2x-p1x)/(p2y-p1y)+p1x
#                    if p1x == p2x or x <= xints:
#                        inside = not inside
#        p1x,p1y = p2x,p2y
#
#    return inside
#    ps.draw(im.sheetfile,p_area,pg.color_p)
#    ps.draw(im.sheetfile,n_area,pg.color_n)
#    ps.draw(im.sheetfile,overlap,[1,1,0])         
#    ps.draw(im.sheetfile,im.infield,[1,0,1])
#    ps.draw(im.sheetfile,im.outfield,[0,1,0])
#    ps.draw(im.sheetfile,sp.MultiPoint(kip_coords),[1,0,0])
#    t1=d.Disp1("Alternative Meshing")
#    y_number=s.precision
#    y_coordinates=np.linspace(0,im.limits[1],y_number+1)
#    sidelength=y_coordinates[1]
#    x_coordinates=np.arange(0,im.limits[0]+sidelength/2,sidelength)
#    x_coordinates=x_coordinates[:-1]+sidelength/2
#    y_coordinates=y_coordinates[:-1]+sidelength/2
#    coords=np.zeros([len(x_coordinates)*len(y_coordinates),2])
#    coords=meshing(coords,x_coordinates,y_coordinates)

