# -*- coding: utf-8 -*-
"""
Created on Wed Jan 17 15:22:17 2018

@author: andres
"""
from imp import reload 
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
import globalclasses as gc
import numpy as np
reload(gc)

def blocks():
    blocks=["Sheets:","Printing process:","Elements:","Markers:","Flags:","Layout:","Simulation:","TEG properties:"]
    return blocks
def pos():
    pos=[(2,0),(4,0),(3,1),(4,1),(2,1),(3,0),(2,3),(3,3)]
    return pos

def fieldnames():
    fieldnames=[
    ["Filename",
     "Full Height (Y-Axis)",
     "Full Width (X-Axis)",
     "Width of substrate",
     "Tilt angle",
     "Spacing bottom",
     "Spacing top",
     "Spacing left",
     "Spacing right"],
    
    ["Shrinkfactor",
     "Wrap rod diameter",
     "Printing direction",
     "Textbox:",
     "X Position",
     "Y Position",
     "Fontsize",
     "Font",
     "Testpads:",
     "Radius",
     "Strip length",
     "Strip aspect ratio",
     "Y Positions offset"],
     
    ["General properties:",
     "Fold depth",
     "Spacing x",
     "Spacing y",
     "n-Cell to p-Cell ratio",
     "Dimensions given",
     "p-Element:",
     "Overlap p",
     "Effective Width p",
     "Number of stripes p",
     "Gapsize p",
     "n-Element:",
     "Overlap n",
     "Effective Width n",
     "Number of stripes n",
     "Gapsize n",
     "Connectors:",
     "Connector type",
     "Connector width",
     "Connector spacing",
     "Linker type",
     "Linker height",
     "Linker length"],
    #Marker fields
    ["Marker line X Position",
     "Alignment marker:",
     "Alignment marker width",
     "Outer space",
     "Inner space",
     "Offset left",
     "Offset right",
     "Circle Marker:",
     "Outer radius",
     "Circle width",
     "Cross width",
     "Fold marker:",
     "Fold marker width",
     "Fold marker length",
     "Spacing right",
     "Spacing left",],
    
    ["Cleaning",
     "Effective Y Optimization",
     "Write indices",
     "Flip inputconnector",
     "Draw window in contactfield",
     "Draw markers",
     "Write Infobox",
     "Include contactfield",
     "Adapt n-width to connector width",
     "Jump connectors are turn connectors"],
      
     ["Type of first element",
     "Variation from foldline",
     "Extra rows of cutline",
     "Starting point",
     "Contactfield:",
     "Spacing right",
     "Spacing left",
     "Spacing to last row",
     "Window width",
     "Window length",
     "Colors(r,g,b):",
     "Color N",
     "Color P",
     "Color C",
     "Drawing area",
     "Element area",
     "Design"],
     ["Model type","P Material","N Material","C Material","jlk","jlk","jlk","jlk","jlk","jlk","jlk","jlk"],
     ["Voltage factor [V/K]","MPP power factor [W/K²]","El. resistance [\u03A9]","Thermo couples","P Material","N Material","C Material","jlk","jlk","jlk","jlk","jlk","jlk","jlk"]]
    return fieldnames

def load_values(inputdata):
    pg=inputdata["pg"]
    te=inputdata["te"]
    l=inputdata["l"]
#    s=inputdata["s"]
    values=dict()
    values["Filename"]=pg.filename
    values["Full Height (Y-Axis)"]=pg.sheet_full_y
    values["Full Width (X-Axis)"]=pg.sheet_full_x
    values["Width of substrate"]=pg.substrate_foil_full_x
    values["Tilt angle"]=pg.angle_in_degree
    values["Spacing bottom"]=pg.sheet_offset_bottom
    values["Spacing top"]=pg.sheet_offset_top
    values["Spacing left"]=pg.sheet_offset_left
    values["Spacing right"]=pg.sheet_offset_right
    values["Shrinkfactor"]=pg.shrinkfactor
    values["Wrap rod diameter"]=pg.wrap_rod_diameter
    values["Printing direction"]=pg.printdirection
    values["Textbox:"]=""
    values["X Position"]=pg.info_box_x
    values["Y Position"]=pg.info_box_y
    values["Fontsize"]=pg.info_box_fontsize
    values["Font"]=pg.info_box_font
    values["Testpads:"]=""
    values["Radius"]=pg.padradius
    values["Strip length"]=pg.pad_length
    values["Strip aspect ratio"]=pg.pad_aspect_ratio
    values["Y Positions offset"]=pg.pad_ypositions_offset
    values["General properties:"]=""
    values["Fold depth"]=te.cell_y
    values["Spacing x"]=te.spacing_x
    values["Spacing y"]=te.spacing_y
    values["n-Cell to p-Cell ratio"]=te.n_cell_x_to_p_cell_x_ratio
    values["Dimensions given"]=te.dimensionsgiven
    values["p-Element:"]=""
    values["Overlap p"]=te.p.overlap
    values["Effective Width p"]=te.p.effective_x
    values["Number of stripes p"]=te.p.num_of_stripes
    values["Gapsize p"]=te.p.gapsize
    values["n-Element:"]=""
    values["Overlap n"]=te.n.overlap
    values["Effective Width n"]=te.n.effective_x
    values["Number of stripes n"]=te.n.num_of_stripes
    values["Gapsize n"]=te.n.gapsize
    values["Connectors:"]=""
    values["Connector type"]=pg.connector_type
    values["Connector width"]=pg.connector_width
    values["Connector spacing"]=pg.connectorspacing
    values["Linker type"]=l.linker_type
    values["Linker height"]=l.linker_height
    values["Linker length"]=l.linker_length
    values["Marker line X Position"]=pg.marker_line_x
    values["Alignment marker:"]=""
    values["Alignment marker width"]=pg.marker_width
    values["Outer space"]=pg.marker_space_outer
    values["Inner space"]=pg.marker_space_inner
    values["Offset left"]=pg.sheet_marker_offset_left
    values["Offset right"]=pg.sheet_marker_offset_right
    values["Circle Marker:"]=""
    values["Outer radius"]=pg.circle_marker_outerradius
    values["Circle width"]=pg.circle_marker_linewidth
    values["Cross width"]=pg.circle_marker_crosswidth
    values["Fold marker:"]=""
    values["Fold marker width"]=pg.foldmarker_width
    values["Fold marker length"]=pg.foldmarker_length
    values["Spacing right"]=pg.foldmarker_spacing_right
    values["Spacing left"]=pg.foldmarker_spacing_left
    values["Cleaning"]=pg.cleaning
    values["Effective Y Optimization"]=pg.effective_y_optimization
    values["Write indices"]=pg.write_indicies
    values["Flip inputconnector"]=pg.flip_inputconnector
    values["Draw window in contactfield"]=pg.draw_window
    values["Draw markers"]=pg.setmarkers
    values["Write Infobox"]=pg.write_infobox
    values["Include contactfield"]=pg.include_contactfield
    values["Adapt n-width to connector width"]=pg.adapt_n_width_to_connector_width
    values["Jump connectors are turn connectors"]=pg.jump_connectors_are_turn_connectors
    values["Type of first element"]=pg.first_field_type
    values["Variation from foldline"]=pg.effective_y_optimization_y_variation_from_foldline
    values["Extra rows of cutline"]=pg.extra_rows_of_cutline
    values["Starting point"]=pg.startingpoint
    values["Contactfield:"]=""
    values["Spacing right"]=pg.contactfield_spacing_right
    values["Spacing left"]=pg.contactfield_spacing_left
    values["Spacing to last row"]=pg.space_between_last_connector_and_last_row
    values["Window width"]=pg.window_width
    values["Window length"]=pg.window_length
    values["Colors(r,g,b):"]=""
    values["Color N"]=pg.color_n
    values["Color P"]=pg.color_p
    values["Color C"]=pg.color_c
    values["Drawing area"]=pg.color_drawingarea
    values["Element area"]=pg.color_elementarea
    values["Design"]=pg.color_design
    values["P Material"]="PEDOT:Tos"
    values["N Material"]="TiS2"
    values["C Material"]="Ag"
    values["Model type"]="simple"
    values["jlk"]=1
    values["Voltage factor [V/K]"]=1
    values["MPP power factor [W/K²]"]=1
    values["El. resistance [\u03A9]"]=1
    values["Thermo couples"]=1
    return values

def convert(string):
    return np.array([np.float(num) for num in [entry for entry in string.replace("[","").replace("]","").split(" ") if entry!=""]])

def get_values(values):
    
    pg=gc.voidclass()
    te=gc.voidclass()
    te.p=gc.voidclass()
    te.n=gc.voidclass()
    l=gc.voidclass()
    s=gc.voidclass()
    ## DEFINITION FILENAME
    pg.filename=values["Filename"].text()
    ##DEFINITION PAGE
    pg.sheet_full_y=convert(values["Full Height (Y-Axis)"].text())
    pg.sheet_full_x=convert(values["Full Width (X-Axis)"].text())
    pg.substrate_foil_full_x=convert(values["Width of substrate"].text())
    pg.angle_in_degree=convert(values["Tilt angle"].text())
    pg.sheet_offset_bottom=convert(values["Spacing bottom"].text())
    pg.sheet_offset_top=convert(values["Spacing top"].text())
    pg.sheet_offset_left=convert(values["Spacing left"].text())
    if values["Spacing right"].text()=="auto":
        pg.sheet_offset_right=convert(values["Spacing right"].text())
    else:
        pg.sheet_offset_right=convert(values["Spacing right"].text())
    pg.shrinkfactor=convert(values["Shrinkfactor"].text())
    pg.wrap_rod_diameter=convert(values["Wrap rod diameter"].text())
    pg.printdirection=convert(values["Printing direction"].text())
    pg.info_box_x=convert(values["X Position"].text())
    pg.info_box_y=convert(values["Y Position"].text())
    pg.info_box_fontsize=convert(values["Fontsize"].text())
    pg.info_box_font=values["Font"].text()
    pg.padradius=convert(values["Radius"].text())
    pg.pad_length=convert(values["Strip length"].text())
    pg.pad_aspect_ratio=convert(values["Strip aspect ratio"].text())
    pg.pad_ypositions_offset=convert(values["Y Positions offset"].text())
    te.cell_y=convert(values["Fold depth"].text())
    te.spacing_x=convert(values["Spacing x"].text())
    te.spacing_y=convert(values["Spacing y"].text())
    te.n_cell_x_to_p_cell_x_ratio=convert(values["n-Cell to p-Cell ratio"].text())
    te.dimensionsgiven=convert(values["Dimensions given"].text())
    te.p.overlap=convert(values["Overlap p"].text())
    te.p.effective_x=convert(values["Effective Width p"].text())
    te.p.num_of_stripes=convert(values["Number of stripes p"].text())
    te.p.gapsize=convert(values["Gapsize p"].text())
    te.n.overlap=convert(values["Overlap n"].text())
    te.n.effective_x=convert(values["Effective Width n"].text())
    te.n.num_of_stripes=convert(values["Number of stripes n"].text())
    te.n.gapsize=convert(values["Gapsize n"].text())
    pg.connector_type=convert(values["Connector type"].text())
    pg.connector_width=convert(values["Connector width"].text())
    pg.connectorspacing=convert(values["Connector spacing"].text())
    l.linker_type=convert(values["Linker type"].text())
    l.linker_height=convert(values["Linker height"].text())
    l.linker_length=convert(values["Linker length"].text())
    pg.marker_line_x=convert(values["Marker line X Position"].text())
    pg.marker_width=convert(values["Alignment marker width"].text())
    pg.marker_space_outer=convert(values["Outer space"].text())
    pg.marker_space_inner=convert(values["Inner space"].text())
    pg.sheet_marker_offset_left=convert(values["Offset left"].text())
    pg.sheet_marker_offset_right=convert(values["Offset right"].text())
    pg.circle_marker_outerradius=convert(values["Outer radius"].text())
    pg.circle_marker_linewidth=convert(values["Circle width"].text())
    pg.circle_marker_crosswidth=convert(values["Cross width"].text())
    pg.foldmarker_width=convert(values["Fold marker width"].text())
    if values["Fold marker length"].text()=='auto':
        pg.foldmarker_length=np.array(pg.wrap_rod_diameter*np.pi)
    else:
        pg.foldmarker_length=convert(values["Fold marker length"].text())
    pg.foldmarker_spacing_right=convert(values["Spacing right"].text())
    pg.foldmarker_spacing_left=convert(values["Spacing left"].text())
    pg.cleaning=np.array(int(values["Cleaning"].isChecked()))
    pg.effective_y_optimization=np.array(int(values["Effective Y Optimization"].isChecked()))
    pg.write_indicies=np.array(int(values["Write indices"].isChecked()))
    pg.flip_inputconnector=np.array(int(values["Flip inputconnector"].isChecked()))
    pg.draw_window=np.array(int(values["Draw window in contactfield"].isChecked()))
    pg.setmarkers=np.array(int(values["Draw markers"].isChecked()))
    pg.write_infobox=np.array(int(values["Write Infobox"].isChecked()))
    pg.include_contactfield=np.array(int(values["Include contactfield"].isChecked()))
    pg.adapt_n_width_to_connector_width=np.array(int(values["Adapt n-width to connector width"].isChecked()))
    pg.jump_connectors_are_turn_connectors=np.array(int(values["Jump connectors are turn connectors"].isChecked()))
    pg.first_field_type=values["Type of first element"].text()
    pg.effective_y_optimization_y_variation_from_foldline=convert(values["Variation from foldline"].text())
    pg.extra_rows_of_cutline=convert(values["Extra rows of cutline"].text())
    pg.startingpoint=convert(values["Starting point"].text())
    pg.contactfield_spacing_right=convert(values["Spacing right"].text())
    pg.contactfield_spacing_left=convert(values["Spacing left"].text())
    pg.space_between_last_connector_and_last_row=convert(values["Spacing to last row"].text())
    pg.window_width=convert(values["Window width"].text())
    if values["Window length"].text()=='auto':
        pg.window_length=np.array(pg.wrap_rod_diameter*np.pi)
    else:
        pg.window_length=convert(values["Window length"].text())
    pg.color_n=convert(values["Color N"].text())
    pg.color_p=convert(values["Color P"].text())
    pg.color_c=convert(values["Color C"].text())
    pg.color_drawingarea=convert(values["Drawing area"].text())
    pg.color_elementarea=convert(values["Element area"].text())
    pg.color_design=convert(values["Design"].text())
#    pg.info_box_x=np.array(pg.marker_line_x+1e-3)
#    pg.foldmarker_length=np.array(pg.wrap_rod_diameter*np.pi)#20e-3   # Length of the folding markers
    return pg,te,l,s

def display_values(values,value_dict):
    blox=blocks()
    fieldnamez=fieldnames()
    for i,block in enumerate(blox):
        for j,name in enumerate(fieldnamez[i]):
            if block=="Flags:":
                values[name].setChecked(bool(value_dict[name]))
            else:
                values[name].setText(str(value_dict[name]))
    return values