# -*- coding: utf-8 -*-
"""
Created on Tue Mar  6 11:27:39 2018

@author: andres
"""
from imp import reload 
import globalclasses as gc
import custommathfunctions as c
import numpy as np
import shapefunctions_s2s as sf
import shapely.geometry as sp

reload(sf)
reload(gc)
reload(c)
def simplify_material_parameters(s):
    materials=["p","n","c","substrate","air"]
    for material in materials:
        s.addatr(material+"_seebeck",np.average(s.g(material).seebeck))
        s.addatr(material+"_el_conductivity",np.average(s.g(material).el_cond))
        s.addatr(material+"_th_conductivity",np.average(s.g(material).th_cond))
        s.addatr(material+"_zt",np.average(s.g(material).zt))
    return s
###############################################################################
def calculate_thermal_resistance_of_generator(pg,te,s,output):
    #pg.wrap_rod_diameter=0.01*10.44/14  #PROVISORIAL
    if pg.angle!=0:
        output.N_layers=2*pg.substrate_foil_full_x/(c.sind(pg.angle)*np.pi*pg.wrap_rod_diameter)
    else:
        output.N_layers=2*pg.substrate_foil_full_x/(np.pi*pg.wrap_rod_diameter)
    p_thickness_avg=output.p_degree_of_filling_sheet*s.p_thickness
    n_thickness_avg=output.n_degree_of_filling_sheet*s.n_thickness
    r_th_fold=(s.substrate_thickness*s.air_th_conductivity+s.v_factor*s.air_thickness*s.substrate_th_conductivity)/(0.5*np.pi*pg.wrap_rod_diameter*(s.substrate_thickness+p_thickness_avg+n_thickness_avg+s.air_thickness)*s.air_th_conductivity*s.substrate_th_conductivity)
    r_th_akt=1/output.N_layers*(2*te.cell_y/(np.pi*pg.wrap_rod_diameter))/(s.substrate_thickness*s.substrate_th_conductivity+s.air_thickness*s.air_th_conductivity+p_thickness_avg*s.p_th_conductivity+n_thickness_avg*s.n_th_conductivity)
    output.ratio_for_eff_dT=r_th_akt/(2*r_th_fold+r_th_akt)
    output.thermal_resistance=(2*r_th_fold+r_th_akt)/pg.num_of_rows
    output.volume=pg.num_of_rows*(2*output.N_layers*(s.air_thickness+s.substrate_thickness)+te.cell_y*pg.wrap_rod_diameter*(p_thickness_avg+n_thickness_avg+s.air_thickness+s.substrate_thickness)) 
    output.kpi_air=output.ratio_for_eff_dT**2
    return output
###############################################################################
def calculate_electrical_resistance_of_generator(pg,te,l,s,output):
    total_length_of_connectors=sum([element.exitconnector_length for element in pg.elementlist])-sum([element.exitconnector_length!=0 for element in pg.elementlist])*pg.connector_width #us.elementlist(ii).exitconnector_length is without the overlap of the connect-fields
    resistance_connectorbands=1/s.c_el_conductivity*total_length_of_connectors/(s.c_thickness*pg.connector_width)
    resistance_p_elements=output.number_of_p_elements*1/s.p_el_conductivity*te.p.effective_y/(s.p_thickness*te.p.effective_x)
    resistance_n_elements=output.number_of_n_elements*1/s.n_el_conductivity*te.n.effective_y/(s.n_thickness*te.n.effective_x)
    number_of_centerconnectors=sum([element.exitconnector_type==1 for element in pg.elementlist])
    resistance_centerconnectors=number_of_centerconnectors*resistance_of_centreconnector( pg,te,l,s )
    resistance_turnconnectors=resistance_of_all_turnconnectors(pg,l,s)
    resistance_overlaps=resistance_overlap(s,te,output)
    output.electrical_resistance=resistance_connectorbands+resistance_p_elements+resistance_n_elements+resistance_centerconnectors+resistance_turnconnectors+resistance_overlaps
    output.electrical_resistance_p=resistance_p_elements
    output.electrical_resistance_n=resistance_n_elements
    output.electrical_resistance_c=resistance_connectorbands+resistance_centerconnectors+resistance_turnconnectors+resistance_overlaps
    return output
###############################################################################
def resistance_of_centreconnector( pg,te,l,s ):
#    if pg.connector_type!=1:
#        bottom_dummy=te.p.contactfield_top
#        top_dummy=list()
#        for field in te.n.contactfield_bottom:
#            top_dummy.append(c.translate(field,[0, pg.cell_y]))
#        first_top=top_dummy[0]
#        last_top=top_dummy[-1]
#        first_bottom=bottom_dummy[0]
#        last_bottom=bottom_dummy[-1]
#        ctop=[first_top[0],last_top[1],last_top[2],first_top[3]]
#        cbottom=[first_bottom[0],last_bottom[1],last_bottom[2],first_bottom[3]]
#        centerconnector=sf.make_connector(bottom_dummy,top_dummy,pg.connector_type,pg.connector_width,pg)
#        centerconnector=c.drawable(c.polyable(centerconnector).difference(c.polyable(ctop)))
#        cc=1
#        for shape in bottom_dummy:
#            if (c.getarea(centerconnector)-c.getarea(cbottom))>10e-10:
#                centerconnector=c.drawable(c.polyable(centerconnector).difference(c.polyable(cbottom)))
#            else:
#                cc=0    
#            if cc:
#                resistance_of_centreconnector=el_resistance(centerconnector,s.c_thickness,s.c_el_conductivity,s.precision)
#            else:
#                resistance_of_centreconnector=0
#    else:
#        resistance_of_centreconnector=el_resistance(l.p.top,s.c_thickness,s.c_el_conductivity,s.precision)+el_resistance(l.n.top,s.c_thickness,s.c_el_conductivity,s.precision) 
    return 0#resistance_of_centreconnector
###############################################################################
def el_resistance(polygon, thickness, conductivity, precision):
    n=precision
    np.append(polygon,polygon[0])
    yvalues=np.linspace(min(polygon[:,1]), max(polygon[:,1]),n)
    lengs=abs(max(polygon[:,1])-min(polygon[:,1]))/(n-1)
    prev_xvals_intersec=np.array([min(polygon[:,0]), max(polygon[:,0])])
    el_res=0
    for y in yvalues: 
        xvals,dontcare=sp.Polygon(polygon).intersection(sp.LineString([[prev_xvals_intersec[0],y],[prev_xvals_intersec[1],y]])).xy
        xvals_intersec=np.array(xvals)
        xvals_intersec=np.array([min(xvals_intersec),max(xvals_intersec)])
        width=(abs(xvals_intersec[0]-xvals_intersec[-1])+abs(prev_xvals_intersec[0]-prev_xvals_intersec[1]))/2
        el_res=el_res+1/conductivity*lengs/(thickness*width)
        prev_xvals_intersec=xvals_intersec
    return el_res
###############################################################################
def resistance_of_all_turnconnectors(pg,c,s):
    sum_of_resistances=0
    res_p_top=el_resistance(c.p.top,s.c_thickness,s.c_el_conductivity,s.precision)
    res_p_bottom=el_resistance(c.p.bottom,s.c_thickness,s.c_el_conductivity,s.precision) 
    res_n_top=el_resistance(c.n.top,s.c_thickness,s.c_el_conductivity,s.precision)
    res_n_bottom=el_resistance(c.n.bottom,s.c_thickness,s.c_el_conductivity,s.precision) 
    for element in pg.elementlist:
        if element.exitconnector_type==0:
            if element.type=='n':
                sum_of_resistances=sum_of_resistances+res_n_top
            else:
                sum_of_resistances=sum_of_resistances+res_p_top
        if element.next_element_index!=0:
            if pg.elementlist[element.next_element_index-1].type=='n':
                sum_of_resistances=sum_of_resistances+res_n_bottom
            else:
                sum_of_resistances=sum_of_resistances+res_p_bottom
    return sum_of_resistances
###############################################################################
def resistance_overlap(s,te,output):
    res_p_path_c=1/s.c_el_conductivity*te.p.overlap/(te.p.effective_x*s.c_thickness)+s.contact_resistance_cp
    res_p_path_p=1/s.p_el_conductivity*te.p.overlap/(te.p.effective_x*s.p_thickness)+s.contact_resistance_cp
    res_n_path_c=1/s.c_el_conductivity*te.n.overlap/(te.n.effective_x*s.c_thickness)+s.contact_resistance_cn
    res_n_path_n=1/s.n_el_conductivity*te.n.overlap/(te.n.effective_x*s.n_thickness)+s.contact_resistance_cn
    resistance_overlap=output.number_of_p_elements*c.par(res_p_path_p,res_p_path_c)+ output.number_of_n_elements*c.par(res_n_path_n,res_n_path_c)
    return resistance_overlap 
###############################################################################
def calculate_voltage_per_kelvin(pg,te,s,output):
    res_c_path_p=1/s.c_el_conductivity*te.p.overlap/(te.p.effective_x*s.c_thickness)+s.contact_resistance_cp
    res_p_path=1/s.p_el_conductivity*te.p.overlap/(te.p.effective_x*s.p_thickness)+s.contact_resistance_cp
    res_ges_p=res_c_path_p+res_p_path
    res_n_path=1/s.n_el_conductivity*te.n.overlap/(te.n.effective_x*s.n_thickness)+s.contact_resistance_cn
    res_c_path_n=1/s.c_el_conductivity*te.n.overlap/(te.n.effective_x*s.c_thickness)+s.contact_resistance_cn
    res_ges_n=res_c_path_n+res_n_path
    if pg.effective_y_optimization:
        vpk_p_element=s.p_seebeck*te.p.effective_y/te.cell_y
        vpk_n_element=-s.n_seebeck*te.n.effective_y/te.cell_y
        if pg.element_of_lower_value=='n':
            factor=-1
        else:
            factor=1
        vpk_p_element_connector=factor*s.c_seebeck*(te.cell_y-te.p.effective_y-2*te.p.overlap)/te.cell_y
        vpk_n_element_connector=factor*s.c_seebeck*(te.cell_y-te.n.effective_y-2*te.n.overlap)/te.cell_y
        u_p=factor*s.p_seebeck*te.p.overlap/te.cell_y
        u_c_p=factor*s.c_seebeck*te.p.overlap/te.cell_y
        u_n=factor*s.n_seebeck*te.n.overlap/te.cell_y
        u_c_n=factor*s.c_seebeck*te.n.overlap/te.cell_y
    else:
        vpk_p_element=s.p_seebeck*te.p.effective_y/te.cell_y
        vpk_p_element_connector=s.c_seebeck*(te.cell_y-te.p.effective_y-2*te.p.overlap)/te.cell_y
        vpk_n_element=-s.n_seebeck*te.n.effective_y/te.cell_y
        vpk_n_element_connector=s.c_seebeck*(te.cell_y-te.n.effective_y-2*te.n.overlap)/te.cell_y
        u_p=s.p_seebeck*te.p.overlap/te.cell_y
        u_c_p=s.c_seebeck*te.p.overlap/te.cell_y
        u_n=-s.n_seebeck*te.n.overlap/te.cell_y
        u_c_n=-s.c_seebeck*te.n.overlap/te.cell_y
    vpk_overlap_p_element=(u_p*res_c_path_p/res_ges_p+u_c_p*res_p_path/res_ges_p)
    vpk_overlap_n_element=u_n*res_c_path_n/res_ges_n+u_c_n*res_n_path/res_ges_n
    output.voltage_per_kelvin=output.number_of_n_elements*(vpk_n_element+vpk_n_element_connector+2*vpk_overlap_n_element) +output.number_of_p_elements*(vpk_p_element+vpk_p_element_connector+2*vpk_overlap_p_element)   
    return output