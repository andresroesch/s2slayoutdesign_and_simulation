# -*- coding: utf-8 -*-
"""
Created on Mon Jul  9 18:22:30 2018

@author: andres
"""
import multiprocessing as mp
from joblib import Parallel, delayed
import disp as d
def processInput(i):
    return i * i
p=mp.Pool(4)
a=list(range(10000))
t1=d.Disp1("Parallel")
num_cores = mp.cpu_count()
results_par = Parallel(n_jobs=num_cores)(delayed(processInput)(i) for i in a)
d.Disp2(t1)
t1=d.Disp1("Series")
results_ser=list()
for i in a:
    results_ser.append(i*i)
d.Disp2(t1)