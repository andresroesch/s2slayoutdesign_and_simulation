module load devel/python/3.5.2
cd TEG
python -m venv virtualenvironment
source virtualenvironment/bin/activate
pip install --upgrade pip
pip list 
pip install shapely
pip install numpy
pip install time
pip install PIL
pip install matplotlib
python testscript.py 
deactivate
rm -rf virtualenvironment
touch jobterminated