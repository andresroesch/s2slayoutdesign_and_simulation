# -*- coding: utf-8 -*-
"""
Created on Sun Mar  4 10:51:34 2018

@author: andres
"""
from imp import reload 
import globalclasses as gc
import custommathfunctions as c
import simulation_kppv as sk
import shapely.ops as so
import shapely.geometry as sp
import disp as d 
reload(d)
reload(gc)
reload(c)

def count_thermocouples(pg,output):
    output.number_of_p_elements=0
    output.number_of_n_elements=0
    for element in pg.elementlist:
        if element.type=='n':
            output.number_of_n_elements=output.number_of_n_elements+1
        else:
            output.number_of_p_elements=output.number_of_p_elements+1
    output.number_of_thermocouples=output.number_of_p_elements        
    return output
###############################################################################
def calculating_areas(im,te,output):
    t1=d.Disp1("Calculating areas")
    p_areas=list()
    n_areas=list()
    for element in im.elementlist:
        for strip in element.stripes:
            if sp.Polygon(strip).is_valid:
                exec(element.type+"_areas.append(sp.Polygon(strip))")
            else:
                exec(element.type+"_areas.append(sp.Polygon(strip).buffer(0))")
        if sp.Polygon(element.exitconnector).is_valid:
            exec(element.exitconnector_mat+"_areas.append(sp.Polygon(element.exitconnector))")
        else:
            exec(element.exitconnector_mat+"_areas.append(sp.Polygon(element.exitconnector).buffer(0))")
    exec(im.connectormaterial+"_areas.append(sp.Polygon(im.input_connector).buffer(0))")
    if im.include_contactfield:
        exec(im.connectormaterial+"_areas.append(sp.Polygon(im.contactfield.main).buffer(0))") 
    p_area=so.unary_union(p_areas)
    n_area=so.unary_union(n_areas)
#    overlap=p_area.intersection(n_area)
    generator=so.unary_union([p_area,n_area])
    output.p_area=p_area.area
    output.n_area=n_area.area
    output.generator_area=generator.area

#    for element in pg.elementlist:
#        for j,strip in enumerate(te.g(element.type).stripes):
#            shrank_stripe=c.shrink(c.translate(c.rotate(strip,pg.angle,[0,0]),element.midpoint),sf[element.type],pg)
#            shrank_contactfield_top=c.shrink(c.translate(c.rotate(te.g(element.type).contactfield_top[j],pg.angle,[0,0]),element.midpoint),sf[element.type],pg)
#            shrank_contactfield_bottom=c.shrink(c.translate(c.rotate(te.g(element.type).contactfield_bottom[j],pg.angle,[0,0]),element.midpoint),sf[element.type],pg)
#            output.area.nc_s=output.area.nc_s+c.getarea(shrank_stripe)-c.getarea(shrank_contactfield_top)-c.getarea(shrank_contactfield_bottom) 
#            stripe=c.translate(c.rotate(strip,pg.angle,[0,0]),element.midpoint)
#            contactfield_top=c.translate(c.rotate(te.n.contactfield_top[j],pg.angle,[0,0]),element.midpoint)
#            contactfield_bottom=c.translate(c.rotate(te.n.contactfield_bottom[j],pg.angle,[0,0]),element.midpoint)
#            output.area.nc=output.area.nc+c.getarea(stripe)-c.getarea(contactfield_top)-c.getarea(contactfield_bottom)
#            output.area.n=output.area.n+c.getarea(stripe)
#        output.area.c_s=output.area.c_s+c.getarea(c.shrink(element.exitconnector,sf[element.type],pg))
#        output.area.c=output.area.c+c.getarea(element.exitconnector)
#        output.area.nc_s=output.area.nc_s+c.getarea(c.shrink(element.exitconnector,sf[element.type],pg))
#        output.area.nc=output.area.nc+c.getarea(element.exitconnector)
#        output.area.pc_s=output.area.pc_s+c.getarea(c.shrink(element.exitconnector,sf[element.type],pg))
#        output.area.pc=output.area.pc+c.getarea(element.exitconnector)
#    # Adding inputconnector
#    output.area.c_s=output.area.c_s+c.getarea(c.shrink(pg.input_connector,sf[pg.connectormaterial],pg))
#    output.area.pc_s=output.area.pc_s+c.getarea(c.shrink(pg.input_connector,sf[pg.connectormaterial],pg))
#    output.area.nc_s=output.area.nc_s+c.getarea(c.shrink(pg.input_connector,sf[pg.connectormaterial],pg))
#    output.area.c=output.area.c+c.getarea(pg.input_connector)
#    output.area.pc=output.area.pc+c.getarea(pg.input_connector)
#    output.area.nc=output.area.nc+c.getarea(pg.input_connector)
#    #  Adding contactfield
#    if pg.include_contactfield:
#        output.area.c=output.area.c+c.getarea(pg.contactfield.main)-2*pg.connector_width**2
#        output.area.c_s=output.area.c_s+c.getarea(c.shrink(pg.contactfield.main,sf[pg.connectormaterial],pg))-c.getarea(c.shrink(pg.contactfield.into,sf[pg.connectormaterial],pg))-c.getarea(c.shrink(pg.contactfield.out,sf[pg.connectormaterial],pg))
#        output.area.nc=output.area.nc+c.getarea(pg.contactfield.main)-2*pg.connector_width**2
#        output.area.nc_s=output.area.nc_s+c.getarea(c.shrink(pg.contactfield.main,sf[pg.connectormaterial],pg))-c.getarea(c.shrink(pg.contactfield.into,sf[pg.connectormaterial],pg))-c.getarea(c.shrink(pg.contactfield.out,sf[pg.connectormaterial],pg))
#        output.area.pc=output.area.pc+c.getarea(pg.contactfield.main)-2*pg.connector_width**2
#        output.area.pc_s=output.area.pc_s+c.getarea(c.shrink(pg.contactfield.main,sf[pg.connectormaterial],pg))-c.getarea(c.shrink(pg.contactfield.into,sf[pg.connectormaterial],pg))-c.getarea(c.shrink(pg.contactfield.out,sf[pg.connectormaterial],pg))
#    # Adding Markers
#    if pg.setmarkers:
#        for shape in pg.marker.firstpress:
#            output.area.c=output.area.c+c.getarea(pg.marker.firstpress[shape])
#            output.area.c_s=output.area.c_s+c.getarea(c.shrink(pg.marker.firstpress[shape],pg.shrinkfactor_unit1,pg))
#            output.area.pc_s=output.area.pc_s+c.getarea(c.shrink(pg.marker.firstpress[shape],pg.shrinkfactor_unit1,pg))
#            output.area.nc_s=output.area.nc_s+c.getarea(c.shrink(pg.marker.firstpress[shape],pg.shrinkfactor_unit1,pg))
#        for shape in pg.marker.secondpress:
#            output.area.p=output.area.p+c.getarea(pg.marker.secondpress[shape])
#            output.area.pc_s=output.area.pc_s+c.getarea(c.shrink(pg.marker.secondpress[shape],pg.shrinkfactor_unit2,pg))
#        
#        for shape in pg.marker.thirdpress:
#            output.area.n=output.area.n+c.getarea(pg.marker.thirdpress[shape])
#            output.area.nc_s=output.area.nc_s+c.getarea(c.shrink(pg.marker.thirdpress[shape],pg.shrinkfactor_unit3,pg))
#    # Printed area
    output.sheet_area=im.sheet_full_y*im.sheet_full_x
    output.printingarea_area=c.getarea(im.printingarea)
    output.substrate_area=c.getarea(im.substrate)
    d.Disp2(t1)
    return output
###############################################################################
def calculating_degree_of_filling(pg,te,output):
    output.p_degree_of_filling_sheet=output.p_area/output.sheet_area
    output.n_degree_of_filling_sheet=output.n_area/output.sheet_area
    output.generator_degree_of_filling_sheet=output.generator_area/output.sheet_area
    
    output.p_degree_of_filling_printingarea=output.p_area/output.printingarea_area
    output.n_degree_of_filling_printingarea=output.n_area/output.printingarea_area
    output.generator_degree_of_filling_printingarea=output.generator_area/output.printingarea_area
    
    output.p_degree_of_filling_substrate=output.p_area/output.substrate_area
    output.n_degree_of_filling_substrate=output.n_area/output.substrate_area
    output.generator_degree_of_filling_substrate=output.generator_area/output.substrate_area
#    area=gc.voidclass()   
#    area.c=0
#    area.n=0
#    area.p=0
#    area.nc=0
#    area.pc=0
#    ## CALCULATING AREAS AGAIN WHICH ARE INSIDE PRINTINGAREA
#    for element in pg.elementlist:
#        if element.type=='n':
#            for jj,strip in enumerate(te.n.stripes):
#                stripe=c.translate(c.rotate(strip,pg.angle,[0 ,0]),element.midpoint)
#                contactfield_top=c.translate(c.rotate(te.n.contactfield_top[jj],pg.angle,[0 ,0]),element.midpoint)
#                contactfield_bottom=c.translate(c.rotate(te.n.contactfield_bottom[jj],pg.angle,[0 ,0]),element.midpoint)
#                area.nc=area.nc+c.getarea(stripe)-c.getarea(contactfield_top)-c.getarea(contactfield_bottom)
#                area.n=area.n+c.getarea(stripe)     
#        elif element.type=='p':
#            for jj,strip in enumerate(te.p.stripes):
#                stripe=c.translate(c.rotate(strip,pg.angle,[0 ,0]),element.midpoint)
#                contactfield_top=c.translate(c.rotate(te.p.contactfield_top[jj],pg.angle,[0, 0]),element.midpoint)
#                contactfield_bottom=c.translate(c.rotate(te.p.contactfield_bottom[jj],pg.angle,[0 ,0]),element.midpoint)
#                area.pc=area.pc+c.getarea(stripe)-c.getarea(contactfield_top)-c.getarea(contactfield_bottom)
#                area.p=area.p+c.getarea(stripe)
#        area.c=area.c+c.getarea(element.exitconnector)               
#    area.c=area.c+c.getarea(pg.input_connector)
#    ## Calculation fill percentages
#    output.fill_percentage=gc.voidclass()
#    output.fill_percentage.printingarea=gc.voidclass()
#    output.fill_percentage.full_sheet=gc.voidclass()
#    output.fill_percentage.printingarea.total=(area.c+area.pc+area.nc)*100/output.area.printingarea ## area.pc/nc is only the active area and not whole connectors included
#    output.fill_percentage.printingarea.c=area.c*100/output.area.printingarea
#    output.fill_percentage.printingarea.n=area.n*100/output.area.printingarea
#    output.fill_percentage.printingarea.p=area.p*100/output.area.printingarea
#    output.fill_percentage.printingarea.nc=(area.c+area.nc)*100/output.area.printingarea   ## area.pc/nc is only the active area and not whole connectors included
#    output.fill_percentage.printingarea.pc=(area.c+area.pc)*100/output.area.printingarea   ## area.pc/nc is only the active area and not whole connectors included
#    output.fill_percentage.full_sheet.total=(output.area.pc+output.area.nc-output.area.c)*100/output.area.sheet ## output.area.pc/nc do have connectors included
#    output.fill_percentage.full_sheet.c=output.area.c*100/output.area.sheet
#    output.fill_percentage.full_sheet.n=output.area.n*100/output.area.sheet
#    output.fill_percentage.full_sheet.p=output.area.p*100/output.area.sheet
#    output.fill_percentage.full_sheet.nc=output.area.nc*100/output.area.sheet
#    output.fill_percentage.full_sheet.pc=output.area.pc*100/output.area.sheet
    return output
###############################################################################
def calculating_material_usage(pg,te,s,output):
    output.p_material_usage=output.p_area*s.p_wetfilm_thickness
    output.n_material_usage=output.n_area*s.n_wetfilm_thickness
    output.generator_material_usage=output.p_material_usage+output.n_material_usage
    return output