# -*- coding: utf-8 -*-
"""
Created on Wed Sep  5 11:32:08 2018

@author: andre
"""

from scipy.sparse import lil_matrix
from scipy.sparse.linalg import spsolve
from numpy.linalg import solve, norm
from numpy.random import rand
import disp as d 
A = lil_matrix((10000, 10000))
A[0, :100] = rand(100)
A[1, 100:200] = A[0, :100]
A.setdiag(rand(10000))
A = A.tocsr()
B=A.todense()
b = rand(10000)
t1=d.Disp1("Sparse")
x = spsolve(A, b)
d.Disp2(t1)
t1=d.Disp1("Dense")
y = solve(B, b)
d.Disp2(t1)