# -*- coding: utf-8 -*-
"""
Created on Wed Aug 30 16:06:55 2017

@author: Andres
"""
from imp import reload 
import disp as d
import numpy as np
import globalclasses as gc
import custommathfunctions as c
import shapely.geometry as sp
import shapely.ops as so
import directions as dr
import customboolean as cb
import pswriter_s2s as ps 
reload(ps)
reload(cb)
reload(dr)
reload(c)
reload(gc)
reload (d)
###############################################################################
def makerectangle(x,y,m):
    return np.array([[m[0]-x/2,m[1]-y/2],[m[0]+x/2,m[1]-y/2],[m[0]+x/2,m[1]+y/2],[m[0]-x/2,m[1]+y/2]])
###############################################################################
def makecircle(center,radius):
    res=100
    n=(np.arange(res))/res
    n=np.append(n,n[0])
    return np.array([center[0]+radius*np.cos(2*np.pi*n),center[1]+radius*np.sin(2*np.pi*n)]).transpose()
###############################################################################
def form_shapes_of_thermoelement(te):
    t1=d.Disp1('Creating thermoelement shapes')
    for m in ['n','p']:
        te.g(m).outline=makerectangle(te.g(m).x,te.g(m).y,[0, 0]);
        te.g(m).active_area=makerectangle(te.g(m).x,te.g(m).effective_y,[0, 0])
        te.g(m).stripes = list()#[gc.voidclass() for i in range(int(te.g(m).num_of_stripes))]
        te.g(m).contactfield_top = list()#[gc.voidclass() for i in range(int(te.g(m).num_of_stripes))]
        te.g(m).contactfield_bottom = list()#[gc.voidclass() for i in range(int(te.g(m).num_of_stripes))]
       
        te.g(m).stripes.append(np.array([
        [te.g(m).outline[0,0],te.g(m).outline[0,1] ],
        [te.g(m).outline[0,0]+te.g(m).stripes_x,te.g(m).outline[0,1]],
        [te.g(m).outline[3,0]+te.g(m).stripes_x,te.g(m).outline[3,1]],
        [te.g(m).outline[3,0],te.g(m).outline[3,1]]]))
        te.g(m).contactfield_top.append(np.array([
        [te.g(m).outline[0,0],te.g(m).outline[3,1]-te.g(m).overlap],
        [te.g(m).outline[0,0]+te.g(m).stripes_x,te.g(m).outline[3,1]-te.g(m).overlap],
        [te.g(m).outline[3,0]+te.g(m).stripes_x,te.g(m).outline[3,1]],
        [te.g(m).outline[3,0],te.g(m).outline[3,1]]]))
        te.g(m).contactfield_bottom.append(np.array([
        [te.g(m).outline[0,0],te.g(m).outline[0,1]],
        [te.g(m).outline[0,0]+te.g(m).stripes_x,te.g(m).outline[0,1]],
        [te.g(m).outline[3,0]+te.g(m).stripes_x,te.g(m).outline[0,1]+te.g(m).overlap],
        [te.g(m).outline[3,0],te.g(m).outline[0,1]+te.g(m).overlap]]))
        te.g(m).overlaparea_top=c.drawable(so.cascaded_union(c.polyable(te.g(m).contactfield_top)).convex_hull)
        te.g(m).overlaparea_bottom=c.drawable(so.cascaded_union(c.polyable(te.g(m).contactfield_bottom)).convex_hull)
    d.Disp2(t1)
    return te
############################################################################
def make_linker(pg,te,l):
    t1=d.Disp1('Creating connector shapes')
    for m in ['n','p']:
        first_top=te.g(m).contactfield_top[0]
        last_top=te.g(m).contactfield_top[-1]
        first_bottom=te.g(m).contactfield_bottom[0]
        last_bottom=te.g(m).contactfield_bottom[-1]
        l.addatr(m,gc.voidclass())
        if l.linker_type==1:
            l.g(m).top=np.array([first_top[0,:],last_top[1,:],last_top[2,:],
            [pg.connector_width/2, te.n.y/2+l.linker_height-l.linker_length],
            [pg.connector_width/2, te.n.y/2+l.linker_height],
            [-pg.connector_width/2, te.n.y/2+l.linker_height],
            [-pg.connector_width/2, te.n.y/2+l.linker_height-l.linker_length],
            first_top[3,:]])
            
            l.g(m).bottom=np.array([first_bottom[0,:],
            [-pg.connector_width/2, -te.n.y/2-l.linker_height+l.linker_length],
            [-pg.connector_width/2, -te.n.y/2-l.linker_height],
            [pg.connector_width/2, -te.n.y/2-l.linker_height],
            [pg.connector_width/2, -te.n.y/2-l.linker_height+l.linker_length],               
            last_bottom[1,:],last_bottom[2,:],first_bottom[3,:]])
            
            l.g(m).top_connect=makerectangle(pg.connector_width,pg.connector_width,[0, (te.g(m).y-pg.connector_width)/2+l.linker_height])
            l.g(m).bottom_connect=makerectangle(pg.connector_width,pg.connector_width,[0, -(te.g(m).y-pg.connector_width)/2-l.linker_height])
        elif l.linker_type==2:
            l.g(m).top=np.array([first_top[0,:],last_top[1,:],last_top[1,:]+np.array([0, pg.connector_width]),first_top[0,:]+np.array([0, pg.connector_width])])
            l.g(m).bottom=np.array([first_bottom[3,:]-np.array([0, pg.connector_width]),last_bottom[2,:]-np.array([0, pg.connector_width]),last_bottom[2,:],first_bottom[3,:]])
            l.g(m).top_connect=makerectangle(pg.connector_width,pg.connector_width,[0,(te.g(m).y-2*te.g(m).overlap+pg.connector_width)/2]);
            l.g(m).bottom_connect=makerectangle(pg.connector_width,pg.connector_width,[0, -(te.g(m).y-2*te.g(m).overlap+pg.connector_width)/2]);
        elif l.linker_type==3:
            l.g(m).top=np.array([first_top[0,:],last_top[1,:],last_top[2,:],first_top[3,:]])
            l.g(m).bottom=np.array([first_bottom[3,:],last_bottom[2,:],last_bottom[1,:],first_bottom[0,:]])
            l.g(m).top_connect=makerectangle(pg.connector_width,pg.connector_width,[0, (te.g(m).y-2*te.g(m).overlap+pg.connector_width)/2]);
            l.g(m).bottom_connect=makerectangle(pg.connector_width,pg.connector_width,[0, -(te.g(m).y-2*te.g(m).overlap+pg.connector_width)/2]);
    d.Disp2(t1)
    return l
###############################################################################################
def make_contactfield(pg,te):
    t1=d.Disp1('Setting contactfield')
    sizefactor=pg.number_of_deleted_rows+1
    if pg.angle!=0:
        for field in pg.fieldlist:
            if field.index[0]==pg.cutlinerow:
                anker=field.corners[1,:];
                break
    else:
        anker=pg.cell_y/2
    pg.contactfield=gc.voidclass()    
    pg.contactfield.main=np.array([[pg.contactfield_spacing_left,pg.contactfield_spacing_left*c.tand(pg.angle)],
    [pg.substratelimits.maximum[0]-pg.contactfield_spacing_right-pg.printinglimits.maximum[0],(pg.substratelimits.maximum[0]-pg.contactfield_spacing_right-pg.printinglimits.maximum[0])*c.tand(pg.angle)],
    [pg.substratelimits.maximum[0]-pg.contactfield_spacing_right-pg.printinglimits.maximum[0],(pg.substratelimits.maximum[0]-pg.contactfield_spacing_right-pg.printinglimits.maximum[0])*c.tand(pg.angle)+pg.cell_y/c.cosd(pg.angle)],
    [pg.contactfield_spacing_left, pg.contactfield_spacing_left*c.tand(pg.angle)+pg.cell_y/c.cosd(pg.angle)]]) 
    pg.contactfield.main[np.int(1-np.sign(pg.angle)),:]=c.translate(pg.contactfield.main[np.int(1-np.sign(pg.angle)),:],np.array([0, -np.sign(pg.angle)])*pg.number_of_deleted_rows*pg.cell_y/c.cosd(pg.angle))
    pg.contactfield.main[np.int(2-np.sign(pg.angle)),:]=c.translate(pg.contactfield.main[np.int(2-np.sign(pg.angle)),:],np.array([0, -np.sign(pg.angle)])*pg.number_of_deleted_rows*pg.cell_y/c.cosd(pg.angle)) 
    pg.contactfield.main=c.translate(pg.contactfield.main,anker)
    pg.contactfield.main[2]=c.translate(pg.contactfield.main[2],np.array([0,1])*pg.cell_y*-pg.reduce_contactfield/c.cosd(pg.angle))
    pg.contactfield.main[3]=c.translate(pg.contactfield.main[3],np.array([0,1])*pg.cell_y*-pg.reduce_contactfield/c.cosd(pg.angle))
#    ps.draw("output_files/"+pg.filename+"/"+pg.filename+"_rgb.eps",pg.contactfield.main,[1,0,0])
    if pg.angle>0:
        pg.contactfield.window=np.array([[0, -pg.window_width/2],[0, +pg.window_width/2],[+pg.window_length, +pg.window_width/2],[+pg.window_length, -pg.window_width/2]])
        pg.contactfield.windowfield=np.array([[0, -pg.window_width/4],[+pg.window_length, +pg.window_width/4],[0, +pg.window_width/4],[+pg.window_length, -pg.window_width/4]])
        pg.contactfield.window=c.translate(pg.contactfield.window,np.array([0,-2])*1e-3);
        pg.contactfield.windowfield=c.translate(pg.contactfield.windowfield,np.array([0,-2])*1e-3);
        pg.contactfield.window=c.translate(c.rotate(pg.contactfield.window,pg.angle,np.array([0, 0])),anker+np.array([0, -1])*pg.cell_y/c.sind(pg.angle))
        pg.contactfield.windowfield=c.translate(c.rotate(pg.contactfield.windowfield,pg.angle,np.array([0, 0])),anker+np.array([0, -1])*pg.cell_y/c.sind(pg.angle))
        pg.contactfield.window=c.translate(pg.contactfield.window,pg.cell_y*sizefactor/2*(dr.up(pg.angle)+dr.right(pg.angle))+np.array([c.cosd(pg.angle), c.sind(pg.angle)])*((pg.substratelimits.maximum[0]-pg.contactfield_spacing_right-pg.contactfield.main[1,0])/c.cosd(pg.angle)));
        pg.contactfield.windowfield=c.translate(pg.contactfield.windowfield,pg.cell_y*sizefactor/2*(dr.up(pg.angle)+dr.right(pg.angle))+np.array([c.cosd(pg.angle), c.sind(pg.angle)])*((pg.substratelimits.maximum[0]-pg.contactfield_spacing_right-pg.contactfield.main[1,0])/c.cosd(pg.angle)));
    pg.contactfield.main=c.translate(pg.contactfield.main,np.array([c.cosd(pg.angle), c.sind(pg.angle)])*((pg.substratelimits.maximum[0]-pg.contactfield_spacing_right-pg.contactfield.main[1,0])/c.cosd(pg.angle)));
#    ps.draw("output_files/"+pg.filename+"/"+pg.filename+"_rgb.eps",pg.contactfield.main,[0,0,0])
    contactfield_out_intern=np.array([pg.contactfield.main[0,:],pg.contactfield.main[0,:]+pg.connector_width*dr.right(pg.angle),pg.contactfield.main[0,:]+pg.connector_width*(dr.up(pg.angle)+dr.right(pg.angle)),pg.contactfield.main[0,:]+pg.connector_width*dr.up(pg.angle)])
    contactfield_in_intern=np.array([pg.contactfield.main[3,:]-pg.connector_width*(dr.left(pg.angle)-dr.down(pg.angle)),pg.contactfield.main[3,:]+pg.connector_width*dr.down(pg.angle),pg.contactfield.main[3,:],pg.contactfield.main[3,:]-pg.connector_width*dr.left(pg.angle)])
    
#    contactfield_in_intern=c.translate(contactfield_in_intern,np.array([0,-2])*te.cell_y*c.tand(pg.angle))
#    ps.draw("output_files/"+pg.filename+"/"+pg.filename+"_rgb.eps",contactfield_out_intern,[1,0,0])
#    ps.draw("output_files/"+pg.filename+"/"+pg.filename+"_rgb.eps",contactfield_in_intern,[1,0,1])
#    if pg.angle>=0:
#    contactfield_in_intern=c.translate(contactfield_in_intern,(dr.down(pg.angle))*pg.space_between_last_connector_and_last_row);
#    contactfield_out_intern=c.translate(contactfield_out_intern,dr.right(pg.angle)*np.linalg.norm((c.get_midpoint(contactfield_in_intern)-c.get_midpoint(contactfield_out_intern)))*c.cosd(pg.angle))
#    pg.contactfield.out=contactfield_out_intern
    pg.contactfield.out=c.translate(contactfield_out_intern,(dr.left(pg.angle))*((contactfield_out_intern[3,0]-pg.elementslimits.maximum[0]-pg.connectorspacing)/c.cosd(pg.angle)+0.01e-3))
    
#    pg.contactfield.main=c.translate(pg.contactfield.main,np.array([0,-2])*te.cell_y*c.tand(pg.angle))
    
    pg.contactfield.into=c.translate(contactfield_in_intern,(dr.left(pg.angle))*((contactfield_in_intern[2,0]-pg.elementslimits.maximum[0]-pg.connectorspacing)/c.cosd(pg.angle)+0.01e-3))
    pg.contactfield.into=c.translate(pg.contactfield.into,dr.down(pg.angle)*(pg.space_between_last_connector_and_last_row))
#    elif pg.angle<0:
#        contactfield_out_intern=c.translate(contactfield_out_intern,(dr.up(pg.angle))*pg.space_between_last_connector_and_last_row)
#        contactfield_in_intern=c.translate(contactfield_in_intern,dr.right(pg.angle)*np.linalg.norm((c.get_midpoint(contactfield_in_intern)-c.get_midpoint(contactfield_out_intern)))*c.cosd(pg.angle));
#        pg.contactfield.out=c.translate(contactfield_out_intern,(dr.left(pg.angle))*((contactfield_out_intern[1,0]-pg.elementslimits.maximum[0]-pg.connectorspacing)/c.cosd(pg.angle)+0.01e-3));
#        pg.contactfield.into=c.translate(contactfield_in_intern,(dr.left(pg.angle))*((contactfield_in_intern[0,0]-pg.elementslimits.maximum[0]-pg.connectorspacing)/c.cosd(pg.angle)+0.01e-3));
#    pg.contactfield.main=c.drawable(so.cascaded_union([
    inline=so.cascaded_union([c.polyable(pg.contactfield.into),c.polyable(contactfield_in_intern)]).convex_hull
    outline=so.cascaded_union([c.polyable(pg.contactfield.out),c.polyable(contactfield_out_intern)]).convex_hull
#            c.polyable(c.connect_two_fields_in_a_line(contactfield_out_intern,pg.contactfield.out)),
#            c.polyable(c.connect_two_fields_in_a_line(contactfield_in_intern,pg.contactfield.into)),
#            c.polyable(pg.contactfield.main)]))
    
    pg.contactfield.main=c.drawable(so.cascaded_union([inline,outline,c.polyable(pg.contactfield.main)]))
    if pg.angle<0:
        temp=pg.contactfield.into
        pg.contactfield.into=pg.contactfield.out
        pg.contactfield.out=temp
    elif pg.angle==0:
        pg.contactfield.out=c.translate(pg.contactfield.out,np.array([0 ,pg.sheet_full_y]))
    mid_out=c.get_midpoint(pg.contactfield.out);
    mid_in=c.get_midpoint(pg.contactfield.into);
    pg.length_contacfield_connectors=(pg.printinglimits.maximum[0]+pg.contactfield_spacing_left-mid_in[0])/c.cosd(pg.angle)+(pg.printinglimits.maximum[0]+pg.contactfield_spacing_left-mid_out[0])/c.cosd(pg.angle)
    d.Disp2(t1)
    return pg
###############################################################################
def make_connector(bottom_dummy,top_dummy,pg):
#    ps.draw("output_files/"+pg.filename+"/"+pg.filename+"_rgb.eps",bottom_dummy,[0,0,0])
#    ps.draw("output_files/"+pg.filename+"/"+pg.filename+"_rgb.eps",top_dummy,[1,0,0])
    connector=c.drawable(so.cascaded_union(c.polyable([bottom_dummy,top_dummy])).convex_hull)
    return connector
###############################################################################
def connect_it(pg,field1,field2,chosen_path):
    pos_field1=c.get_midpoint(field1)
    pos_field2=c.get_midpoint(field2)
    biggest_length_in_sheet=5*np.sqrt(pg.sheet_full_x**2+pg.sheet_full_y**2);
    horizontal1=sp.Polygon(c.rotate(makerectangle(biggest_length_in_sheet,pg.connector_width,pos_field1),pg.angle,pos_field1))
    vertical1=sp.Polygon(c.rotate(makerectangle(pg.connector_width,biggest_length_in_sheet,pos_field1),pg.angle,pos_field1))
    horizontal2=sp.Polygon(c.rotate(makerectangle(biggest_length_in_sheet,pg.connector_width,pos_field2),pg.angle,pos_field2))
    vertical2=sp.Polygon(c.rotate(makerectangle(pg.connector_width,biggest_length_in_sheet,pos_field2),pg.angle,pos_field2))
    if not(chosen_path):
        crossing=horizontal2.intersection(vertical1)
    else:
        crossing=horizontal1.intersection(vertical2)
    joint1=so.cascaded_union([sp.Polygon(field1),crossing]).convex_hull.buffer(0)
    joint2=so.cascaded_union([sp.Polygon(field2),crossing]).convex_hull.buffer(0)
    connection=c.drawable(so.cascaded_union([joint1,joint2]))
    return connection
###############################################################################
def clipping_exitconnectors(pg):
    for element in pg.elementlist:
        line=sp.Polygon(element.exitconnector)
        if element.element_in_series==1:
            if element.exit_direction:
                pg.input_connector=c.drawable(so.cascaded_union([sp.Polygon(element.bottom),sp.Polygon(pg.input_connector)]))
            else:
                pg.input_connector=c.drawable(so.cascaded_union([sp.Polygon(element.top),sp.Polygon(pg.input_connector)]))
        if not(element.exitconnector_type) or pg.connector_type==1:
            if element.exit_direction:
                linker1=sp.Polygon(element.top)
            else:
                linker1=sp.Polygon(element.bottom)
            if element.next_element_index!=len(pg.elementlist):
                if pg.elementlist[element.next_element_index].exit_direction:
                    linker2=sp.Polygon(c.translate(pg.elementlist[element.next_element_index].bottom,[0, element.page_jump*pg.sheet_full_y]))
                else:
                    linker2=sp.Polygon(c.translate(pg.elementlist[element.next_element_index].top,[0, element.page_jump*pg.sheet_full_y]))
            else:
                linker2=linker1
            element.exitconnector=c.drawable(so.cascaded_union([line,linker1,linker2]))
    return pg

def set_syncmarkers(pg):
    trapez=np.array([ [0, 0],[ 2e-3+pg.marker_width*np.tan(30/180*np.pi), 0],[ 2e-3, pg.marker_width], [0, pg.marker_width ]])
    sync_markers=list()
    sync_markers.append(np.array([ [30e-3, 0],[ 30e-3+2.5e-3, 0], [30e-3+2.5e-3, pg.marker_width],[ 30e-3, pg.marker_width ]]))
    sync_markers.append(np.array([ [30e-3+2.5e-3+5e-3, 0],[ 30e-3+2.5e-3+5e-3+5e-3, 0],[ 30e-3+2.5e-3+5e-3+5e-3, pg.marker_width],[ 30e-3+2.5e-3+5e-3, pg.marker_width ]]))
    sync_markers.append(np.array([ [30e-3+2.5e-3+5e-3+5e-3+2.5e-3, 0],[ 30e-3+2.5e-3+5e-3+5e-3+2.5e-3+2.5e-3, 0],[ 30e-3+2.5e-3+5e-3+5e-3+2.5e-3+2.5e-3, pg.marker_width],[ 30e-3+2.5e-3+5e-3+5e-3+2.5e-3, pg.marker_width ]]))
    sync_markers.append(trapez+np.array([ 30e-3+25.5e-3, 0 ]))
    sync_markers.append(trapez+np.array([ 30e-3+25.5e-3+39e-3, 0 ]))
    sync_markers.append(trapez+np.array([ 30e-3+25.5e-3+39e-3+39e-3, 0 ]))
    marker_offsets = [ 30e-3+25.5e-3, 30e-3+25.5e-3+13e-3, 30e-3+25.5e-3+13e-3+13e-3, 30e-3+25.5e-3+39e-3+13e-3, 30e-3+25.5e-3+39e-3+13e-3+13e-3 ]
    for i,marker in enumerate(sync_markers):
        marker=np.array([ pg.sheet_full_y - pg.sheet_offset_top, pg.marker_width+pg.sheet_marker_offset_left ])-marker
        marker = np.fliplr(marker)
        if pg.printdirection==1:
            pg.marker.firstpress['sync'+str(i)]=marker
        else:
            pg.marker.firstpress['sync'+str(i)]=c.rotate(marker,180,[pg.sheet_full_x/2, pg.sheet_full_y/2])
    if pg.printdirection==1:
        pg.marker.secondpress['sync']=np.fliplr(np.array([pg.sheet_full_y-pg.sheet_offset_top,pg.marker_width+pg.sheet_marker_offset_left])-trapez - np.array( [ marker_offsets[1],0 ] ))
        pg.marker.thirdpress['sync']=np.fliplr(np.array([pg.sheet_full_y-pg.sheet_offset_top,pg.marker_width+pg.sheet_marker_offset_left])-trapez - np.array( [ marker_offsets[2] ,0] ))
    else:
        pg.marker.secondpress['sync']=c.rotate(np.fliplr(pg.marker_width+np.array([pg.sheet_marker_offsets,pg.sheet_marker_offset_right])-trapez - np.array( [ marker_offsets[3],0 ] ))+np.array([0,pg.sheet_full_y]),180,[pg.sheet_full_x/2, pg.sheet_full_y/2])
        pg.marker.thirdpress['sync']=c.rotate(np.fliplr(pg.marker_width+np.array([pg.sheet_marker_offsets,pg.sheet_marker_offset_right])-trapez - np.array( [ marker_offsets[4] ,0] ))+np.array([0,pg.sheet_full_y]),180,[pg.sheet_full_x/2, pg.sheet_full_y/2])
    
    return pg
def set_shrink_markers(pg):
    shrinkmarkers=list()
    shrinkmarkers.append(c.translate( np.array([ [0, 0],[ -2, 2],[ -2, 2-0.25],[ -0.25, 0],[ -2, -2+0.25],[-2, -2]]), np.array([ -1, 0 ])))
    shrinkmarkers.append(c.translate( np.array([[0, 0],[2, 2],[2, 2-0.25],[0.25, 0],[2, -2+0.25],[2, -2]]),np.array( [ 1, 0 ])))
    y_values = np.array([0,0.05,0.1,0.15,0.20,0.25,0.3,0.35,0.4,0.45])
    cross_positions=np.array([np.ones(len(y_values))*(pg.sheet_full_x-pg.substrate_foil_full_x)/2,y_values.transpose()]).transpose()
    for i,pos in enumerate(cross_positions):
        for j,marker in enumerate(shrinkmarkers):
            poly_edges=c.translate(1e-3*marker,pos)
            pg.marker.firstpress['shrink'+str(i)+str(j)]=poly_edges
            pg.marker.firstpress['mshrink'+str(i)+str(j)]=c.mirror(c.translate(1e-3*marker,pos),pg)
    return pg
def set_cross_markers(pg):
    crossmarkers_a=list()
    crossmarkers_b=list()
    crossmarkers_a.append(np.array(c.translate([[0, 0],[0.25, 1],[-0.25, 1]],[0,3])))
    crossmarkers_b.append(np.array(c.translate([[0, 0],[0.25, -1],[-0.25, -1]],[0,3])))
    crossmarkers_a.append(np.array(c.translate([[0, 0],[ -1, 0.25],[ -1, -0.25]],[0,0])))
    crossmarkers_b.append(np.array(c.translate([[0, 0],[+1, 0.25],[+1, -0.25]],[0,0])))
    y_values=pg.marker_line_y+7e-3
    cross_positions=np.array([np.ones(len(y_values))*pg.marker_line_x,y_values.transpose()]).transpose()
    for i,pos in enumerate(cross_positions):
        numeration1=makerectangle(0.002,0.0002,pos-np.array([0.004,0.0005]))
        numeration2=makerectangle(0.002,0.0002,pos-np.array([0.004,-0.0005]))
        numeration3=makerectangle(0.002,0.0002,pos-np.array([0.004,0.00075])-np.array([0,7e-3]))
        numeration4=makerectangle(0.002,0.0002,pos-np.array([0.004,0])-np.array([0,7e-3]))
        numeration5=makerectangle(0.002,0.0002,pos-np.array([0.004,-0.00075])-np.array([0,7e-3]))
        pg.marker.firstpress['numxmsp1'+str(i)]=numeration1
        pg.marker.firstpress['numxmsp2'+str(i)]=numeration2
        pg.marker.firstpress['numxmtp3'+str(i)]=numeration3+np.array([0,17e-3])
        pg.marker.firstpress['numxmtp4'+str(i)]=numeration4+np.array([0,17e-3])
        pg.marker.firstpress['numxmtp5'+str(i)]=numeration5+np.array([0,17e-3])
        pg.marker.firstpress['mnuxcmsp1'+str(i)]=c.mirror(numeration1,pg)
        pg.marker.firstpress['mnuxcmsp2'+str(i)]=c.mirror(numeration2,pg)
        pg.marker.firstpress['mnuxcmtp3'+str(i)]=c.mirror(numeration3+np.array([0,17e-3]),pg)
        pg.marker.firstpress['mnuxcmtp4'+str(i)]=c.mirror(numeration4+np.array([0,17e-3]),pg)
        pg.marker.firstpress['mnuxcmtp5'+str(i)]=c.mirror(numeration5+np.array([0,17e-3]),pg)
        for j,marker in enumerate(crossmarkers_b):
            poly_edges=c.translate(marker*1.6e-3,pos)
            pg.marker.secondpress['cross0sp'+str(i)+str(j)]=poly_edges
            pg.marker.secondpress['mcross0sp'+str(i)+str(j)]=c.mirror(c.translate(marker*1.6e-3,pos),pg)
        for j,marker in enumerate(crossmarkers_a):
            poly_edges=c.translate(marker*1.6e-3,pos)
            pg.marker.firstpress['cross0'+str(i)+str(j)]=poly_edges
            pg.marker.firstpress['mcross0'+str(i)+str(j)]=c.mirror(c.translate(marker*1.6e-3,pos),pg)
        position=pos+np.array([0,10e-3])
        for j,marker in enumerate(crossmarkers_b):
            poly_edges=c.translate(marker*1.6e-3,position)
            pg.marker.thirdpress['cross1tp'+str(i)+str(j)]=poly_edges
            pg.marker.thirdpress['mcross1tp'+str(i)+str(j)]=c.mirror(c.translate(marker*1.6e-3,position),pg)
        for j,marker in enumerate(crossmarkers_a):
            poly_edges=c.translate(marker*1.6e-3,position)
            pg.marker.firstpress['cross1'+str(i)+str(j)]=poly_edges
            pg.marker.firstpress['mcross1'+str(i)+str(j)]=c.mirror(c.translate(marker*1.6e-3,position),pg)
    return pg
def set_circle_markers(pg):
    xposition=pg.marker_line_x
    yposition=np.append(pg.marker_line_y,pg.sheet_full_y)
    bigradius=pg.circle_marker_outerradius
    smallradius=pg.circle_marker_outerradius-pg.circle_marker_linewidth
    for i,pos in enumerate(yposition):               
        if not((i==len(yposition)-1)):
            center=np.array([xposition,pos])
            outer=list(makecircle(center,bigradius))
            inner=list(np.flipud(makecircle(center,smallradius)))
            numeration1=makerectangle(0.002,0.0002,center-np.array([0.004,0.0005]))
            numeration2=makerectangle(0.002,0.0002,center-np.array([0.004,-0.0005]))
            numeration3=makerectangle(0.002,0.0002,center-np.array([0.004,0.00075])-np.array([0,7e-3]))
            numeration4=makerectangle(0.002,0.0002,center-np.array([0.004,0])-np.array([0,7e-3]))
            numeration5=makerectangle(0.002,0.0002,center-np.array([0.004,-0.00075])-np.array([0,7e-3]))
            cross1=c.rotate(makerectangle(2*pg.circle_marker_outerradius,pg.circle_marker_crosswidth,center),45,center)
            cross2=c.rotate(makerectangle(2*pg.circle_marker_outerradius,pg.circle_marker_crosswidth,center),-45,center)
            cross=c.drawable(so.cascaded_union([sp.Polygon(cross1),sp.Polygon(cross2)]))
            pg.marker.firstpress['spcircle'+str(i)]=np.array(outer+inner)
            pg.marker.firstpress['numcmsp1'+str(i)]=numeration1
            pg.marker.firstpress['numcmsp2'+str(i)]=numeration2
            pg.marker.firstpress['numcmtp3'+str(i)]=numeration3
            pg.marker.firstpress['numcmtp4'+str(i)]=numeration4
            pg.marker.firstpress['numcmtp5'+str(i)]=numeration5
            pg.marker.firstpress['mnumcmsp1'+str(i)]=c.mirror(numeration1,pg)
            pg.marker.firstpress['mnumcmsp2'+str(i)]=c.mirror(numeration2,pg)
            pg.marker.firstpress['mnumcmtp3'+str(i)]=c.mirror(numeration3,pg)
            pg.marker.firstpress['mnumcmtp4'+str(i)]=c.mirror(numeration4,pg)
            pg.marker.firstpress['mnumcmtp5'+str(i)]=c.mirror(numeration5,pg)
            pg.marker.firstpress['mspcircle'+str(i)]=c.mirror(np.array(outer+inner),pg)
            pg.marker.firstpress['tpcircle'+str(i)]=np.array(outer+inner)-np.array([0,7e-3])
            pg.marker.firstpress['mtpcircle'+str(i)]=c.mirror(np.array(outer+inner)-np.array([0,7e-3]),pg)
            pg.marker.secondpress['spcirlcecross'+str(i)]=c.drawable(so.cascaded_union([sp.Polygon(cross1),sp.Polygon(cross2)]))
            pg.marker.thirdpress['tpcirlcecross'+str(i)]=c.drawable(so.cascaded_union([sp.Polygon(cross1),sp.Polygon(cross2)]))-np.array([0,7e-3])
            mirrored_cross=c.mirror(cross,pg)
            pg.marker.secondpress['mspcirlcecross'+str(i)]=mirrored_cross
            pg.marker.thirdpress['mtpcirlcecross'+str(i)]=c.mirror(c.drawable(so.cascaded_union([sp.Polygon(cross1),sp.Polygon(cross2)]))-np.array([0,7e-3]),pg)

    return pg
###############################################################################
def set_controlbar(pg):
    pg.marker.firstpress["controlbar"]=makerectangle(pg.controlbar_width,pg.controlbar_length,pg.controlbar_position)
    return pg    
###############################################################################
def set_fold_markers(pg):
    anker_midpoint=pg.elementlist[0].midpoint
    if pg.angle!=0:
        foldmarker_distance=pg.cell_y/c.cosd(pg.angle)
    else:
        foldmarker_distance=pg.cell_y
    anker_midpoint=anker_midpoint#+dr.up(pg.angle)*pg.cell_y/2
    fold_marker=np.array([[-pg.foldmarker_length/2, 0],[-pg.foldmarker_length/2, +pg.foldmarker_width],[+pg.foldmarker_length/2, +pg.foldmarker_width],[+pg.foldmarker_length/2, 0]])
    foldstrip=makerectangle(0.5e-3,pg.foldmarker_width,[-pg.foldmarker_length/2, 0])
    
    if pg.angle!=0:
        fold_marker = c.rotate( fold_marker, pg.angle,[0, 0] )
        foldstrip = c.rotate( foldstrip, pg.angle,[0, 0] )
        fold_marker=c.translate( fold_marker, anker_midpoint)
        foldstrip=c.translate( foldstrip, anker_midpoint)
        shift_cell_width = (pg.sheet_full_x-pg.foldmarker_spacing_right-max(fold_marker[:,0]))/c.cosd(pg.angle)
        fold_marker=c.translate( fold_marker,np.array([c.cosd(pg.angle), c.sind(pg.angle)])*shift_cell_width)
        foldstrip=c.translate( foldstrip,np.array([c.cosd(pg.angle), c.sind(pg.angle)])*shift_cell_width)
        foldstrip=c.translate(foldstrip,dr.down(pg.angle)*pg.cell_y/2)
    else:
        fold_marker=c.translate( fold_marker, anker_midpoint)
        shift_cell_width = (pg.sheet_full_x-pg.foldmarker_spacing_right-max(fold_marker[:,0]))
        fold_marker=c.translate( fold_marker, np.array([1,0])*shift_cell_width)
    pg.marker.firstpress['fold0']=fold_marker
    for i in range(10):
        pg.marker.firstpress['foldstrip'+str(i)]=c.translate(foldstrip,dr.right(pg.angle)*3e-3*i)
#    ps.draw("output_files/richard/richard_rgb.eps",fold_marker,[0,0,0])
    temp_top=c.translate( fold_marker,  np.array([0, 1])*foldmarker_distance)
    temp_down=c.translate( fold_marker, np.array([0, -1])*foldmarker_distance)
    foldstripstart=foldstrip
    i=1
    while cb.isitin(temp_top,pg.sheet):
        pg.marker.firstpress['fold'+str(i)]=temp_top
        foldstrip=c.translate( foldstrip, np.array([0,1])*foldmarker_distance)
        for j in range(10):
            pg.marker.firstpress['foldstrip'+str(i)+str(j)]=c.translate(foldstrip,dr.right(pg.angle)*3e-3*j)
#        ps.draw("output_files/richard/richard_rgb.eps",temp_top,[1,0,0])
        temp_top=c.translate( temp_top, np.array([0,1])*foldmarker_distance)
        i=i+1
    foldstrip=foldstripstart
    while cb.isitin(temp_down,pg.sheet):
        pg.marker.firstpress['fold'+str(i)]=temp_down
        foldstrip=c.translate( foldstrip, np.array([0,-1])*foldmarker_distance)
        for j in range(10):
            pg.marker.firstpress['foldstrip'+str(i)+str(j)]=c.translate(foldstrip,dr.right(pg.angle)*3e-3*j)
#        ps.draw("output_files/richard/richard_rgb.eps",temp_down,[0,1,0])
        temp_down=c.translate( temp_down, np.array([0,-1])*foldmarker_distance)
        i=i+1
    
    generatorparts=list()
    generatorparts.append(sp.Polygon(pg.contactfield.main))
    generatorparts.append(sp.Polygon(pg.input_connector))
#    for element in pg.elementlist:
#        generatorparts.append(sp.Polygon(element.outline))
#        generatorparts.append(sp.Polygon(element.exitconnector))
#    generator=so.cascaded_union(generatorparts)
#    if pg.set_intermediate_foldmarkers:
#        try:
#            pg.intermediate_foldmarker_length
#        except NameError:
#            pg.intermediate_foldmarker_length=np.array(0)
#        
#        intermediate_foldmarker_length_calculated=0
#        
#        if pg.intermediate_foldmarker_length!=0:
#            intermediate_foldmarker_length_calculated=pg.intermediate_foldmarker_length #use the new value
#        else:
#            intermediate_foldmarker_length_calculated=pg.cell_x*0.5 # the old value
#        
#        for i,field in enumerate(pg.fieldlist):
#            bar=makerectangle(intermediate_foldmarker_length_calculated,pg.foldmarker_width,(field.corners[0]+field.corners[3])/2)
#            bar=c.rotate(bar,pg.angle,(field.corners[0]+field.corners[3])/2)
#            if not(generator.intersects(sp.Polygon(bar)) or not(sp.Polygon(pg.printingarea).contains(sp.Polygon(bar)))):
#                pg.marker.firstpress['foldbar'+str(i)]=bar    
    return pg
###############################################################################
def set_testpads(pg):
    pad_ypositions=457.2e-3 * (np.array([0, 4])/9)+pg.pad_ypositions_offset
    middlestrip=makerectangle(pg.pad_length/pg.pad_aspect_ratio,pg.pad_length+2*pg.padradius,[0, 0])
    top_circle=makecircle(np.array([0, pg.pad_length/2+pg.padradius]),pg.padradius)
    bottom_circle=makecircle(np.array([0,-pg.pad_length/2-pg.padradius]),pg.padradius)
    testpad_0=c.drawable(so.cascaded_union([sp.Polygon(middlestrip),sp.Polygon(top_circle),sp.Polygon(bottom_circle)]))
    testpad_0=c.translate(testpad_0,np.array([pg.marker_line_x, 0]))
    for i,pos in enumerate(pad_ypositions):
        testpad=c.translate(testpad_0,np.array([0, pos]))
        pg.marker.firstpress['testpad'+str(i)]=testpad
    return pg
###############################################################################
def set_antiboxingstrucure(pg):
    abs_left=makerectangle(pg.abs_width,pg.sheet_full_y,[pg.abs_width/2+pg.abs_offset,pg.sheet_full_y/2])
    pg.marker.firstpress['abs_left']=abs_left
    pg.marker.firstpress['abs_right']=c.mirror(makerectangle(pg.abs_width,pg.sheet_full_y,[pg.abs_width/2+pg.abs_offset,pg.sheet_full_y/2]),pg)
    return pg